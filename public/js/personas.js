$(document).ready(function() {
    // Oculto el input de dni y cuit ni bien carga para mostrar el que corresponda segun el tipo de persona (fisica/juridica)
    $('#identificador_cuit, #identificador_dni').hide();

    $('#datetimepicker1').datepicker();

    $("select#matriz_id").change(function(){
        var matriz_id = $(this).find(":selected").val();

        // Limpio el contenido de la matriz
        $('#matriz').empty();

        // Muestro el "cargando..."
        $('#loading-indicator').show();

        if( matriz_id != 0 )
        {
            var persona_id = $('#persona_id').val();

            $.ajax({
                url: "/api/matriz/" + matriz_id + "/armar_nueva_persona_matriz/" + persona_id,
                success: function(matriz){
                    $('#loading-indicator').hide();

                    $('#matriz').html(matriz);

                    $('.alert-error').empty(); // Limpio errores existentes
                }
            })
        }
    });

    /**
     * Verifico que todos los campos hayan sido completados antes de enviar el formulario
     */
    $('form#nueva_persona').on('submit', function(e){
        e.preventDefault();
        $('.alert-error').empty();

        var error = false,
            cant_parametros = 0,
            cant_niveles_seleccionados = $('.nivel:checked').size();

        // Verifico que todos los campos se hayan completado
        /*$('.form-group > .campo').each(function(){
            if( $(this).val() == "" )
            {
                error = true;
                agregarMensajeDeError('Faltan completar el campo ' + $(this).data('descripcion') );
                $(this).parent().removeClass('has-success').addClass('has-error has-feedback');
            }
            else
            {
                $(this).parent().removeClass('has-error').addClass('has-success has-feedback').focus();
            }
        });*/

        // Verifico que haya al menos un check tildado para cada parametro
        $('.parametro').each(function(){
            cant_parametros++;
        });

        if(cant_niveles_seleccionados < cant_parametros)
        {
            error = true;
            agregarMensajeDeError('Faltan seleccionar niveles');
        }

        // No hay errores, enviar el formulario
        if( !error )
        {
            var formData = $(this).serialize(); // form data as string
            var formAction = $(this).attr('action'); // form handler url
            var formMethod = $(this).attr('method');

            $.ajax({
                type     : formMethod,
                url      : formAction,
                data     : formData,
                dataType : 'json',

                success : function(data){
                    location.href = data.redirectTo;
                },

                error : function(data){
                    var errors = data.responseJSON;

                    for(var key in errors)
                    {
                        $('input[name=' + key + ']').parent().removeClass('has-success').addClass('has-error has-feedback');
                        agregarMensajeDeError(errors[key]);
                    }
                }
            });
        }
    });

    function agregarMensajeDeError(mensaje)
    {
        $('.alert-error').append(
            "<div class='alert alert-danger'>" +
                "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>&nbsp;" + mensaje +
            "</div>"
        ).show();
    }
});
$(document).ready(function() {
    var adjunto_subido = ""; // Lista de archivos subidos que se enviara al hacer click en Adjuntar

    /* SUBIDA DEL ARCHIVO */
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
        '//jquery-file-upload.appspot.com/' : 'subir_adjunto';

    $('#form-nuevo-adjunto').fileupload({
        url: url,
        dataType: 'json',
        add: function(e, data) {
            var uploadErrors = [];

            if(data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 2000000) { // 2MB
                uploadErrors.push('Filesize is too big');
            }

            if(uploadErrors.length > 0) {
                alert(uploadErrors.join("\n"));
            } else {
                data.submit();
            }
        },
        done: function (e, data) {
            var resultado = data.result;

            // Si adjunto_subido != "" significa que ya se subio algo => no hago nada (pero se sube igual)
            if( adjunto_subido == "" ) {
                if (resultado.valid == true) {
                    $('<p/>').text(resultado.nombre_original).appendTo('#files');
                    adjunto_subido = resultado.url_adjunto_subido;
                } else {
                    $('#files').html(resultado.error);
                }
            } else {
                alert("Usted ya subio un archivo");
            }
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        },
        fail: function (e, data) {
            $.each(data.messages, function (index, error) {
                $('<p style="color: red;">Upload file error: ' + error + '<i class="elusive-remove" style="padding-left:10px;"/></p>')
                    .appendTo('#form-nuevo-adjunto');
            });
        },
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    function clearForm()
    {
        adjunto_subido = "";
        $('#fileupload').val('');
        $('#descripcion').val('');
        $('#files').html('');
    };

    $('#mostrar_nuevo_adjunto').on('click', function(e){
        e.preventDefault();

        $("#dialog-nuevo-adjunto").dialog({
            modal: true,
            width: 650,
            height: 400,
            close: clearForm,
            buttons: {
                "Adjuntar": function(){
                    if($('#descripcion').val() != "" && adjunto_subido !== 0) {
                        var persona_id = $('#persona_id').val() | null,
                            cuenta_id  = $('#cuenta_id').val() | null,
                            alerta_id  = $('#alerta_id').val() | null;

                        var formData   = {
                            'adjunto'     : adjunto_subido,
                            'descripcion' : $('#descripcion').val(),
                            'persona_id'  : persona_id,
                            'cuenta_id'   : cuenta_id,
                            'alerta_id'   : alerta_id,
                            '_token'      : $('input[name=_token]').val()
                        };

                        $.ajax({
                            type     : 'POST',
                            url      : 'nuevo_adjunto',
                            data     : formData,
                            dataType : 'json',

                            success : function(data){
                                var fecha_creacion = data.created_at.date;

                                // Si no hay ningun adjunto, en vez de hacer prepend, redirecciono a la pagina para que aparezca la tabla
                                /*if ($('#lista_adjuntos').length > 0) {
                                    $('#lista_adjuntos').prepend(
                                        "<tr>" +
                                        "<td>" + data.descripcion + "</td>" +
                                        "<td><a href='../" + data.url + "' download='../" + data.url + "'>Ver...</a></td>" +
                                        "<td>" + fecha_creacion.substr(0, fecha_creacion.length - 7) + "</td>" +
                                        "<td><a href='#' class='eliminar_adjunto' data-persona-id='" + persona_id + "' data-cuenta-id='" + cuenta_id + "' data-alerta-id='" + alerta_id + "'>Eliminar</a></td>" +
                                        "</tr>"
                                    );
                                } else {
                                    location.href = window.location.href;
                                }*/

                                location.href = window.location.href;

                                $("#dialog-nuevo-adjunto").dialog("close")
                            }
                        });

                        // Limpio el dialog para que pueda subir otro adjunto
                        adjunto_subido = "";
                        $('#descripcion').val("");
                    } else {
                        alert('Faltan datos !!');
                    }
                },
                "Cerrar": function () {
                    $("#dialog-nuevo-adjunto").dialog("close");
                }
            }
        });
    });
});
$(document).ready(function() {
    $('a.eliminar_adjunto').on('click', function(e) {
        e.preventDefault();

        if(confirm("Esta seguro que desea eliminar el adjunto?")) {
            var formData = {
                'adjunto_id': $(this).data('id'),
                'persona_id': $(this).data('persona-id'),
                'cuenta_id' : $(this).data('cuenta-id'),
                'alerta_id' : $(this).data('alerta-id'),
                '_token'    : $('input[name="_token"]').val()
            };

            $.ajax({
                type: 'POST',
                url: 'eliminar_adjunto',
                data: formData,
                dataType: 'json',
                success : function(data){
                    if( data.success == true ) {
                        //$('tr#' + data.adjunto_id).remove();
                        location.href = window.location.href;
                    } else {
                        alert('Error al eliminar el adjunto');
                    }
                }
            });
        }
    });
});
//# sourceMappingURL=personas.js.map
