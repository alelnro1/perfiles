$(document).ready(function() {
    $('#comerciales').multiselect({
        nonSelectedText: 'Seleccione un comercial...',
        allSelectedText: 'Todos seleccionados',
        enableFiltering: true,
        filterPlaceholder: 'Buscar'
    });
});
//# sourceMappingURL=mailing.js.map
