<div id="dialog-nuevo-adjunto" style="display:none;" title="Adjuntar un archivo">
    <form enctype="multipart/form-data" id="form-nuevo-adjunto">
        {!! csrf_field() !!}

        <fieldset>
            @if ($tipo_adjunto == "cuenta")
                <input type="hidden" id="cuenta_id" value="{{ $cuenta->id }}">
            @elseif ($tipo_adjunto == "persona")
                <input type="hidden" id="persona_id" value="{{ $persona->id }}">
            @elseif ($tipo_adjunto == "alerta")
                <input type="hidden" id="alerta_id" value="{{ $alerta->id }}">
            @endif

            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label">Descripcion</label>

                <input type="text" name="descripcion" id="descripcion" placeholder="Descripcion" class="form-control">
            </div>

            <div>
               <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Seleccione un archivo...</span>

                    <input id="fileupload" type="file" name="adjunto">
                </span>
                <br>
                <br>
                <!-- The global progress bar -->
                <div id="progress" class="progress">
                    <div class="progress-bar progress-bar-success"></div>
                </div>
                <!-- The container for the uploaded files -->
                <div id="files" class="files"></div>
            </div>
        </fieldset>
    </form>
</div>