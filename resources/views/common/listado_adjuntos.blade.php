<legend>
    Adjuntos
    @if (Auth::user()->hasRole('operador'))
    <div style="width:150px; float:right;">
        <a href="#" id="mostrar_nuevo_adjunto">Nuevo Adjunto</a>
    </div>
    @endif
</legend>

<table class="table table-striped">
    @if (count($adjuntos) > 0)
        <thead>
        <th>Descripcion</th>
        <th>Link</th>
        <th colspan="2">Fecha de Subida</th>
        </thead>

        <tbody id="lista_adjuntos">
        @foreach ($adjuntos as $adjunto)
            @if ($adjunto->adjunto != "")
                <tr id="{{ $adjunto->id }}">
                    {{ csrf_field() }}
                    <td>{{ $adjunto->descripcion }}</td>
                    <td><a href="{{ url($adjunto->adjunto) }}" download="{{ url($adjunto->adjunto) }}">Ver...</a></td>
                    <td>{{ $adjunto->created_at }}</td>

                    @if (Auth::user()->hasRole('operador'))
                        <td>
                            <a href="#" class="eliminar_adjunto" data-id="{{ $adjunto->id }}" data-persona-id="{{ $adjunto->persona_id }}" data-cuenta-id="{{ $adjunto->cuenta_id }}" data-alerta-id="{{ $adjunto->alerta_id }}">Eliminar</a>
                        </td>
                    @endif
                </tr>
            @endif
        @endforeach
        </tbody>
    @else
        NO HAY ADJUNTOS
    @endif
</table>

