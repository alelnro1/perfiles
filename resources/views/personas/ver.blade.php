@extends('layouts.app')

@section('content')
    <link href="{{asset('/css/fileupload.css')}}" rel="stylesheet">

    <div class="panel-heading">
        Persona:
        {{ $persona->nombre }}

        @if ($descripcion_matriz != "")
            ({{ $descripcion_matriz }})
        @endif
    </div>

    <div class="panel-body">
        @if(Session::has('actualizado'))
            <div class="alert alert-success">
                {{ Session::get('actualizado') }}
            </div>
        @endif
    <!-- Campos especiales de la matriz -->
        @if ($descripcion_matriz != "")
            <fieldset>
                <legend>Cabecera</legend>
                <table class="table table-striped task-table" style="width: 35%;">
                    @foreach ($campos as $campo)
                        @if ($campo->nombre != "perfil_vigente" && !empty($persona[$campo->nombre]))
                            {{-- Disculpas por esta asquerosidad: Si la fecha de documentacion o el cierre de balance son 0000-00-00 00:00:00 => no mostrar --}}
                            @if($campo->nombre == "cierre_balance" || $campo->nombre == "fecha_documentacion" && (strtotime($persona[$campo->nombre]) !== 0 && (strtotime($persona[$campo->nombre]) > 0)))
                                @if($campo->nombre == "fecha_documentacion" && date("Y", strtotime($campo->descripcion)) == date('Y'))
                                    <tr>

                                        <td>{{ $campo->descripcion }}</td>
                                        <td>
                                            <span style="float:right;">{{ date("d/m/Y", strtotime($persona[$campo->nombre])) }}</span>
                                        </td>
                                    </tr>
                                @endif
                            @endif

                            @if(!($campo->nombre == "cierre_balance" || $campo->nombre == "fecha_documentacion"))
                                <tr>
                                    <td>{{ $campo->descripcion }}</td>
                                    <td>
                                        @if(is_numeric($persona[$campo->nombre]))
                                            @if(!in_array($campo->nombre, ['anio_nacimiento', 'dni', 'cuit', 'cuil']))
                                                <span style="float:right;">{{ number_format($persona[$campo->nombre], 2, ',', '.') }}</span>
                                            @elseif(in_array($campo->nombre, ['dni', 'cuit', 'cuil']))
                                                <span style="float:right;">{{ $persona[$campo->nombre] }}</span>
                                            @else
                                                <span style="float:right;">{{ number_format($persona[$campo->nombre], 0) }}</span>
                                            @endif
                                        @else
                                            <span style="float:right;">{{ $persona[$campo->nombre] }}</span>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        @endif
                    @endforeach

                    <tr>
                        <td>Riesgo</td>
                        <td><span style="float:right;">{{ $persona->Riesgo->descripcion }}</span></td>
                    </tr>

                    <tr>
                        <td>Período</td>
                        <td><span style="float:right;">{{ date("Y", time()) }}</span></td>
                    </tr>
                </table>

            </fieldset>
            <!-- Matriz -->
            @include('matrices.matriz_con_datos')

        <!-- Cuentas -->
            <fieldset>
                <legend>Cuentas</legend>
                @if (count($cuentas) > 0)
                    <table class="table table-striped">
                        <thead>
                        <th>Nro de Cuenta</th>
                        <th colspan="2">Perfil</th>
                        </thead>

                        <tbody>
                        @foreach ($cuentas as $cuenta)
                            <tr>
                                <td>{{ $cuenta['id_sistema1'] || $cuenta['id_sistema2']}}</td>
                                <td>{{ number_format($cuenta['perfil'], 2, ',', '.') }}</td>
                                <td><a href="/{{ $system }}/cuentas/{{ $cuenta['id'] }}">Ver</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    La persona no tiene cuentas relacionadas
                @endif
            </fieldset>

            <!-- Adjuntos -->
            <fieldset>
                @include('common.listado_adjuntos')
                @include('common.form_nuevo_adjunto', array('tipo_adjunto' => 'persona'))
            </fieldset>



            @if(Auth::user()->hasRole('operador'))
                <a href="/{{ $system }}/personas/{{ $persona->id }}/edit">Nueva Matriz</a>
            @endif
        @else
            <fieldset>

                <legend>Cabecera</legend>
                <table class="table table-striped task-table" style="width: 35%;">
                    {{-- Disculpas por esta asquerosidad: Si la fecha de documentacion o el cierre de balance son 0000-00-00 00:00:00 => no mostrar --}}
                    <tr>

                        <td>DNI</td>
                        <td>
                            <span style="float:right;">{{ $persona->dni }}</span>
                        </td>
                    </tr>
                    @if($persona->cuil)

                        <tr>
                            <td>CUIL</td>
                            <td>
                                <span style="float:right;">{{$persona->cuil}}</span>
                            </td>
                        </tr>
                    @endif
                    @if($persona->cuit)
                        <tr>
                            <td>CUIT</td>
                            <td>
                                <span style="float:right;">{{$persona->cuit}}</span>
                            </td>
                        </tr>
                    @endif

                    <tr>
                        <td>Riesgo</td>
                        <td><span style="float:right;">{{ $persona->Riesgo->descripcion }}</span></td>
                    </tr>
                    @if($persona->perfil_vigente)
                        <tr>
                            <td>Perfil Vigente</td>
                            <td><span style="float:right;">{{ $persona->perfil_vigente }}</span></td>
                        </tr>
                    @endif
                    @if($persona->perfil_calculado)
                        <tr>
                            <td>Perfil Vigente</td>
                            <td><span style="float:right;">{{ $persona->perfil_calculado }}</span></td>
                        </tr>
                    @endif
                    <tr>
                        <td>Período</td>
                        <td><span style="float:right;">{{ date("Y", time()) }}</span></td>
                    </tr>
                </table>
            </fieldset>

            Para poder visualizar o editar una persona, debe tener una matriz asignada. Seleccione la matriz a asignar a
            la persona

            <form action="/{{ $system }}/personas/{{ $persona->id }}/asignarMatriz" method="POST">
                {!! csrf_field() !!}

                <select name="matriz_id" class="form-control">
                    <option value="0">Seleccione una matriz...</option>

                    @foreach ($matrices as $matriz)
                        @if ($persona->matriz_id == $matriz->id)
                            <option value="{{ $matriz->id }}" selected>{{ $matriz->descripcion }}</option>
                        @else
                            <option value="{{ $matriz->id }}">{{ $matriz->descripcion }}</option>
                        @endif
                    @endforeach
                </select>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Asignar matriz</button>
                </div>
            </form>
    @endif
    <!-- Historial -->
        <fieldset>
            <legend>Historial</legend>

            <table class="table table-striped">
                @if (count($historial) > 0)
                    <thead>
                    <th>Período</th>
                    <th>Fecha Actualizacion</th>
                    <th>Perfil Calculado</th>
                    <th>Perfil Vigente</th>
                    <th>Cambio por</th>
                    <th colspan="2">Perfil</th>
                    </thead>

                    <tbody>
                    @foreach($historial as $matriz_historica)
                        <tr>
                            <td>{{ date("Y", strtotime($matriz_historica->created_at)) }}</td>
                            <td>{{ $matriz_historica->created_at }}</td>
                            <td>{{ number_format($matriz_historica->perfil_calculado, 2, ',', '.') }}</td>
                            <td>{{ number_format($matriz_historica->perfil_vigente, 2, ',', '.') }}</td>
                            <td>{{ $matriz_historica->user['email'] }}</td>
                            <td>
                                @if($matriz_historica->matriz_id)
                                    <a href="/{{ $system }}/matrices/matriz-historica/{{ $matriz_historica->id }}/{{ date("Y", strtotime($matriz_historica->created_at)) }}">
                                        Ver
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                @else
                    NO HAY MATRICES HISTORICAS
                @endif
            </table>
        </fieldset>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>

    <script src="//cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
    <script src="{{ asset('/js/fileupload.js')}} "></script>
    <script src="{{ asset('/js/personas.js')}}"></script>
@stop