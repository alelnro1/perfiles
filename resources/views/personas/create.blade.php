@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="{{ asset('/css/datepicker/bootstrap-datepicker3.min.css') }}">
    <div class="panel-heading">Nueva Persona</div>

    <div class="panel-body">
        <form action="/{{ $system }}/personas" method="POST" id="nueva_persona">
            {!! csrf_field() !!}

            <fieldset>
                <legend>Tipo de Persona</legend>

                <div class="form-group {{ $errors->has('matriz_id') ? ' has-error' : '' }}">
                    <label for="matriz_id" class="control-label">Matriz</label>

                    <select name="matriz_id" id="matriz_id" class="form-control">
                        <option value="0">Seleccionar...</option>

                        @foreach ($matrices as $matriz)
                            <option value="{{ $matriz->id }}">{{ $matriz->descripcion  }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group {{ $errors->has('matriz') ? ' has-error' : '' }}" id="matriz"></div>
            </fieldset>

            <img src="/images/ajax-loader.gif" id="loading-indicator" style="display:none" />

            <div class="alert alert-error" style="display: none;"></div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Crear</button>
            </div>
        </form>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

@section('javascript')
    <script src="{{ asset('/js/fileupload.js')}}"></script>
    <script src="{{ asset('/js/personas.js')}}"></script>
    <script src="{{ asset('/js/datepicker/bootstrap-datepicker.min.js') }}"></script>
    @parent
@stop