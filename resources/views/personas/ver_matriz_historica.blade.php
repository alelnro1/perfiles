@extends('layouts.app')

@section('styles')
    <style type="text/css" media="print">
        @page {
            size: auto;   /* auto is the initial value */
            margin: 0;  /* this affects the margin in the printer settings */
        }

        #volver {
            display: none !important;
        }

        header {
            display: none !important;
        }
    </style>
@endsection

@section('content')
    <h3 style="text-align:center;">Matriz de riesgo historica de <b>{{ $persona['nombre'] }}</b></h3>

    <!-- Datos -->
    <fieldset>
        <legend>Datos</legend>
        <!-- Fecha de Actualizacion -->
        <div class="form-group">
            <label for="fecha_actualizacion" class="control-label">
                Fecha de última actualización:</label>
            @if($historial_anterior)
                <span>{{ $historial_anterior->created_at }}</span>
            @else
                <span>{{ $persona->created_at }}</span>
            @endif

            <b> - </b>

            <span>{{ $fecha_actualizacion }}</span>
        </div>

        <!-- Riesgo -->
        <div class="form-group">
            <label for="fecha_actualizacion" class="control-label">Riesgo: </label>

            <span>{{ $riesgo_historico->descripcion }}</span>
        </div>

    @if ($periodo)
        <!-- Período -->
            <div class="form-group">
                <label for="fecha_actualizacion" class="control-label">Período: </label>

                <span>{{ $periodo }}</span>
            </div>
    @endif

    <!-- Tipo de Matriz -->
        <div class="form-group">
            <label for="tipo_matriz" class="control-label">Tipo de Matriz: </label>

            <span>{{ $descripcion_matriz }}</span>
        </div>
    </fieldset>

    <fieldset>
        <legend>Cabecera</legend>
        @foreach ($campos as $campo)
            <div class="form-group {{ $errors->has($campo->descripcion) ? ' has-error' : '' }}">
                <label for="campo_{{ $campo->id }}" class="control-label">{{ $campo->descripcion }}: </label>

                <span>{{ $persona[$campo->nombre] }}</span>
            </div>
        @endforeach
    </fieldset>

    <!-- Matriz -->
    @include('matrices.matriz_con_datos')

    <div id="volver">
        <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
    </div>
@stop

