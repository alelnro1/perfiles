@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
@stop

@section('content')
    <div class="panel-heading">Personas</div>
    <div class="panel-body">
        @if ($personas != false && count($personas) > 0)
            <table class="table table-striped task-table" id="personas">
                <!-- Table Headings -->
                <thead>
                    <tr>
                        <th>Cuenta</th>
                        <th>DNI</th>
                        <th>CUIT</th>
                        <th>CUIL</th>
                        <th>Nombre</th>
                        <th>Perfil Vigente</th>
                        <th>Perfil Cuenta</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($personas as $persona)
                    @if (count($persona['cuentas']) > 0)
                        @foreach ($persona['cuentas'] as $cuenta)
                            <tr>
                                <td>
                                    @if ($cuenta['id_sistema1'] != "")
                                        <a href="/{{ $system }}/cuentas/{{ $cuenta['id'] }}">{{ $cuenta['id_sistema1'] }}</a>
                                    @else
                                        <a href="/{{ $system }}/cuentas/{{ $cuenta['id'] }}">{{ $cuenta['id_sistema2'] }}</a>
                                    @endif
                                </td>
                                <td nowrap>{{ $persona['dni'] }}</td>
                                <td nowrap>{{ $persona['cuit'] }}</td>
                                <td nowrap>{{ $persona['cuil'] }}</td>
                                <td>{{ $persona['nombre'] }}</td>
                                <td style="text-align:right;">{{ number_format($persona['perfil_vigente'], 2, ',', '.') }}</td>
                                <td style="text-align:right;">{{ number_format($cuenta['perfil'], 2, ',', '.') }}</td>
                                <td><a href="/{{ $system }}/personas/{{ $persona['id'] }}">Ver</a></td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td>-</td>
                            <td nowrap>{{ $persona['dni'] }}</td>
                            <td nowrap>{{ $persona['cuit'] }}</td>
                            <td nowrap>{{ $persona['cuil'] }}</td>
                            <td>{{ $persona['nombre'] }}</td>
                            <td style="text-align:right;">{{ number_format($persona['perfil_vigente'], 2, ',', '.') }}</td>
                            <td style="text-align:center;">-</td>
                            <td><a href="/{{ $system }}/personas/{{ $persona['id'] }}">Ver</a></td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>

            @if (isset($filtros))
                <div>
                    <a href="/{{ $system }}/personas/exportar/{{ $filtros['dni'] }}/{{ $filtros['cuit'] }}/{{ $filtros['cuil'] }}/{{ $filtros['nombre'] }}/{{ $filtros['nro_cuenta'] }}/">Exportar todos los resultados</a>
                </div>
            @endif
        @else
            NO SE ENCONTRARON PERSONAS CON ESOS CRITERIOS
        @endif

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            oTable = $('#personas').DataTable({
                    iDisplayLength: 500,
                    columnDefs: [
                        { orderable: false, targets: -1 }
                    ],
                    bFilter: false,
                    "language": {
                        "info": "Mostrando _START_ a _END_ de _TOTAL_ personas filtradas",
                        "paginate": {
                            "first":      "Primera",
                            "last":       "Ultima",
                            "next":       "Siguiente",
                            "previous":   "Anterior"
                        },
                        "lengthMenu": "Mostrar _MENU_ personas",
                        "search": "Buscar:",
                        "infoFiltered": "(de un total de _MAX_ personas)",
                    }
                 });
        });
    </script>
@stop
