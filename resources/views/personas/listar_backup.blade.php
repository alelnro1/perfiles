@extends('layouts.app')

@section('content')
    <div class="panel-heading">Personas</div>
    <div class="panel-body">
        @if ($personas != false && count($personas) > 0)
            <table class="table table-striped task-table" id="users">
                <!-- Table Headings -->
                <thead>
                    @if($system == "gainvest")
                        <th>Cuenta GAINVEST</th>
                    @elseif($system == "cibsa")
                        <th>Cuenta CIBSA</th>
                    @endif
                    <th>DNI</th>
                    <th>CUIT</th>
                    <th>CUIL</th>
                    <th>Nombre</th>
                    <th>Perfil Vigente</th>
                    <th>Perfil Cuenta</th>
                </thead>

                <tbody>
                    @foreach ($personas as $persona)
                        @if (count($persona['cuentas']) > 0)
                            @foreach ($persona['cuentas'] as $cuenta)
                                <tr>
                                    <td>
                                        @if ($cuenta['id_sistema1'] != "")
                                            <a href="/{{ $system }}/cuentas/{{ $cuenta['id'] }}">{{ $cuenta['id_sistema1'] }}</a>
                                        @else
                                            <a href="/{{ $system }}/cuentas/{{ $cuenta['id'] }}">{{ $cuenta['id_sistema2'] }}</a>
                                        @endif
                                    </td>
                                    <td>{{ $persona['dni'] }}</td>
                                    <td>{{ $persona['cuit'] }}</td>
                                    <td>{{ $persona['cuil'] }}</td>
                                    <td>{{ $persona['nombre'] }}</td>
                                    <td style="text-align:right;">{{ number_format($persona['perfil_vigente'], 2) }}</td>
                                    <td style="text-align:right;">{{ number_format($cuenta['perfil'], 2) }}</td>
                                    <td><a href="/{{ $system }}/personas/{{ $persona['id'] }}">Ver</a></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td>-</td>
                                <td>{{ $persona['dni'] }}</td>
                                <td>{{ $persona['cuit'] }}</td>
                                <td>{{ $persona['cuil'] }}</td>
                                <td>{{ $persona['nombre'] }}</td>
                                <td style="text-align:right;">{{ number_format($persona['perfil_vigente'], 2) }}</td>
                                <td  style="text-align:center;">-</td>
                                <td><a href="/{{ $system }}/personas/{{ $persona['id'] }}">Ver</a></td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
            {{ $personas->render() }}

            <div>
                <a href="/{{ $system }}/personas/exportar/{{ $filtros['dni'] }}/{{ $filtros['cuit'] }}/{{ $filtros['cuil'] }}/{{ $filtros['nombre'] }}/{{ $filtros['nro_cuenta'] }}/">Exportar todos los resultados</a>
            </div>
        @else
            NO SE ENCONTRARON PERSONAS CON ESOS CRITERIOS
        @endif

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            oTable = $('#users').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "/users/data",
                "columns": [
                    {data: 'dni', name: 'DNI'},
                    {data: 'cjit', name: 'CUIT'},
                    {data: 'cuil', name: 'CUIL'},
                    {data: 'nombre', name: 'Nombre'}
                    {data: 'perfil_vigente', name: 'Perfil Vigente'}
                    {data: 'perfil_cuenta', name: 'Perfil Cuenta'}
                ]
            });
        });
    </script>
@endsection
