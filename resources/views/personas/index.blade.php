@extends('layouts.app')

@section('content')
    <div class="panel-heading">Buscar persona</div>

    <div class="panel-body">
        <form action="/{{ $system }}/personas/buscar" method="GET">
            <!-- DNI -->
                <div class="form-group {{ $errors->has('dni') ? ' has-error' : '' }}">
                    <label for="dni" class="control-label">DNI</label>
                    <input type="text" name="dni" id="dni" class="form-control" value="{{ old('dni') }}">

                    @if ($errors->has('dni'))
                        <span class="help-block">
                            <strong>{{ $errors->first('dni') }}</strong>
                        </span>
                    @endif
                </div>


            <!-- CUIT -->
                <div class="form-group {{ $errors->has('cuit') ? ' has-error' : '' }}">
                    <label for="cuit" class="control-label">CUIT</label>
                    <input type="text" name="cuit" id="cuit" class="form-control" value="{{ old('cuit') }}">

                    @if ($errors->has('cuit'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cuit') }}</strong>
                        </span>
                    @endif
                </div>

            <!-- CUIL -->
                <div class="form-group {{ $errors->has('cuil') ? ' has-error' : '' }}">
                    <label for="cuil" class="control-label">CUIL</label>
                    <input type="text" name="cuil" id="cuil" class="form-control" value="{{ old('cuil') }}">

                    @if ($errors->has('cuil'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cuil') }}</strong>
                        </span>
                    @endif
                </div>

            <!-- Nombre/Razon Social -->
                <div class="form-group {{ $errors->has('nombre') ? ' has-error' : '' }}">
                    <label for="nombre" class="control-label">Nombre</label>
                    <input type="text" name="nombre" id="nombre" class="form-control" value="{{ old('nombre') }}">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>

            <!-- Nro de cuenta -->
                <div class="form-group {{ $errors->has('nro_cuenta') ? ' has-error' : '' }}">
                    <label for="nro_cuenta" class="control-label">Nro de cuenta</label>
                    <input type="text" name="nro_cuenta" id="nro_cuenta" class="form-control" value="{{ old('nro_cuenta') }}">

                    @if ($errors->has('nro_cuenta'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nro_cuenta') }}</strong>
                        </span>
                    @endif
                </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Buscar</button>
            </div>
        </form>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop