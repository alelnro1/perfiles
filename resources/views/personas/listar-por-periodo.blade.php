@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
@stop

@section('content')
    <div class="panel-heading">Resultados del Período {{ $periodo }}</div>
    <div class="panel-body">
        @if ($historiales != false && count($historiales) > 0)
            <table class="table table-striped task-table" id="personas">
                <!-- Table Headings -->
                <thead>
                <tr>
                    <th>Fecha</th>
                    <th>Cuenta</th>
                    <th>DNI</th>
                    <th>CUIT</th>
                    <th>CUIL</th>
                    <th>Nombre</th>
                    <th>Perfil Vigente</th>
                    <th></th>
                </tr>
                </thead>

                <tbody>
                @foreach ($historiales as $historial)
                    @if (count($historial->Persona->Cuentas) > 0)
                        @foreach ($historial->Persona->Cuentas as $cuenta)
                            <tr>
                                <td>
                                    @if (date("Y", strtotime($historial->created_at)) < $periodo)
                                        01/01/{{ $periodo }}
                                    @else
                                        {{ date("d/m/Y", strtotime($historial->created_at)) }}
                                    @endif
                                </td>
                                <td>
                                    @if ($cuenta['id_sistema1'] != "")
                                        <a href="/{{ $system }}/cuentas/{{ $cuenta['id'] }}">{{ $cuenta['id_sistema1'] }}</a>
                                    @else
                                        <a href="/{{ $system }}/cuentas/{{ $cuenta['id'] }}">{{ $cuenta['id_sistema2'] }}</a>
                                    @endif
                                </td>
                                <td>{{ $historial->Persona->dni }}</td>
                                <td>{{ $historial->Persona->cuit }}</td>
                                <td>{{ $historial->Persona->cuil }}</td>
                                <td>
                                    <a href="{{ url($system . '/personas/' . $historial->Persona->id) }}">{{ $historial->Persona->nombre }}</a>
                                </td>
                                <td style="text-align:right;">{{ number_format($historial->perfil_vigente, 2) }}</td>

                                @if($historial->matriz_id)
                                <td><a href="{{ url($system . '/matrices/matriz-historica/' . $historial->id . '/' . $periodo) }}">Ver</a></td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td>
                                @if (date("Y", strtotime($historial->created_at)) < $periodo)
                                    01/01/{{ $periodo }}
                                @else
                                    {{ date("d/m/Y", strtotime($historial->created_at)) }}
                                @endif
                            </td>
                            <td>-</td>
                            <td>{{ $historial->Persona->dni }}</td>
                            <td>{{ $historial->Persona->cuit }}</td>
                            <td>{{ $historial->Persona->cuil }}</td>
                            <td>
                                <a href="{{ url($system . '/personas/' . $historial->Persona->id) }}">{{ $historial->Persona->nombre }}</a>
                            </td>
                            <td style="text-align:right;">{{ number_format($historial->perfil_vigente, 2) }}</td>
                            @if($historial->matriz_id)
                                <td><a href="{{ url($system . '/matrices/matriz-historica/' . $historial->id . '/' . $periodo) }}">Ver</a></td>
                            @endif
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
		
			{{ $historiales->setPath('por-periodo-results?_token='. $_GET['_token'] . '&periodo=' . $_GET['periodo'] )->render() }}
        @else
            NO SE ENCONTRARON PERSONAS CON ESOS CRITERIOS
        @endif

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>

    <!--script type="text/javascript">
        $(document).ready(function() {
            oTable = $('#personas').DataTable({
                iDisplayLength: 100,
                columnDefs: [
                    { orderable: false, targets: -1 }
                ],
                bFilter: true,
                "language": {
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ personas filtradas",
                    "paginate": {
                        "first":      "Primera",
                        "last":       "Ultima",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                    "lengthMenu": "Mostrar _MENU_ personas",
                    "search": "Buscar:",
                    "infoFiltered": "(de un total de _MAX_ personas)",
                }
            });
        });
    </script-->
@stop
