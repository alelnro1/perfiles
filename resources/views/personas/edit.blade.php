@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="{{ asset('/css/datepicker/bootstrap-datepicker3.min.css') }}">
    <div class="panel-heading">Persona: {{ $persona->nombre }} ({{ $descripcion_matriz }})</div>

    <div class="panel-body">
        @if(Session::has('actualizado'))
            <div class="alert alert-success">
                {{ Session::get('actualizado') }}
            </div>
        @endif

        <form action="/{{ $system }}/personas/{{ $persona->id }}" method="POST" id="nueva_persona">
            {{ method_field('PATCH') }}
            {!! csrf_field() !!}

            <fieldset>
                <legend>Modificar Matriz</legend>
                <div class="form-group {{ $errors->has('matriz_id') ? ' has-error' : '' }}">
                    <input type="hidden" name="persona_id" id="persona_id" value="{{ $persona->id }}">

                    <select name="matriz_id" id="matriz_id" class="form-control">
                        <option value="0">Seleccione una matriz...</option>
                        @foreach ($matrices as $matriz)
                            @if ($persona->matriz_id == $matriz->id)
                                <option value="{{ $matriz->id }}" selected>{{ $matriz->descripcion }}</option>
                            @else
                                <option value="{{ $matriz->id }}">{{ $matriz->descripcion }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </fieldset>

            <div class="form-group {{ $errors->has('matriz') ? ' has-error' : '' }}" id="matriz">
                <!-- Campos especiales de la matriz -->
                <fieldset>
                    <legend>Cabecera</legend>
                    @foreach ($campos as $campo)
                        @if ($campo->nombre != "perfil_vigente")
                            <div class="form-group {{ $errors->has($campo->descripcion) ? ' has-error' : '' }}">
                                <label for="campo_{{ $campo->id }}" class="control-label">{{ $campo->descripcion }}</label>

                                @if($campo->nombre == "cuit" || $campo->nombre == "cuil")

                                    <input type="text" name="{{ $campo->nombre }}" value="{{ $persona[$campo->nombre] }}" class="form-control campo" data-descripcion="{{ $campo->descripcion }}" />
                                    <i>No ingresar puntos ni guiones</i>

                                @elseif($campo->nombre == "fecha_documentacion" || $campo->nombre == "cierre_balance")

                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type='text' class="form-control campo" name="{{ $campo->nombre }}"
                                               value=
                                               "
                                               @if(strtotime($persona[$campo->nombre]) !== 0 && strtotime($persona[$campo->nombre]) > 0)
                                                {{ date("Y/m/d", strtotime($persona[$campo->nombre])) }}
                                               @endif
                                               "
                                               data-descripcion="{{ $campo->descripcion }}" readonly />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>

                                @else
                                    <input type="text" name="{{ $campo->nombre }}" value="{{ $persona[$campo->nombre] }}" class="form-control campo" data-descripcion="{{ $campo->descripcion }}" />
                                @endif
                            </div>
                        @endif
                    @endforeach

                    <div class="form-group">
                        <label for="riesgo">Riesgo</label>

                        <select name="riesgo_id" class="form-control">
                            @foreach ($riesgos as $key => $riesgo)
                                <option value="{{ $riesgo->id }}" @if ($persona->riesgo_id == $riesgo->id) selected @endif>
                                    {{ $riesgo->descripcion }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <hr style="border-top: dotted 1px;" />

                    <div>
                        <h4>PERFIL CALCULADO: <span>{{ number_format($persona->perfil_calculado, 2) }}</span></h4>
                    </div>

                    <div class="form-group{{ $errors->has('ajuste') ? ' has-error' : '' }}" style="margin-left: -14px;">
                        <h4 class="col-md-1 control-label">AJUSTE</h4>

                        <div class="col-md-3">
                            <input type="text" class="form-control" name="ajuste" value="{{ old('ajuste') }}">

                            @if ($errors->has('ajuste'))
                                <span class="help-block">
                                <strong>{{ $errors->first('ajuste') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div><br><br>

                    <div>
                        <h4>PERFIL VIGENTE: <span>{{ number_format($persona->perfil_vigente, 2) }}</span></h4>
                    </div>
                </fieldset>

                <!-- Matriz -->
                <fieldset>
                    <legend>Matriz</legend>

                    <table class="table table-striped">
                        <thead>
                        <th>Parametros</th>
                        <th>Peso relativo</th>
                        <th>Niveles de riesgo</th>
                        <th colspan="2">Peso</th>
                        </thead>

                        <tbody>
                        @foreach ($matriz_parametros as $parametro)
                            <tr class="parametro" id="{{ $parametro['id'] }}">
                                <td>{{ $parametro['descripcion'] }}</td>
                                <td>{{ $parametro['peso'] }}</td>
                                <td colspan="2">
                                    <table class="table table-striped">
                                        @foreach ($parametro['niveles'] as $nivel)
                                            <tr>
                                                <td class="col-xs-1">{{ $nivel['nivel'] }}</td>
                                                <td>{{ $nivel['descripcion'] }}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </td>

                                <td>
                                    <table class="table table-striped">
                                        @foreach ($parametro['niveles'] as $nivel)
                                            <tr>
                                                <td>{{ $nivel['peso'] }}</td>
                                                <td>
                                                    @if (isset($nivel['checked']))
                                                        <input type="radio" class="form-control nivel" name="niveles[{{$parametro['id'] }}]" value="{{ $nivel['id'] }}" style="width: 12px;" checked>
                                                    @else
                                                        <input type="radio" class="form-control nivel" name="niveles[{{$parametro['id'] }}]" value="{{ $nivel['id'] }}" style="width: 12px;">
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </fieldset>
            </div>

            <div class="alert alert-error" style="display: none;"></div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Grabar</button>
            </div>
        </form>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

@section('javascript')
    <script src="{{ asset('/js/fileupload.js')}}"></script>
    <script src="{{ asset('/js/personas.js')}}"></script>
    <script src="{{ asset('/js/datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datepicker({
                todayHighlight: true,
                endDate: '+0d',
                autoclose: true,
                format: 'yyyy/mm/dd'
            });
        });
    </script>
    @parent
@stop