@extends('layouts.app')

@section('content')
    <div class="panel-heading">Nuevo nivel</div>

    <div class="panel-body">
        <form action="/niveles" method="POST">
            {!! csrf_field() !!}

            <div class="form-group {{ $errors->has('matriz') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label">Matriz</label>

                <select name="matriz_id" id="matriz_id" class="form-control">
                    <option value="0">Seleccione una matriz...</option>
                    @foreach ($matrices as $matriz)
                        <option value="{{ $matriz->id }}">{{ $matriz->descripcion  }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group {{ $errors->has('parametro') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label">Parametro</label>

                <!-- El select va a ser completado con jQuery luego de la seleccion de la matriz -->
                <select name="parametro_id" id="parametro_id" class="form-control"></select>
            </div>

            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label">Descripcion</label>
                <input type="text" name="descripcion" id="descripcion" class="form-control" value="{{ old('descripcion') }}">

                @if ($errors->has('descripcion'))
                    <span class="help-block">
                        <strong>{{ $errors->first('descripcion') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('peso') ? ' has-error' : '' }}">
                <label for="peso" class="control-label">Peso</label>
                <input type="text" name="peso" id="peso" class="form-control" value="{{ old('peso') }}">

                @if ($errors->has('peso'))
                    <span class="help-block">
                        <strong>{{ $errors->first('peso') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Crear</button>
            </div>
        </form>
    </div>
    <script src="{{ asset('/js/niveles.js')}}"></script>
@stop


