@extends('layouts.app')

@section('content')
    <div class="panel-heading">Niveles</div>
    <div class="panel-body">
        @if (count($niveles) > 0)
            <table class="table table-striped task-table">
                <!-- Table Headings -->
                <thead>
                    <th>ID</th>
                    <th>Descripcion</th>
                    <th>Parametro</th>
                    <th>Matriz</th>
                </thead>

                <tbody>
                @foreach ($niveles as $nivel)
                    <tr>
                        <td class="table-text">
                            <div>{{ $nivel->id }}</div>
                        </td>
                        <td>
                            <div>{{ $nivel->descripcion }}</div>
                        </td>
                        <td>
                            <div>{{ $nivel->peso }}</div>
                        </td>
                        <td>
                            <div>
                                <a href="{{ url('parametros/' . $nivel->parametros->id) }}">
                                    {{ $nivel->parametros->descripcion }}
                                </a>
                            </div>
                        </td>
                        <td>
                            <a href="{{ url('niveles/' . $nivel->id) }}">Ver</a> |
                            <a href="{{ url('niveles/edit/' . $nivel->id) }}">Editar</a> |
                            <a href="{{ url('niveles/delete/' . $nivel->id) }}">Eliminar</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            NO HAY NIVELES
        @endif
    </div>
@endsection
