@extends('layouts.app')

@section('content')
    <div class="panel-heading">
        Editar Cuenta

        @if ($cuenta->id_sistema1)
            {{ $cuenta->id_sistema1 }}
        @else
            {{ $cuenta->id_sistema2 }}
        @endif
    </div>

    <div class="panel-body">
        @if(Session::has('actualizado'))
            <div class="alert alert-success">
                {{ Session::get('actualizado') }}
            </div>
        @endif

        <form class="form-horizontal" method="POST" action="/{{ $system }}/cuentas/{{ $cuenta->id }}">
            {{ method_field('PATCH') }}
            {!! csrf_field() !!}

            <div class="form-group{{ $errors->has('comercial_id') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Comercial</label>

                <div class="col-md-6">
                    <select name="comercial_id" class="form-control">
                        <option value="0">Seleccione un comercial...</option>

                        @foreach ($comerciales as $comercial)
                            @if ($cuenta->comercial_id == $comercial->id)
                                <option value="{{ $comercial->id }}" selected>{{ $comercial->nombre }}</option>
                            @else
                                <option value="{{ $comercial->id }}">{{ $comercial->nombre }}</option>
                            @endif

                        @endforeach
                    </select>

                    @if ($errors->has('comercial_id'))
                        <span class="help-block">
                                <strong>{{ $errors->first('comercial_id') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>Actualizar
                    </button>
                </div>
            </div>
        </form>
        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop