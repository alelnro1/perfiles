@extends('layouts.app')

@section('content')
    <div class="panel-heading">Nueva cuenta</div>

    <div class="panel-body">
        <form action="/cuentas" method="POST">
            {!! csrf_field() !!}

            <!--<div class="form-group {{ $errors->has('persona_id') ? ' has-error' : '' }}">
                <label for="persona_id" class="control-label">Persona</label>

                <select name="persona_id" class="form-control">
                    <option value="0">Seleccionar...</option>

                    @foreach ($personas as $persona)
                        <option value="{{ $persona->id }}">{{ $persona->nombre  }}</option>
                    @endforeach
                </select>
            </div>-->

            <div class="form-group {{ $errors->has('id_sistema1') ? ' has-error' : '' }}">
                <label for="id_sistema1" class="control-label">ID Sistema 1</label>
                <input type="text" name="id_sistema1" id="id_sistema1" class="form-control" value="{{ old('id_sistema1') }}">

                @if ($errors->has('id_sistema1'))
                    <span class="help-block">
                        <strong>{{ $errors->first('id_sistema1') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('id_sistema2') ? ' has-error' : '' }}">
                <label for="id_sistema2" class="control-label">ID Sistema 2</label>
                <input type="text" name="id_sistema2" id="id_sistema2" class="form-control" value="{{ old('id_sistema2') }}">

                @if ($errors->has('id_sistema2'))
                    <span class="help-block">
                        <strong>{{ $errors->first('id_sistema2') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('perfil') ? ' has-error' : '' }}">
                <label for="perfil" class="control-label">Perfil</label>
                <input type="text" name="perfil" id="perfil" class="form-control" value="{{ old('perfil') }}">

                @if ($errors->has('perfil'))
                    <span class="help-block">
                        <strong>{{ $errors->first('perfil') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Crear</button>
            </div>
        </form>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop