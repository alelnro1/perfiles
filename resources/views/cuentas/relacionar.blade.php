@extends('layouts.app')

@section('content')
    <div class="panel-heading">Relacion <i>persona</i> con <i>cuenta</i></div>

    <div class="panel-body">
        <form action="/cuentas/relacionar" method="POST">
            {!! csrf_field() !!}

            <div class="form-group {{ $errors->has('persona_id') ? ' has-error' : '' }}">
                <label for="persona_id" class="control-label">Persona</label>

                <select name="persona_id" class="form-control">
                    <option value="0">Seleccionar...</option>

                    @foreach ($personas as $persona)
                        <option value="{{ $persona->id }}">{{ $persona->nombre  }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group {{ $errors->has('cuenta_id') ? ' has-error' : '' }}">
                <label for="cuenta_id" class="control-label">Cuenta</label>

                <select name="cuenta_id" class="form-control">
                    <option value="0">Seleccionar...</option>

                    @foreach ($cuentas as $cuenta)
                        <option value="{{ $cuenta->id }}">{{ $cuenta->id_sistema1  }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Crear</button>
            </div>
        </form>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop