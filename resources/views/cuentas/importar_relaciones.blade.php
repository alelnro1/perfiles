@extends('layouts.app')

@section('content')
    <div class="panel-heading">Importar Relaciones Personas<->Cuentas</div>

    <div class="panel-body">
        @if (Session::has('error_sistema_invalido'))
            <div class="alert-danger alert">
                <strong>Error! </strong>
                {!! Session::get('error_sistema_invalido') !!}
            </div>
        @endif

        @if (Session::has('link_archivo'))
            <div class="alert-box success">
                <div class="alert alert-success">
                    <span aria-hidden="true" class="glyphicon glyphicon-ok"></span>
                    El archivo ha sido procesado y sus resultados pueden descargarse desde: <a href="../../{!! Session::get('link_archivo') !!}" download="../../{!! Session::get('link_archivo') !!}"><b>aca</b></a>
                </div>
            </div>
        @endif

        <form action="/{{ $system }}/cuentas/importar_relaciones" method="POST" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <div class="form-group {{ $errors->has('archivo') ? ' has-error' : '' }}">
                <label for="archivo" class="control-label">Archivo Importacion</label>

                <input type="file" name="archivo" class="form-control">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Importar</button>
            </div>
        </form>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop