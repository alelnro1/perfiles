@extends('layouts.app')

@section('content')
    <link href="{{asset('/css/fileupload.css')}}" rel="stylesheet">
    <div class="panel-heading">
        Cuenta
        @if($cuenta != "")
            @if ($cuenta->id_sistema1)
                <i>GAINVEST</i>
            @else
                <i>CIBSA</i>
            @endif
        @endif
    </div>

    <div class="panel-body">
        @if($cuenta != "")
            <table class="table table-striped task-table" style="width: 35%;">
                <tr>
                    <td>Numero de Cuenta:</td>
                    <td>
                        @if ($cuenta->id_sistema1)
                            {{ $cuenta->id_sistema1 }}
                        @else
                            {{ $cuenta->id_sistema2 }}
                        @endif
                    </td>
                </tr>

                <tr>
                    <td>Perfil de la Cuenta</td>
                    <td>{{ number_format($cuenta->perfil, 2) }}</td>
                </tr>

                <tr>
                    <td>Comercial de la Cuenta</td>
                    <td>
                        @if(isset($cuenta->comercial))
                            {{ $cuenta->comercial->nombre }}
                        @else
                            No asignado
                        @endif
                    </td>
                </tr>
            </table>

            <!-- Personas asociadas a la cuenta -->
            <fieldset>
                <legend>Personas asociadas</legend>
                @if (count($personas) > 0)
                    <table class="table table-striped">
                        <thead>
                            <th>Nombre</th>
                            <th colspan="2">Perfil vigente</th>
                        </thead>

                        <tbody>
                            @foreach ($personas as $persona)
                                <tr>
                                    <td><a href="/{{ $system }}/personas/{{ $persona->id }}">{{ $persona->nombre }}</a></td>
                                    <td>{{ number_format($persona->perfil_vigente, 2) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    NO HAY PERSONAS ASOCIADAS A LA CUENTA
                @endif
            </fieldset>

            <!-- Adjuntos -->
            <fieldset>
                @include('common.listado_adjuntos')
                @include('common.form_nuevo_adjunto', array('tipo_adjunto' => 'cuenta'))
            </fieldset>

            <!-- Alertas asociadas a la cuenta -->
            <fieldset>
                <legend>Alertas</legend>
                @if (count($alertas) > 0)
                    <table class="table table-striped">
                        <thead>
                            <th>Excedente</th>
                            <th>Estado</th>
                            <th>Oficial de Cuenta</th>
                        </thead>

                        <tbody>
                            @foreach ($alertas as $alerta)
                                <tr>
                                    <td style="float:right;">{{ number_format($alerta->excedente, 2) }}</td>
                                    <td>{{ $alerta->estado->descripcion }}</td>
                                    <td>{{ $alerta->oficial_cuenta }}</td>
                                    <td><a href="/{{ $system }}/alertas/{{ $alerta->id }}">Ver alerta...</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    LA CUENTA NO TIENE ALERTAS
                @endif
            </fieldset>
        @else
            Cuenta invalida
        @endif

        <div>
            <a href="/{{ $system }}/cuentas/{{ $cuenta->id }}/edit">Editar</a>
        </div>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>

    <script src="//cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
    <script src="{{ asset('/js/fileupload.js')}} "></script>
    <script src="{{ asset('/js/cuentas.js')}}"></script>
@stop

