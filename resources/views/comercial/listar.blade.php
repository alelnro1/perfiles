@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
@stop

@section('content')
    <div class="panel-heading">
        Comerciales
        @if(Auth::user()->hasRole('operador'))
            <div style="float:right;">
                <a href="/{{ $system }}/comercial/create">Nuevo</a>
            </div>
        @endif
    </div>
    <div class="panel-body">
        @if(Session::has('comercial_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('comercial_eliminado') }}
            </div>
        @endif

        @if(Session::has('comercial_creado'))
            <div class="alert alert-success">
                {{ Session::get('comercial_creado') }}
            </div>
        @endif

        @if (count($comerciales) > 0)
            <table class="table table-striped task-table" id="comerciales">
                <!-- Table Headings -->
                <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Telefono</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($comerciales as $comercial)
                    <tr>
                        <td>{{ $comercial->codigo }}</td>
                        <td style="width:75%">{{ $comercial->nombre }}</td>
                        <td>{{ $comercial->email }}</td>
                        <td>{{ $comercial->telefono }}</td>
                        <td>
                            <a href="/{{ $system }}/comercial/{{ $comercial['id'] }}">Ver</a>
                            |
                            <a href="/{{ $system }}/comercial/{{ $comercial['id'] }}"
                               data-method="delete"
                               data-token="{{ csrf_token() }}"
                               data-confirm="Esta seguro que desea eliminar al comercial {{ $comercial->nombre }}?">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            NO HAY COMERCIALES
        @endif

        <br>
        <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
    </div>

    <script src="{{ asset('/js/delete-link.js') }}"></script>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            oTable = $('#comerciales').DataTable({
                columnDefs: [
                    { orderable: false, targets: -1 }
                ],
                "language": {
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ comerciales filtrados",
                    "paginate": {
                        "first":      "Primera",
                        "last":       "Ultima",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                    "lengthMenu": "Mostrar _MENU_ comerciales",
                    "search": "Buscar:",
                    "infoFiltered": "(de un total de _MAX_ comerciales)",
                }
            });
        });
    </script>
@stop