@extends('layouts.app')

@section('content')
    <div class="panel-heading">Comercial</div>

    <div class="panel-body">
        <form class="form-horizontal" method="POST" action="/{{ $system }}/comercial/{{ $comercial->id }}">
            {{ method_field('PATCH') }}
            {!! csrf_field() !!}

            <div class="form-group{{ $errors->has('codigo') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Codigo</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="codigo" value="{{ $comercial->codigo }}">

                    @if ($errors->has('codigo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('codigo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Nombre</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre" value="{{ $comercial->nombre }}">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Email</label>

                <div class="col-md-6">
                    <input type="email" class="form-control" name="email" value="{{ $comercial->email }}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Telefono</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="telefono" value="{{ $comercial->telefono }}">

                    @if ($errors->has('telefono'))
                        <span class="help-block">
                        <strong>{{ $errors->first('telefono') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>Actualizar
                    </button>
                </div>
            </div>
        </form>
        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop