@extends('layouts.app')

@section('content')
    <div class="panel-heading">Comercial</div>

    <div class="panel-body">
        @if(Session::has('actualizado'))
            <div class="alert alert-success">
                {{ Session::get('actualizado') }}
            </div>
        @endif

        @if($comercial != "")
            <table class="table table-striped task-table" style="width: 35%;">
                <tr>
                    <td>Codigo</td>
                    <td>{{ $comercial->codigo }}</td>
                </tr>

                <tr>
                    <td>Nombre</td>
                    <td>{{ $comercial->nombre }}</td>
                </tr>

                <tr>
                    <td>Email</td>
                    <td>{{ $comercial->email }}</td>
                </tr>

                <tr>
                    <td>Telefono</td>
                    <td>{{ $comercial->telefono }}</td>
                </tr>
            </table>
        @else
            Comercial invalida
        @endif

        <div>
            <a href="/{{ $system }}/comercial/{{ $comercial->id }}/edit">Editar</a>
        </div>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

