@extends('layouts.app')

@section('content')
    <div class="panel-heading">Nuevo comercial</div>

    <div class="panel-body">
        <form action="/{{ $system }}/comercial" method="POST" class="form-horizontal">
            {!! csrf_field() !!}

            <div class="form-group{{ $errors->has('codigo') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Codigo</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="codigo" value="{{ old('codigo') }}">

                    @if ($errors->has('codigo'))
                        <span class="help-block">
                                <strong>{{ $errors->first('codigo') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Nombre</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="nombre" value="{{ old('nombre') }}">

                    @if ($errors->has('nombre'))
                        <span class="help-block">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Email</label>

                <div class="col-md-6">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Telefono</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="telefono" value="{{ old('telefono') }}">

                    @if ($errors->has('telefono'))
                        <span class="help-block">
                                <strong>{{ $errors->first('telefono') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>Crear
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop