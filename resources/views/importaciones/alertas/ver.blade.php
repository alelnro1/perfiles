@extends('layouts.app')

@section('content')
    <link href="{{asset('/css/fileupload.css')}}" rel="stylesheet">

    <div class="panel-heading">Importacion de Alertas con Fecha: <i><b>{{ $fecha_importacion }}</b></i></div>

    <div class="panel-body">
        <fieldset>
            <legend>Alertas Importadas</legend>

            <table class="table table-striped">
                @if (count($alertas) > 0)
                    <thead>
                        <th>Nro Cuenta</th>
                        <th>Nombre</th>
                        <th>Monto Estimado</th>
                        <th>Desvio</th>
                        <th>Importe</th>
                    </thead>

                    <tbody>
                    @foreach($alertas as $alerta)
                        <tr>
                            <td>
                                @if ($alerta->cuenta->id_sistema1)
                                    <a href="{{ url('/' . $system . '/cuentas/' . $alerta->cuenta->id) }}">{{ $alerta->cuenta->id_sistema1 }}</a>
                                @else
                                    <a href="{{ url('/' . $system . '/cuentas/' . $alerta->cuenta->id) }}">{{ $alerta->cuenta->id_sistema2 }}</a>
                                @endif
                            </td>
                            <td>{{ $alerta->nombre }}</td>
                            <td>{{ number_format($alerta->monto_estimado, 2) }}</td>
                            <td>{{ number_format($alerta->desvio, 2) }}</td>
                            <td>{{ number_format($alerta->importe, 2) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                @else
                    NO HAY ALERTAS IMPORTADAS
                @endif
            </table>
        </fieldset>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop