@extends('layouts.app')

@section('content')
    <div class="panel-heading">Importaciones de {{ $tipo_importacion }}</div>
    <div class="panel-body">
        @if (count($importaciones) > 0)
            <table class="table table-striped task-table">
                <!-- Table Headings -->
                <thead>
                    <th>Fecha Importacion</th>
                    <th>Cantidad de Registros</th>
                    <th>Realizada por</th>
                    <th colspan="2">Ver</th>
                </thead>

                <tbody>
                @foreach ($importaciones as $importacion)
                    @if(count($importacion->$tipo_importacion) > 0)
                        <tr>
                            <td class="table-text">
                                <div>{{ $importacion->created_at }}</div>
                            </td>
                            <td>
                                <div>{{ count($importacion->$tipo_importacion) }}</div>
                            </td>
                            <td>
                                <div>{{ $importacion->user['email'] }}</div>
                            </td>
                            <td>
                                <a href="{{ url("/" . $system . "/". $tipo_importacion . '/importaciones/' . $importacion->id) }}">Ver</a>
                                |
                                <a href="{{ url($importacion->link) }}">Descargar</a>
                            </td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>

            {{ $importaciones->render() }}
        @else
            NO HAY IMPORTACIONES
        @endif

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@endsection
