@extends('layouts.app')

@section('content')
    <link href="{{asset('/css/fileupload.css')}}" rel="stylesheet">

    <div class="panel-heading">Importacion de Cuentas con Fecha: <i><b>{{ $fecha_importacion }}</b></i></div>

    <div class="panel-body">
        <fieldset>
            <legend>Cuentas Importadas</legend>

            <table class="table table-striped">
                @if (count($cuentas) > 0)
                    <thead>
                        <th>Numero de cuenta</th>
                        <th>Personas Relacionadas</th>
                    </thead>

                    <tbody>
                    @foreach($cuentas as $cuenta)
                        <tr>
                            <td>
                                @if ($cuenta->id_sistema1)
                                    <a href="{{ url('/' . $system . '/cuentas/' . $cuenta->id) }}">{{ $cuenta->id_sistema1 }}</a>
                                @else
                                    <a href="{{ url('/' . $system . '/cuentas/' . $cuenta->id) }}">{{ $cuenta->id_sistema2 }}</a>
                                @endif
                            </td>
                            <td>
                                @if (count($cuenta->personas) > 0)
                                    <ul class="list-group">
                                        @foreach ($cuenta->personas as $persona)
                                             <li class="list-group-item"><a href="{{ url('/' . $system . '/personas/' . $persona->id) }}">{{ $persona->id }} ({{ $persona->nombre }})</a></li>
                                        @endforeach
                                    </ul>
                                @else
                                    No tiene
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                @else
                    NO HAY ALERTAS IMPORTADAS
                @endif
            </table>
        </fieldset>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop