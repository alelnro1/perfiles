@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
@stop

@section('content')
    <div class="panel-heading">
        Categorías Monotributo
        @if(Auth::user()->hasRole('operador'))
            <div style="float:right;">
                <a href="/{{ $system }}/configuracion/categorias-monotributo/create">Nueva</a>
            </div>
        @endif
    </div>
    <div class="panel-body">
        @if(Session::has('categoria_monotributo_eliminada'))
            <div class="alert alert-success">
                Categoría Monotributo eliminada
            </div>
        @endif

        @if(Session::has('categoria_monotributo_creada'))
            <div class="alert alert-success">
                Categoría Monotributo creada
            </div>
        @endif

        @if(Session::has('categoria_monotributo_actualizada'))
            <div class="alert alert-success">
                Categoría Monotributo actualizada
            </div>
        @endif

        @if (count($categorias_monotributo) > 0)
            <table class="table table-striped task-table" id="ahorros_edad">
                <!-- Table Headings -->
                <thead>
                <tr>
                    <th>Categoría</th>
                    <th>Ingresos</th>
                    <th></th>
                </tr>
                </thead>

                <tbody>
                @foreach ($categorias_monotributo as $categoria_monotributo)
                    <tr>
                        <td>{{ $categoria_monotributo->nombre }}</td>
                        <td>${{ $categoria_monotributo->ingresos }}</td>
                        <td>
                            <a href="{{ url('/' . $system . '/configuracion/categorias-monotributo/' . $categoria_monotributo['id'] . '/edit') }}">Editar</a>
                            |
                            <a href="{{ url('/' . $system . '/configuracion/categorias-monotributo/' . $categoria_monotributo['id']) }}"
                               data-method="delete"
                               data-token="{{ csrf_token() }}"
                               data-confirm="Esta seguro que desea eliminar?">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            NO HAY CATEGORIAS MONOTRIBUTO
        @endif

        <br>
        <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
    </div>

    <script src="{{ asset('/js/delete-link.js') }}"></script>
@endsection