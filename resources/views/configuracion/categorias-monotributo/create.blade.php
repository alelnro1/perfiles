@extends('layouts.app')

@section('content')
    <div class="panel-heading">Nueva Categoría Monotributo</div>

    <div class="panel-body">
        <form action="{{ url($system . '/configuracion/categorias-monotributo/store') }}" method="POST">
            {!! csrf_field() !!}

            <div class="form-group {{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label for="nombre" class="control-label">Categoría</label>
                <input type="text" maxlength="1" name="nombre" id="nombre" class="form-control" value="{{ old('nombre') }}" required>

                @if ($errors->has('nombre'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nombre') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('ingresos') ? ' has-error' : '' }}">
                <label for="ingresos" class="control-label">Ingresos</label>
                <input type="number" name="ingresos" id="ingresos" class="form-control" value="{{ old('ingresos') }}" required>

                @if ($errors->has('ingresos'))
                    <span class="help-block">
                        <strong>{{ $errors->first('ingresos') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Crear</button>
            </div>
        </form>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop