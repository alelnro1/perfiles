@extends('layouts.app')

@section('content')
    <div class="panel-heading">Nueva Configuración Ahorro Por Edad</div>

    <div class="panel-body">
        <form action="{{ url($system . '/configuracion/ahorro-edad/store') }}" method="POST">
        {!! csrf_field() !!}

            <div class="form-group {{ $errors->has('edad_desde') ? ' has-error' : '' }}">
                <label for="edad_desde" class="control-label">Edad Desde <small>(>)</small></label>
                <input type="number" name="edad_desde" id="edad_desde" class="form-control" value="{{ old('edad_desde') }}" required>

                @if ($errors->has('edad_desde'))
                    <span class="help-block">
                        <strong>{{ $errors->first('edad_desde') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('edad_hasta') ? ' has-error' : '' }}">
                <label for="edad_hasta" class="control-label">Edad Hasta <small>(<=)</small></label>
                <input type="number" name="edad_hasta" id="edad_hasta" class="form-control" value="{{ old('edad_hasta') }}" required>

                @if ($errors->has('edad_hasta'))
                    <span class="help-block">
                        <strong>{{ $errors->first('edad_hasta') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('porcentaje') ? ' has-error' : '' }}">
                <label for="porcentaje" class="control-label">Porcentaje</label>
                <input type="number" name="porcentaje" id="porcentaje" class="form-control" value="{{ old('porcentaje') }}" required>

                @if ($errors->has('porcentaje'))
                    <span class="help-block">
                        <strong>{{ $errors->first('porcentaje') }}</strong>
                    </span>
                @endif
            </div>


            <div class="form-group {{ $errors->has('anios') ? ' has-error' : '' }}">
                <label for="anios" class="control-label">Años</label>
                <input type="number" name="anios" id="anios" class="form-control" value="{{ old('anios') }}" required>

                @if ($errors->has('anios'))
                    <span class="help-block">
                        <strong>{{ $errors->first('anios') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Crear</button>
            </div>
        </form>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop