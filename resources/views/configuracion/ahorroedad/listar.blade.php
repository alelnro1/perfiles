@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
@stop

@section('content')
    <div class="panel-heading">
        Ahorros Por Edad
        @if(Auth::user()->hasRole('operador'))
            <div style="float:right;">
                <a href="/{{ $system }}/configuracion/ahorro-edad/create">Nuevo</a>
            </div>
        @endif
    </div>
    <div class="panel-body">
        @if(Session::has('ahorro_edad_eliminado'))
            <div class="alert alert-success">
                Ahorro por Edad eliminado
            </div>
        @endif

        @if(Session::has('ahorro_edad_creado'))
            <div class="alert alert-success">
                Ahorro por Edad creado
            </div>
        @endif

        @if(Session::has('ahorro_edad_actualizado'))
            <div class="alert alert-success">
                Ahorro por Edad actualizado
            </div>
        @endif

        @if (count($ahorros_edad) > 0)
            <table class="table table-striped task-table" id="ahorros_edad">
                <!-- Table Headings -->
                <thead>
                <tr>
                    <th>Edad Desde</th>
                    <th>Edad Hasta</th>
                    <th>Porcentaje</th>
                    <th>Años</th>
                    <th></th>
                </tr>
                </thead>

                <tbody>
                @foreach ($ahorros_edad as $ahorro_edad)
                    <tr>
                        <td>{{ $ahorro_edad->edad_desde }}</td>
                        <td>{{ $ahorro_edad->edad_hasta }}</td>
                        <td>{{ $ahorro_edad->porcentaje }}</td>
                        <td>{{ $ahorro_edad->anios }}</td>
                        <td>
                            <a href="{{ url('/' . $system . '/configuracion/ahorro-edad/' . $ahorro_edad['id'] . '/edit') }}">Editar</a>
                            |
                            <a href="{{ url('/' . $system . '/configuracion/ahorro-edad/' . $ahorro_edad['id']) }}"
                               data-method="delete"
                               data-token="{{ csrf_token() }}"
                               data-confirm="Esta seguro que desea eliminar?">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            NO HAY RANGOS PARA AHORROS POR EDAD
        @endif

        <br>
        <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
    </div>

    <script src="{{ asset('/js/delete-link.js') }}"></script>
@endsection