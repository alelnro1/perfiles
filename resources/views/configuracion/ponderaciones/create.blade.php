@extends('layouts.app')

@section('content')
    <div class="panel-heading">Nueva Ponderación</div>

    <div class="panel-body">
        <form action="{{ url($system . '/configuracion/ponderaciones/store') }}" method="POST">
            {!! csrf_field() !!}

            <div class="form-group {{ $errors->has('tipo') ? ' has-error' : '' }}">
                <label for="tipo" class="control-label">Tipo</label>
                <select name="tipo">
                    <option value="fisica">Física</option>
                    <option value="juridica">Jurídica</option>
                </select>

                @if ($errors->has('tipo'))
                    <span class="help-block">
                        <strong>{{ $errors->first('tipo') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('peso_desde') ? ' has-error' : '' }}">
                <label for="peso_desde" class="control-label">Peso Desde <small>(>)</small></label>
                <input type="number" name="peso_desde" id="peso_desde" class="form-control" value="{{ old('peso_desde') }}" required>

                @if ($errors->has('peso_desde'))
                    <span class="help-block">
                        <strong>{{ $errors->first('peso_desde') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('peso_hasta') ? ' has-error' : '' }}">
                <label for="peso_hasta" class="control-label">Peso Hasta <small>(<=)</small></label>
                <input type="number" name="peso_hasta" id="peso_hasta" class="form-control" value="{{ old('peso_hasta') }}" required>

                @if ($errors->has('peso_hasta'))
                    <span class="help-block">
                        <strong>{{ $errors->first('peso_hasta') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('ponderacion') ? ' has-error' : '' }}">
                <label for="ponderacion" class="control-label">Ponderacion</label>
                <input type="number" name="ponderacion" id="ponderacion" class="form-control" value="{{ old('ponderacion') }}" required>

                @if ($errors->has('ponderacion'))
                    <span class="help-block">
                        <strong>{{ $errors->first('ponderacion') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Crear</button>
            </div>
        </form>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop