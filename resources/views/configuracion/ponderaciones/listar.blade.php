@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
@stop

@section('content')
    <div class="panel-heading">
        Ponderaciones
        @if(Auth::user()->hasRole('operador'))
            <div style="float:right;">
                <a href="/{{ $system }}/configuracion/ponderaciones/create">Nueva</a>
            </div>
        @endif
    </div>
    <div class="panel-body">
        @if(Session::has('ponderacion_eliminada'))
            <div class="alert alert-success">
                Ponderacion eliminada
            </div>
        @endif

        @if(Session::has('ponderacion_creada'))
            <div class="alert alert-success">
                Ponderacion creada
            </div>
        @endif

        @if(Session::has('ponderacion_actualizada'))
            <div class="alert alert-success">
                Ponderacion actualizada
            </div>
        @endif

        <fieldset>
            <legend>Ponderaciones Físicas</legend>

            @include('configuracion.ponderaciones.listado-fisicas')
        </fieldset>

        <fieldset>
            <legend>Ponderaciones Jurídicas</legend>

            @include('configuracion.ponderaciones.listado-juridicas')
        </fieldset>

        <br>
        <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
    </div>

    <script src="{{ asset('/js/delete-link.js') }}"></script>
@endsection