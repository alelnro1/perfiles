@if (count($ponderaciones_fisicas) > 0)
    <table class="table table-striped task-table" id="ahorros_edad">
        <!-- Table Headings -->
        <thead>
        <tr>
            <th>Peso Desde <small>(>)</small></th>
            <th>Peso Hasta <small>(<=)</small></th>
            <th>Ponderación</th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        @foreach ($ponderaciones_fisicas as $ponderacion)
            <tr>
                <td>{{ $ponderacion->peso_desde }}</td>
                <td>{{ $ponderacion->peso_hasta }}</td>
                <td>{{ $ponderacion->ponderacion }}</td>
                <td>
                    <a href="{{ url('/' . $system . '/configuracion/ponderaciones/' . $ponderacion['id'] . '/edit') }}">Editar</a>
                    |
                    <a href="{{ url('/' . $system . '/configuracion/ponderaciones/' . $ponderacion['id']) }}"
                       data-method="delete"
                       data-token="{{ csrf_token() }}"
                       data-confirm="Esta seguro que desea eliminar?">
                        Eliminar
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    NO HAY PONDERACIONES FÍSICAS
@endif