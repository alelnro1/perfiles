@extends('layouts.app')

@section('content')
    <div class="panel-heading">Nuevo campo</div>

    <div class="panel-body">
        <form action="/campos" method="POST">
            {!! csrf_field() !!}

            <div class="form-group {{ $errors->has('matriz') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label">Matriz</label>

                <select name="matriz_id" class="form-control">
                    @foreach ($matrices as $matriz)
                        <option value="{{ $matriz->id }}">{{ $matriz->descripcion  }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group {{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label for="nombre" class="control-label">Nombre (el nombre de la columna de la tabla <i>personas</i>)</label>
                <input type="text" name="nombre" id="nombre" class="form-control" value="{{ old('nombre') }}">

                @if ($errors->has('nombre'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nombre') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                <label for="descripcion" class="control-label">Descripcion</label>
                <input type="text" name="descripcion" id="descripcion" class="form-control" value="{{ old('descripcion') }}">

                @if ($errors->has('descripcion'))
                    <span class="help-block">
                        <strong>{{ $errors->first('descripcion') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Crear</button>
            </div>
        </form>
    </div>
@stop