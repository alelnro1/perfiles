    @extends('layouts.app')

@section('content')
    <div class="panel-heading">Alertas de Cuentas</div>
    <div class="panel-body">
        @if (count($alertas) > 0)
            <table class="table table-striped task-table">
                <!-- Table Headings -->
                <thead>
                    <th>Fecha</th>
                    <th>Cuenta</th>
                    <th>Estado</th>
                    <th style="width: 165px;">Perfil (Monto estimado)</th>
                    <th>Desvio</th>
                    <th>Nombre</th>
                    <th>Contador</th>
                    <th></th>
                </thead>

                <tbody>
                @foreach ($alertas as $alerta)
                    <tr>
                        <td>{{ date("d/m/Y", strtotime($alerta->fecha_alta)) }}</td>
                        <td>
                            @if ($alerta->cuenta->id_sistema1 != 0)
                                <a href="/{{ $system }}/cuentas/{{ $alerta->cuenta->id }}">{{ $alerta->cuenta->id_sistema1 }}</a>
                            @endif
                        </td>
                        <td>
                            {{ $alerta->estado->descripcion }}
                        </td>
                        <td style="text-align: right;">
                            {{ number_format($alerta->monto_estimado, 2) }}
                        </td>
                        <td style="text-align: right;">
                            {{ number_format($alerta->desvio, 2) }}
                        </td>
                        <td>
                            {{ $alerta->nombre }}
                        </td>
                        <td>
                            {{ $alerta->contador }}
                        </td>
                        <td>
                            <a href="/{{ $system }}/alertas/{{ $alerta->id }}">Ver...</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $alertas->render() }}

            <div>
                <a href="/{{ $system }}/mailing/alertas/{{ $filtros['estado'] }}/{{ $filtros['nro_cuenta'] }}/{{ $filtros['nombre'] }}/{{ $filtros['tipo_busca_contador'] }}/{{ $filtros['contador'] }}/{{ $filtros['fecha_desde'] }}/{{ $filtros['fecha_hasta'] }}/{{ $filtros['comercial'] }}">Enviar mails</a>
            </div>

            <div>
                <a href="/{{ $system }}/alertas/exportar/{{ $filtros['estado'] }}/{{ $filtros['nro_cuenta'] }}/{{ $filtros['nombre'] }}/{{ $filtros['tipo_busca_contador'] }}/{{ $filtros['contador'] }}/{{ $filtros['fecha_desde'] }}/{{ $filtros['fecha_hasta'] }}/{{ $filtros['comercial'] }}">Exportar todos los resultados</a>
            </div>
        @else
            NO HAY ALERTAS
        @endif

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@endsection
