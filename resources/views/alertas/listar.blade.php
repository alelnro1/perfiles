@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">
@stop

@section('content')
    <div class="panel-heading">Alertas de Cuentas</div>
    <div class="panel-body">
        @if (count($alertas) > 0)
            <table class="table table-striped task-table" id="alertas">
                <!-- Table Headings -->
                <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>Cuenta</th>
                        <th>Estado</th>
                        <th style="width: 165px;">Perfil (Monto estimado)</th>
                        <th>Desvio</th>
                        <th>Nombre</th>
                        <th>Contador</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                @foreach ($alertas as $alerta)
                    <tr>
                        <td>{{ date("d/m/Y", strtotime($alerta->fecha_alta)) }}</td>
                        <td>
                            @if ($alerta->cuenta->id_sistema1 != 0)
                                <a href="/{{ $system }}/cuentas/{{ $alerta->cuenta->id }}">{{ $alerta->cuenta->id_sistema1 }}</a>
                            @endif
                        </td>
                        <td>
                            {{ $alerta->estado->descripcion }}
                        </td>
                        <td style="text-align: right;">
                            {{ number_format($alerta->monto_estimado, 2) }}
                        </td>
                        <td style="text-align: right;">
                            {{ number_format($alerta->desvio, 2) }}
                        </td>
                        <td>
                            {{ $alerta->nombre }}
                        </td>
                        <td>
                            {{ $alerta->contador }}
                        </td>
                        <td>
                            <a href="/{{ $system }}/alertas/{{ $alerta->id }}">Ver...</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div>
                <a href="/{{ $system }}/mailing/alertas/{{ $filtros['estado'] }}/{{ $filtros['nro_cuenta'] }}/{{ $filtros['nombre'] }}/{{ $filtros['tipo_busca_contador'] }}/{{ $filtros['contador'] }}/{{ $filtros['fecha_desde'] }}/{{ $filtros['fecha_hasta'] }}/{{ $filtros['comercial'] }}">Enviar mails</a>
            </div>

            <div>
                <a href="/{{ $system }}/alertas/exportar/{{ $filtros['estado'] }}/{{ $filtros['nro_cuenta'] }}/{{ $filtros['nombre'] }}/{{ $filtros['tipo_busca_contador'] }}/{{ $filtros['contador'] }}/{{ $filtros['fecha_desde'] }}/{{ $filtros['fecha_hasta'] }}/{{ $filtros['comercial'] }}">Exportar todos los resultados</a>
            </div>
        @else
            NO HAY ALERTAS
        @endif

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            oTable = $('#alertas').DataTable({
                columnDefs: [
                    { orderable: false, targets: -1 }
                ],
                bFilter: false,
                "language": {
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ alertas filtradas",
                    "paginate": {
                        "first":      "Primera",
                        "last":       "Ultima",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                    "lengthMenu": "Mostrar _MENU_ alertas",
                    "search": "Buscar:",
                    "infoFiltered": "(de un total de _MAX_ alertas)",
                }
            });
        });
    </script>
@stop
