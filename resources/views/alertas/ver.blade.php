@extends('layouts.app')

@section('content')
    <link href="{{asset('/css/fileupload.css')}}" rel="stylesheet">
    <div class="panel-heading">
        Alerta de Cuenta con numero
            <b><i>{{ $cuenta->id_sistema1 }}</i></b>

        con contador en <b><i> {{ $alerta->contador }}</i></b>
    </div>

    <div class="panel-body">
        <fieldset>
            <legend>Datos</legend>

            <div>
                <div>Fecha: {{ date("d/m/Y", strtotime($alerta->fecha_alta)) }}</div>
            </div>
        </fieldset>

        @if (Auth::user()->hasRole('operador'))
            <fieldset>
                <legend>Cambiar Estado</legend>

                <form action="/{{ $system }}/alertas/{{ $alerta->id }}" method="POST">
                    {{ method_field('PATCH') }}
                    {!! csrf_field() !!}

                    <div class="form-group {{ $errors->has('estado_id') ? ' has-error' : '' }}">
                        <label for="estado_id" class="control-label">Seleccione estado: </label>

                        <select name="estado_id" id="" class="form-control">
                            @foreach ($estados as $estado)
                                @if ($estado->id == $alerta->estado_id)
                                    <option value="{{ $estado->id }}" selected>{{ $estado->descripcion }}</option>
                                @else
                                    <option value="{{ $estado->id }}">{{ $estado->descripcion }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group {{ $errors->has('observacion') ? ' has-error' : '' }}">
                        <label for="observacion" class="control-label">Ingrese una descripcion para el cambio de estado: </label>

                        <textarea name="observacion" rows="3" class="form-control"></textarea>
                    </div>

                    @if($observacion_historico != "")
                        <div class="form-group">
                            <textarea name="observacion_historico" id="" cols="30" rows="9" class="form-control" disabled>{{ $observacion_historico }}</textarea>
                        </div>
                    @endif

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Cambiar</button>
                    </div>
                </form>
            </fieldset>
        @else
            <fieldset>
                <legend>Estado de la alerta: <i>{{ $alerta->estado->descripcion }}</i></legend>
            </fieldset>
        @endif
        <!-- Adjuntos -->
        <fieldset>
            @include('common.listado_adjuntos')
            @include('common.form_nuevo_adjunto', array('tipo_adjunto' => 'alerta'))
        </fieldset>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>


    <script src="//cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
    <script src="{{ asset('/js/fileupload.js')}} "></script>
    <script src="{{ asset('/js/cuentas.js')}}"></script>
@stop

