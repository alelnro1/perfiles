@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="{{ asset('/css/datepicker/bootstrap-datepicker3.min.css') }}">
    <div class="panel-heading">Buscar alertas</div>

    <div class="panel-body">
        <form action="/{{ $system }}/alertas/buscar" method="GET">
            <!-- Estado -->
            <div class="form-group {{ $errors->has('estado') ? ' has-error' : '' }}">
                <label for="estado" class="control-label">Estado</label>
                <select name="estado" id="estado" class="form-control">
                    <option value="0">Seleccione un estado...</option>
                    @foreach ($estados as $estado)
                        <option value="{{ $estado->id }}">{{ $estado->descripcion }}</option>
                    @endforeach
                </select>
            </div>

            <!-- Cuenta -->
            <div class="form-group {{ $errors->has('nro_cuenta') ? ' has-error' : '' }}">
                <label for="nro_cuenta" class="control-label">Numero de cuenta</label>
                <input type="text" name="nro_cuenta" id="nro_cuenta" class="form-control" value="{{ old('nro_cuenta') }}">

                @if ($errors->has('nro_cuenta'))
                    <span class="help-block">
                            <strong>{{ $errors->first('nro_cuenta') }}</strong>
                        </span>
                @endif
            </div>

            <!-- Nombre -->
            <div class="form-group {{ $errors->has('nombre') ? ' has-error' : '' }}">
                <label for="nombre" class="control-label">Nombre</label>
                <input type="text" name="nombre" id="nombre" class="form-control" value="{{ old('nombre') }}">

                @if ($errors->has('nombre'))
                    <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                @endif
            </div>

            <!-- Comercial de Cuenta -->
            <div class="form-group {{ $errors->has('comercial_cuenta') ? ' has-error' : '' }}">
                <label for="nombre" class="control-label">Comercial de Cuenta</label>
                <input type="text" name="comercial_cuenta" id="comercial_cuenta" class="form-control" value="{{ old('comercial_cuenta') }}">

                @if ($errors->has('comercial_cuenta'))
                    <span class="help-block">
                            <strong>{{ $errors->first('comercial_cuenta') }}</strong>
                        </span>
                @endif
            </div>

            <!-- Contador -->
            <div class="form-group {{ $errors->has('contador') ? ' has-error' : '' }}">
                <label for="contador" class="control-label">Contador</label>
                <br>
                <div class="input-group">
                    <select name="tipo_busca_contador" style="float:left; width: 35%;" class="form-control">
                        <option value="<"><</option>
                        <option value="=">=</option>
                        <option value=">">></option>
                    </select>

                    <input type="text" name="contador" id="contador" class="form-control" value="{{ old('contador') }}" style="float:right; width: 65%">
                </div>


                @if ($errors->has('contador'))
                    <span class="help-block">
                        <strong>{{ $errors->first('contador') }}</strong>
                    </span>
                @endif
            </div>

            <!-- Fecha Desde -->
            <div class="form-group {{ $errors->has('fecha_desde') ? ' has-error' : '' }}">
                <label for="contador" class="control-label">Desde el</label>

                <div class='input-group date col-md-6' id='datetimepicker1'>
                    <input type='text' class="form-control" name="fecha_desde" value="{{ old('fecha_desde') }}" readonly />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                </div>

                @if ($errors->has('fecha_desde'))
                    <span class="help-block">
                        <strong>{{ $errors->first('fecha_desde') }}</strong>
                    </span>
                @endif
            </div>

            <!-- Fecha Hasta -->
            <div class="form-group {{ $errors->has('fecha_hasta') ? ' has-error' : '' }}">
                <label for="contador" class="control-label">Hasta el</label>

                <div class='input-group date col-md-6' id='datetimepicker2'>
                    <input type='text' class="form-control" name="fecha_hasta" value="{{ old('fecha_hasta') }}" readonly />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                </div>

                @if ($errors->has('fecha_hasta'))
                    <span class="help-block">
                        <strong>{{ $errors->first('fecha_hasta') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Buscar</button>
            </div>
        </form>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

@section('javascript')
    <script src="{{ asset('/js/datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1, #datetimepicker2').datepicker({
                todayHighlight: true,
                endDate: '+0d',
                autoclose: true,
                format: 'yyyy-mm-dd'
            });
        });
    </script>
    @parent
@stop