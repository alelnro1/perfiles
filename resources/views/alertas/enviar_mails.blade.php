@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap-multiselect.css') }}">
@stop

@section('content')
    <div class="panel-heading">
        Enviar Mail a Comerciales
    </div>

    <div class="panel-body">
        @if(Session::has('mails_enviados'))
            <div class="alert alert-success">
                {{ Session::get('mails_enviados') }}
            </div>
        @endif

        <form action="/{{ $system }}/mailing/alertas" method="POST">
            {{ csrf_field() }}
            <!-- Filtros para hacerlas busquedas -->
            <input type="hidden" name="filtros[estado]" value="{{ $filtros['estado'] }}">
            <input type="hidden" name="filtros[nro_cuenta]" value="{{ $filtros['nro_cuenta'] }}">
            <input type="hidden" name="filtros[nombre]" value="{{ $filtros['nombre'] }}">
            <input type="hidden" name="filtros[tipo_busca_contador]" value="{{ $filtros['tipo_busca_contador'] }}">
            <input type="hidden" name="filtros[contador]" value="{{ $filtros['contador'] }}">
            <input type="hidden" name="filtros[fecha_desde]" value="{{ $filtros['fecha_desde'] }}">
            <input type="hidden" name="filtros[fecha_hasta]" value="{{ $filtros['fecha_hasta'] }}">
            <input type="hidden" name="filtros[comercial]" value="{{ $filtros['comercial'] }}">

            <table class="table table-striped task-table">
                <tr class="form-group {{ $errors->has('comerciales') ? ' has-error' : '' }}">
                    <td>Comerciales</td>
                    <td>
                        <select name="comerciales[]" id="comerciales" class="form-control" multiple="multiple">
                            @foreach ($comerciales as $comercial)
                                @if(isset($comercial->email) && trim($comercial->email) != "")
                                    @if((old('comerciales') && in_array($comercial->id, old('comerciales'))) || in_array($comercial->id, $comerciales_en_alertas))
                                        <option value="{{ $comercial->id }}" selected>{{ $comercial->nombre }}</option>
                                    @else
                                        <option value="{{ $comercial->id }}">{{ $comercial->nombre }}</option>
                                    @endif
                                @endif
                            @endforeach
                        </select>

                        @if ($errors->has('comerciales'))
                        <span class="help-block">
                            <strong>{{ $errors->first('comerciales') }}</strong>
                        </span>
                        @endif
                    </td>
                </tr>

                <tr class="form-group {{ $errors->has('asunto') ? ' has-error' : '' }}">
                    <td>Asunto</td>
                    <td>
                        <input type="text" name="asunto" class="form-control" placeholder="Escriba un asunto" value="{{ old('asunto') }}">

                        @if ($errors->has('asunto'))
                            <span class="help-block">
                            <strong>{{ $errors->first('asunto') }}</strong>
                        </span>
                        @endif
                    </td>
                </tr>

                <tr class="form-group {{ $errors->has('cuerpo') ? ' has-error' : '' }}">
                    <td>Cuerpo</td>
                    <td>
                        <textarea name="cuerpo" class="form-control" style="width:100%;"; rows="10" placeholder="Escriba el contenido del mail">{{ old('cuerpo') }}</textarea>
                    </td>
                </tr>

                <tr class="checkbox" style="width: 200px;">
                    <td>
                        <label><input type="checkbox" checked name="adjuntar">Enviar Adjunto <i>(con los resultados de la búsqueda)</i></label>
                    </td>
                </tr>

                <tr>
                    <td>
                        <button type="submit" class="btn btn-primary btn-block">Enviar</button>
                    </td>
                </tr>
            </table>
        </form>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

@section('javascript')
    <script src="{{ asset('/js/mailing.js')}}"></script>
    <script src="{{ asset('/js/bootstrap/bootstrap-multiselect.js') }}"></script>
@stop