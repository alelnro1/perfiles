@extends('layouts.app')

@section('content')
    <div class="panel-heading">Parametros</div>
    <div class="panel-body">
        @if (count($parametros) > 0)
            <table class="table table-striped task-table">
                <!-- Table Headings -->
                <thead>
                    <th>ID</th>
                    <th>Descripcion</th>
                    <th>Peso</th>
                    <th>Matriz</th>
                </thead>

                <tbody>
                @foreach ($parametros as $parametro)
                    <tr>
                        <td class="table-text">
                            <div>{{ $parametro->id }}</div>
                        </td>
                        <td>
                            <div>{{ $parametro->descripcion }}</div>
                        </td>
                        <td>
                            <div>{{ $parametro->peso }}</div>
                        </td>
                        <td>
                            <div>
                                    <a href="{{ url('matrices/' . $parametro->matrices->id) }}">
                                        {{ $parametro->matrices->descripcion }}
                                    </a>
                            </div>
                        </td>
                        <td>
                            <a href="{{ url('parametros/' . $parametro->id) }}">Ver</a> |
                            <a href="{{ url('parametros/edit/' . $parametro->id) }}">Editar</a> |
                            <a href="{{ url('parametros/delete/' . $parametro->id) }}">Eliminar</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            NO HAY PARAMETROS
        @endif
    </div>
@endsection
