@extends('layouts.app')

@section('content')
    <div class="panel-heading">
        Ver Usuario
    </div>

    <div class="panel-body">
        @if(Session::has('usuario_actualizado'))
            <div class="alert alert-success">
                {{ Session::get('usuario_actualizado') }}
            </div>
        @endif

        <!-- Personas asociadas a la cuenta -->
        <fieldset>
            <legend>Datos</legend>
                <div class="form-group">
                    <strong>Nombre:</strong> {{ $usuario->nombre }}
                </div>

                <div class="form-group">
                    <strong>Email:</strong> {{ $usuario->email }}
                </div>

                <div class="form-group">
                    <strong>Rol:</strong> {{ $usuario->rol->nombre }}
                </div>

                <div class="form-group">
                    <strong>Fecha Creacion:</strong> {{ $usuario->created_at }}
                </div>
            <a href="/{{ $system }}/usuarios/{{ $usuario->id }}/edit">Editar</a>
        </fieldset>

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop

