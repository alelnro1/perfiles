@extends('layouts.app')

@section('content')
    <div class="panel-heading">Usuario: {{ $usuario->nombre }}</div>

    <div class="panel-body">
        @if(Session::has('actualizado'))
            <div class="alert alert-success">
                {{ Session::get('actualizado') }}
            </div>
        @endif

        <form class="form-horizontal" method="POST" action="/{{ $system }}/usuarios/{{ $usuario->id }}">
            {{ method_field('PATCH') }}
                {!! csrf_field() !!}

                <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Nombre</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" name="nombre" value="{{ $usuario->nombre }}">

                        @if ($errors->has('nombre'))
                            <span class="help-block">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">E-Mail</label>

                    <div class="col-md-6">
                        <input type="email" class="form-control" name="email" value="{{ $usuario->email }}">

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password">

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Confirm Password</label>

                    <div class="col-md-6">
                        <input type="password" class="form-control" name="password_confirmation">

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('rol_id') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Rol</label>

                    <div class="col-md-6">
                        <select name="rol_id" class="form-control">
                            <option value="0">Seleccione un rol...</option>

                            @foreach ($roles as $rol)
                                @if ($usuario->rol_id == $rol->id)
                                    <option value="{{ $rol->id }}" selected>{{ $rol->nombre }}</option>
                                @else
                                    <option value="{{ $rol->id }}">{{ $rol->nombre }}</option>
                                @endif

                            @endforeach
                        </select>

                        @if ($errors->has('rol_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('rol_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-user"></i>Registrar
                        </button>
                    </div>
                </div>
            </form>
        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@stop