@extends('layouts.app')

@section('content')
    <div class="panel-heading">
        Usuarios
        <div style="width:100px; float:right;">
            <a href="/{{ $system }}/usuarios/create">Nuevo</a>
        </div>
    </div>
    <div class="panel-body">
        @if(Session::has('usuario_eliminado'))
            <div class="alert alert-success">
                {{ Session::get('usuario_eliminado') }}
            </div>
        @endif

        @if (count($usuarios) > 0)
            <table class="table table-striped task-table">
                <!-- Table Headings -->
                <thead>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th colspan="2">Rol</th>
                </thead>

                <tbody>
                @foreach ($usuarios as $usuario)
                    <tr>
                        <td>{{ $usuario->nombre }}</td>
                        <td>{{ $usuario->email }}</td>
                        <td>{{ $usuario->rol->nombre }}</td>
                        <td>
                            <a href="/{{ $system }}/usuarios/{{ $usuario['id'] }}">Ver</a>
                            |
                            <a href="/{{ $system }}/usuarios/{{ $usuario->id }}/delete" onclick="return confirm('Esta seguro que desea eliminar al usuario {{ $usuario->nombre }} ?');">Eliminar</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            NO HAY USUARIOS
        @endif

        <div>
            <a href="#" onclick="window.history.go(-1); return false;">Volver</a>
        </div>
    </div>
@endsection
