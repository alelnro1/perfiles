@extends('layouts.app')

@section('content')
    <h2 style="text-align:center;">Matriz de riesgo | <i>{{  $nombre_matriz }}</i></h2>
    <table class="table table-striped">
        <thead>
            <th>Parametros</th>
            <th>Peso relativo</th>
            <th>Niveles de riesgo</th>
            <th colspan="2">Peso</th>
        </thead>

        <tbody>
            @foreach ($matriz_parametros as $parametro)
                <tr>
                    <td>{{ $parametro['descripcion'] }}</td>
                    <td>{{ $parametro['peso'] }}</td>
                    <td colspan="2">
                        <table class="table table-striped">
                            @foreach ($parametro['niveles'] as $nivel)
                                <tr>
                                    <td class="col-xs-1">{{ $nivel['nivel'] }}</td>
                                    <td>{{ $nivel['descripcion'] }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </td>

                    <td>
                        <table class="table table-striped">
                            @foreach ($parametro['niveles'] as $nivel)
                                <tr>
                                    <td>{{ $nivel['peso'] }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@stop

