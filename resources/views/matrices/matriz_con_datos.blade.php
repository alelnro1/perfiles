<fieldset>
    <legend>Matriz</legend>

    <table class="table table-striped">
        <thead>
            <th>Parametros</th>
            <th>Peso relativo</th>
            <th>Niveles de riesgo</th>
            <th colspan="2">Peso</th>
        </thead>

        <tbody>
        @foreach ($matriz_parametros as $parametro)
            <tr>
                <td>{{ $parametro['descripcion'] }}</td>
                <td>{{ $parametro['peso'] }}</td>
                <td colspan="2">
                    <table class="table table-striped">
                        @foreach ($parametro['niveles'] as $nivel)
                            <tr>
                                <td class="col-xs-1">{{ $nivel['nivel'] }}</td>
                                <td>{{ $nivel['descripcion'] }}</td>
                            </tr>
                        @endforeach
                    </table>
                </td>

                <td>
                    <table class="table table-striped">
                        @foreach ($parametro['niveles'] as $nivel)
                            <tr>
                                <td>{{ $nivel['peso'] }}</td>
                                <td>
                                    @if (isset($nivel['checked']))
                                        <i class="glyphicon glyphicon-ok"></i>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

        <h4>PERFIL CALCULADO: <span>${{ number_format($perfil_calculado, 2, ",", ".") }}</span></h4>
        <h4>PERFIL VIGENTE: <span>${{ number_format($perfil_vigente, 2, ",", ".") }}</span></h4>
</fieldset>