@extends('layouts.app')

@section('content')
    <div class="panel-heading">Matrices</div>
    <div class="panel-body">
        @if (count($matrices) > 0)
            <table class="table table-striped task-table">
                <!-- Table Headings -->
                <thead>
                    <th>ID</th>
                    <th>Descripcion</th>
                    <th>Opciones</th>
                </thead>

                <tbody>
                @foreach ($matrices as $matriz)
                    <tr>
                        <td class="table-text">
                            <div>{{ $matriz->id }}</div>
                        </td>
                        <td>
                            <div>{{ $matriz->descripcion }}</div>
                        </td>
                        <td>
                            <a href="{{ url('matrices/' . $matriz->id) }}">Ver</a> |
                            <a href="{{ url('matrices/edit/' . $matriz->id) }}">Editar</a> |
                            <a href="{{ url('matrices/delete/' . $matriz->id) }}">Eliminar</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            NO HAY MATRICES
        @endif
    </div>
@endsection
