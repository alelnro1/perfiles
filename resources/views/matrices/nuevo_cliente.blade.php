<div class="panel-heading">Nueva matriz</div>

<div class="panel-body">
    <form action="/matrices" method="POST">
        {!! csrf_field() !!}

        <div class="form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
            <label for="descripcion" class="control-label">Descripcion</label>
            <input type="text" name="descripcion" id="descripcion" class="form-control" value="{{ old('descripcion') }}">

            @if ($errors->has('descripcion'))
                <span class="help-block">
                    <strong>{{ $errors->first('descripcion') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Crear</button>
        </div>
    </form>
</div>
