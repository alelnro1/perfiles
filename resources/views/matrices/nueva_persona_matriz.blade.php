<!-- Campos especiales de la matriz -->
<fieldset>
    <legend>Cabecera</legend>
    @foreach ($campos as $campo)
        @if ($campo->nombre != "perfil_vigente")
            <div class="form-group {{ $errors->has($campo->descripcion) ? ' has-error' : '' }}">
                <label for="campo_{{ $campo->id }}" class="control-label">{{ $campo->descripcion }}</label>

                @if($campo->nombre == "cuit" || $campo->nombre == "cuil")

                    <input type="text" name="{{ $campo->nombre }}" value="{{ $persona[$campo->nombre] }}" class="form-control campo" data-descripcion="{{ $campo->descripcion }}" />
                    <i>No ingresar puntos ni guiones</i>

                @elseif($campo->nombre == "fecha_documentacion" || $campo->nombre == "cierre_balance")

                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control campo" name="{{ $campo->nombre }}" value="{{ $persona[$campo->nombre] }}" data-descripcion="{{ $campo->descripcion }}" readonly />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>

                @else
                    <input type="text" name="{{ $campo->nombre }}" value="{{ $persona[$campo->nombre] }}" class="form-control campo" data-descripcion="{{ $campo->descripcion }}" />
                @endif
            </div>
        @endif
    @endforeach

    <div class="form-group">
        <label for="riesgo">Riesgo</label>

        <select name="riesgo_id" class="form-control">
            @foreach ($riesgos as $key => $riesgo)
                <option value="{{ $riesgo->id }}">{{ $riesgo->descripcion }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label></label>Período</label>

        <input type="text" value="{{ date("Y", time()) }}" class="form-control" disabled>
    </div>
</fieldset>

<fieldset>
    <legend>Parametros</legend>

    <table class="table table-striped">
        <!-- Matriz -->
        <thead>
            <th>Parametros</th>
            <th>Peso relativo</th>
            <th>Niveles de riesgo</th>
            <th colspan="2">Peso</th>
        </thead>

        <tbody>
        @foreach ($matriz_parametros as $parametro)
            <tr class="parametro" id="{{ $parametro['id'] }}">
                <td>{{ $parametro['descripcion'] }}</td>
                <td>{{ $parametro['peso'] }}</td>
                <td colspan="2">
                    <table class="table table-striped">
                        @foreach ($parametro['niveles'] as $nivel)
                            <tr>
                                <td class="col-xs-1">{{ $nivel['nivel'] }}</td>
                                <td>{{ $nivel['descripcion'] }}</td>
                            </tr>
                        @endforeach
                    </table>
                </td>

                <td>
                    <table class="table table-striped">
                        @foreach ($parametro['niveles'] as $nivel)
                            <tr>
                                <td>{{ $nivel['peso'] }}</td>
                                <td><input type="radio" class="form-control nivel" name="niveles[{{$parametro['id'] }}]" value="{{ $nivel['id'] }}" style="width: 12px;"></td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</fieldset>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datepicker({
            todayHighlight: true,
            endDate: '+0d',
            autoclose: true,
            format: 'yyyy/mm/dd'
        });
    });
</script>