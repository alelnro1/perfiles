<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Perfiles INTL - {{ $system }}</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    @yield('styles')

    <!-- JQuery -->
    <script src="{{ asset('/js/jquery/jquery.min.js') }}"></script>
    <link href="{{ asset('/css/app.css')}}" rel="stylesheet">

    <style>
        body {
            font-family: 'Lato';
            background: rgba(0, 0, 0, 0) url("/images/bkg_header.jpg") no-repeat scroll 0 6px / contain ;
        }

        .fa-btn {
            margin-right: 6px;
        }

        header {
            background: rgba(0, 0, 0, 0) url("/images/bkg_header.jpg") no-repeat scroll 0 6px / contain ;
            display: table;
            height: 15px;
            position: relative;
            width: 100%;
            padding-top:30px;
        }

        .panel-heading {
            font-size: 18px;
            letter-spacing: 1px;
        }

        @if($system == "gainvest")
            .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:focus, .navbar-default .navbar-nav > .open > a:hover {
                background-color: #ffae00;
                color: #555;
            }

            .navbar-default .navbar-nav > li > a {
                color: #FFF;
                font-weight: bold;
            }
        @endif
    </style>
</head>
<body id="app-layout">
    <header>
        <div class="container" style="padding-bottom: 10px">

            <a href="/{{ $system }}">
                @if($system == "gainvest")
                    <img alt="INTL GAINVEST S. A." src="/images/gainvest-logo.jpg" width="412">
                @else
                    <img alt="INTL CIBSA S. A." src="/images/cibsa-logo.jpg" width="344">
                @endif
            </a>
        </div>
    </header>

    <nav class="navbar navbar-default" @if($system == "gainvest") style="background: #ffae00 none repeat scroll 0 0" @endif>
        <div class="container">
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right navbar-nav-white"
                    @if($system == "gainvest") style="color: #FFF;" @endif>
                    <!-- Authentication Links -->
                    @if (Auth::user())
                        @if (Auth::user()->hasRole('super admin'))
                            <!-- Matrices -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    MATRICES <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url($system . '/matrices') }}">Ver todas</a></li>
                                    <li><a href="{{ url($system . '/matrices/create') }}">Nueva</a></li>
                                </ul>
                            </li>

                            <!-- Parametros -->
                            <li class="dropdown drop2">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    PARAMS <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url($system . '/parametros') }}">Ver todos</a></li>
                                    <li><a href="{{ url($system . '/parametros/create') }}">Nuevo</a></li>
                                </ul>
                            </li>

                            <!-- Niveles -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    NIVELES <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url($system . '/niveles') }}">Ver todos</a></li>
                                    <li><a href="{{ url($system . '/niveles/create') }}">Nuevo</a></li>
                                </ul>
                            </li>

                            <!-- Campos -->
                            <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                CAMPOS <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url($system . '/campos/create') }}">Nuevo</a></li>
                            </ul>
                        </li>
                        @endif

                        @if (Auth::user()->hasRole('admin'))
                            <!-- Usuarios -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    USUARIOS <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url($system . '/usuarios') }}">Ver todos</a></li>
                                    <li><a href="{{ url($system . '/usuarios/create') }}">Nuevo</a></li>
                                </ul>
                            </li>
                        @endif

                        @if (Auth::user()->hasRole('operador') || Auth::user()->hasRole('consulta'))
                            <!-- Personas -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    PERSONAS <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url($system . '/personas') }}">Por Período Actual</a></li>
                                    <li><a href="{{ url($system . '/personas/por-periodo') }}">Por Períodos Anteriores</a></li>
                                    @if (Auth::user()->hasRole('operador'))
                                        <li><a href="{{ url($system . '/personas/create') }}">Nueva</a></li>
                                    @endif
                                </ul>
                            </li>

                            <!-- Alertas -->
                            @if($system == "gainvest")
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        ALERTAS <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ url('/gainvest/alertas') }}">Buscar</a></li>

                                        @if (Auth::user()->hasRole('operador'))
                                            <li><a href="{{ url('/gainvest/alertas/importar_relaciones') }}">Importar</a></li>
                                            <li><a href="{{ url('/gainvest/alertas/importaciones') }}">Ver importaciones</a></li>
                                        @endif
                                    </ul>
                                </li>
                            @endif

                            <!-- Comercial -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    COMERCIAL <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url($system . '/comercial') }}">Ver todos</a></li>
                                    @if (Auth::user()->hasRole('operador'))
                                        <li><a href="{{ url($system . '/comercial/create') }}">Nuevo</a></li>
                                    @endif
                                </ul>
                            </li>

                        @endif

                        <!-- Cuentas -->
                        @if (Auth::user()->hasRole('operador'))
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    CUENTAS <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url($system . '/cuentas/importar_relaciones') }}">Importar relaciones</a></li>
                                    <li><a href="{{ url($system . '/cuentas/importaciones') }}">Ver importaciones</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    CONFIGURACIÓN <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url($system . '/configuracion/ahorro-edad') }}">Ahorro Por Edad</a></li>
                                    <li><a href="{{ url($system . '/configuracion/categorias-monotributo') }}">Categorías Monotributo</a></li>
                                    <li><a href="{{ url($system . '/configuracion/ponderaciones') }}">Ponderaciones</a></li>
                                </ul>
                            </li>
                        @endif

                        <!-- Opciones Perfil Actual -->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ strtoupper(Auth::user()->nombre) }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url($system . '/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Desconectarme</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

    <!-- JavaScripts -->
    <script src="{{ asset('/js/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/js/jqueryui/jqueryui.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>


@yield('javascript')

</body>
</html>
