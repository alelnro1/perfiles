$(document).ready(function(){
    $("select#matriz_id").change(function(){
        var matriz_id = $(this).find(":selected").val();

        if( matriz_id != 0 )
        {
            $('#parametro_id').empty(); // Limpio el select

            $.ajax({
                url: "/api/matriz/" + matriz_id + "/obtener_parametros",
                success: function(data){
                    $.each(data.array_parametros, function(parametro, valor){
                        $('#parametro_id').append(
                            "<option value='" + valor.id + "'>" + valor.descripcion + "</option>"
                        );
                    });
                }
            })
        }
    });
});