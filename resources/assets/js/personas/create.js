$(document).ready(function() {
    // Oculto el input de dni y cuit ni bien carga para mostrar el que corresponda segun el tipo de persona (fisica/juridica)
    $('#identificador_cuit, #identificador_dni').hide();

    $('#datetimepicker1').datepicker();

    $("select#matriz_id").change(function(){
        var matriz_id = $(this).find(":selected").val();

        // Limpio el contenido de la matriz
        $('#matriz').empty();

        // Muestro el "cargando..."
        $('#loading-indicator').show();

        if( matriz_id != 0 )
        {
            var persona_id = $('#persona_id').val();

            $.ajax({
                url: "/api/matriz/" + matriz_id + "/armar_nueva_persona_matriz/" + persona_id,
                success: function(matriz){
                    $('#loading-indicator').hide();

                    $('#matriz').html(matriz);

                    $('.alert-error').empty(); // Limpio errores existentes
                }
            })
        }
    });

    /**
     * Verifico que todos los campos hayan sido completados antes de enviar el formulario
     */
    $('form#nueva_persona').on('submit', function(e){
        e.preventDefault();
        $('.alert-error').empty();

        var error = false,
            cant_parametros = 0,
            cant_niveles_seleccionados = $('.nivel:checked').size();

        // Verifico que todos los campos se hayan completado
        /*$('.form-group > .campo').each(function(){
            if( $(this).val() == "" )
            {
                error = true;
                agregarMensajeDeError('Faltan completar el campo ' + $(this).data('descripcion') );
                $(this).parent().removeClass('has-success').addClass('has-error has-feedback');
            }
            else
            {
                $(this).parent().removeClass('has-error').addClass('has-success has-feedback').focus();
            }
        });*/

        // Verifico que haya al menos un check tildado para cada parametro
        $('.parametro').each(function(){
            cant_parametros++;
        });

        if(cant_niveles_seleccionados < cant_parametros)
        {
            error = true;
            agregarMensajeDeError('Faltan seleccionar niveles');
        }

        // No hay errores, enviar el formulario
        if( !error )
        {
            var formData = $(this).serialize(); // form data as string
            var formAction = $(this).attr('action'); // form handler url
            var formMethod = $(this).attr('method');

            $.ajax({
                type     : formMethod,
                url      : formAction,
                data     : formData,
                dataType : 'json',

                success : function(data){
                    location.href = data.redirectTo;
                },

                error : function(data){
                    var errors = data.responseJSON;

                    for(var key in errors)
                    {
                        $('input[name=' + key + ']').parent().removeClass('has-success').addClass('has-error has-feedback');
                        agregarMensajeDeError(errors[key]);
                    }
                }
            });
        }
    });

    function agregarMensajeDeError(mensaje)
    {
        $('.alert-error').append(
            "<div class='alert alert-danger'>" +
                "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>&nbsp;" + mensaje +
            "</div>"
        ).show();
    }
});