$(document).ready(function() {
    var adjunto_subido = ""; // Lista de archivos subidos que se enviara al hacer click en Adjuntar

    /* SUBIDA DEL ARCHIVO */
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
        '//jquery-file-upload.appspot.com/' : 'subir_adjunto';

    $('#form-nuevo-adjunto-persona').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            var resultado = data.result;

            // Si adjunto_subido != "" significa que ya se subio algo => no hago nada (pero se sube igual)
            if( adjunto_subido == "" ) {
                if (resultado.valid == true) {
                    $('<p/>').text(resultado.nombre_original).appendTo('#files');
                    adjunto_subido = resultado.url_adjunto_subido;
                }
            } else {
                alert("Usted ya subio un archivo");
            }
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');


    $('#mostrar_nuevo_adjunto').on('click', function(e){
        e.preventDefault();

        $("#dialog-nuevo-adjunto-persona").dialog({
            modal: true,
            width: 650,
            height: 400,
            buttons: {
                "Adjuntar": function(){
                    if($('#descripcion').val() != "" && adjunto_subido !== 0) {
                        var formData   = {
                            'adjunto'    : adjunto_subido,
                            'descripcion' : $('#descripcion').val(),
                            'persona_id'  : $('#persona_id').val(),
                            '_token'      : $('input[name=_token]').val()
                        };

                        $.ajax({
                            type     : 'POST',
                            url      : 'nuevo_adjunto',
                            data     : formData,
                            dataType : 'json',

                            success : function(data){
                                var fecha_creacion = data.created_at.date;
                                $('#lista_adjuntos').prepend(
                                    "<tr>" +
                                        "<td>" + data.descripcion + "</td>" +
                                        "<td><a href='../" + data.url + "' download='../" + data.url + "'>Ver...</a></td>" +
                                        "<td>" + fecha_creacion.substr(0, fecha_creacion.length - 7) + "</td>" +
                                        "<td>Eliminar</td>" +
                                    "</tr>"
                                );

                                $("#dialog-nuevo-adjunto-persona").dialog("close")
                            }
                        });

                        // Limpio el dialog para que pueda subir otro adjunto
                        adjunto_subido = "";
                        $('#descripcion').val("")
                    } else {
                        alert('Faltan datos !!');
                    }
                },
                "Cerrar": function () {
                    $("#dialog-nuevo-adjunto-persona").dialog("close")
                }
            }
        });
    });
});