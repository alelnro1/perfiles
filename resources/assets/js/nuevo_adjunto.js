$(document).ready(function() {
    var adjunto_subido = ""; // Lista de archivos subidos que se enviara al hacer click en Adjuntar

    /* SUBIDA DEL ARCHIVO */
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
        '//jquery-file-upload.appspot.com/' : 'subir_adjunto';

    $('#form-nuevo-adjunto').fileupload({
        url: url,
        dataType: 'json',
        add: function(e, data) {
            var uploadErrors = [];

            if(data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 2000000) { // 2MB
                uploadErrors.push('Filesize is too big');
            }

            if(uploadErrors.length > 0) {
                alert(uploadErrors.join("\n"));
            } else {
                data.submit();
            }
        },
        done: function (e, data) {
            var resultado = data.result;

            // Si adjunto_subido != "" significa que ya se subio algo => no hago nada (pero se sube igual)
            if( adjunto_subido == "" ) {
                if (resultado.valid == true) {
                    $('<p/>').text(resultado.nombre_original).appendTo('#files');
                    adjunto_subido = resultado.url_adjunto_subido;
                } else {
                    $('#files').html(resultado.error);
                }
            } else {
                alert("Usted ya subio un archivo");
            }
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        },
        fail: function (e, data) {
            $.each(data.messages, function (index, error) {
                $('<p style="color: red;">Upload file error: ' + error + '<i class="elusive-remove" style="padding-left:10px;"/></p>')
                    .appendTo('#form-nuevo-adjunto');
            });
        },
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    function clearForm()
    {
        adjunto_subido = "";
        $('#fileupload').val('');
        $('#descripcion').val('');
        $('#files').html('');
    };

    $('#mostrar_nuevo_adjunto').on('click', function(e){
        e.preventDefault();

        $("#dialog-nuevo-adjunto").dialog({
            modal: true,
            width: 650,
            height: 400,
            close: clearForm,
            buttons: {
                "Adjuntar": function(){
                    if($('#descripcion').val() != "" && adjunto_subido !== 0) {
                        var persona_id = $('#persona_id').val() | null,
                            cuenta_id  = $('#cuenta_id').val() | null,
                            alerta_id  = $('#alerta_id').val() | null;

                        var formData   = {
                            'adjunto'     : adjunto_subido,
                            'descripcion' : $('#descripcion').val(),
                            'persona_id'  : persona_id,
                            'cuenta_id'   : cuenta_id,
                            'alerta_id'   : alerta_id,
                            '_token'      : $('input[name=_token]').val()
                        };

                        $.ajax({
                            type     : 'POST',
                            url      : 'nuevo_adjunto',
                            data     : formData,
                            dataType : 'json',

                            success : function(data){
                                var fecha_creacion = data.created_at.date;

                                // Si no hay ningun adjunto, en vez de hacer prepend, redirecciono a la pagina para que aparezca la tabla
                                /*if ($('#lista_adjuntos').length > 0) {
                                    $('#lista_adjuntos').prepend(
                                        "<tr>" +
                                        "<td>" + data.descripcion + "</td>" +
                                        "<td><a href='../" + data.url + "' download='../" + data.url + "'>Ver...</a></td>" +
                                        "<td>" + fecha_creacion.substr(0, fecha_creacion.length - 7) + "</td>" +
                                        "<td><a href='#' class='eliminar_adjunto' data-persona-id='" + persona_id + "' data-cuenta-id='" + cuenta_id + "' data-alerta-id='" + alerta_id + "'>Eliminar</a></td>" +
                                        "</tr>"
                                    );
                                } else {
                                    location.href = window.location.href;
                                }*/

                                location.href = window.location.href;

                                $("#dialog-nuevo-adjunto").dialog("close")
                            }
                        });

                        // Limpio el dialog para que pueda subir otro adjunto
                        adjunto_subido = "";
                        $('#descripcion').val("");
                    } else {
                        alert('Faltan datos !!');
                    }
                },
                "Cerrar": function () {
                    $("#dialog-nuevo-adjunto").dialog("close");
                }
            }
        });
    });
});