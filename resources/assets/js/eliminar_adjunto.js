$(document).ready(function() {
    $('a.eliminar_adjunto').on('click', function(e) {
        e.preventDefault();

        if(confirm("Esta seguro que desea eliminar el adjunto?")) {
            var formData = {
                'adjunto_id': $(this).data('id'),
                'persona_id': $(this).data('persona-id'),
                'cuenta_id' : $(this).data('cuenta-id'),
                'alerta_id' : $(this).data('alerta-id'),
                '_token'    : $('input[name="_token"]').val()
            };

            $.ajax({
                type: 'POST',
                url: 'eliminar_adjunto',
                data: formData,
                dataType: 'json',
                success : function(data){
                    if( data.success == true ) {
                        //$('tr#' + data.adjunto_id).remove();
                        location.href = window.location.href;
                    } else {
                        alert('Error al eliminar el adjunto');
                    }
                }
            });
        }
    });
});