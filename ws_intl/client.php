<?php

require_once "lib/nusoap.php";

$client = new nusoap_client("http://localhost/ws_intl/server.php?wsdl");

$error = $client->getError();
if ($error) {
    echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
}

$nombres_parametros = array(
    "param1", "param2", "param3"
);

$valores_parametros = array(
    "valor1",
    "valor2",
    "valor3"
);

$result = $client->call("consultasCuentas", array($nombres_parametros, $valores_parametros));

if ($client->fault) {
    echo "<h2>Fault</h2><pre>";
    print_r($result);
    echo "</pre>";
}
else {
    $error = $client->getError();
    if ($error) {
        echo "<h2>Error</h2><pre>" . $error . "</pre>";
    }
    else {
        echo "<h2>Books</h2><pre>";
        echo $result;
        echo "</pre>";
    }
}


echo "<br><br><br>";
echo "<h2>Request</h2>";
echo "<pre>" . htmlspecialchars($client->request) . "</pre>";
echo "<h2>Response</h2>";
echo "<pre>" . htmlspecialchars($client->response) . "</pre>";