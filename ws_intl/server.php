<?php

require_once('lib/nusoap.php');
include('config.php');

/*
 * DB_SERVER
 * DB_USERNAME
 * DB_PASSWORD
 * DB_SP_NAME
 */
// exec spWEBResumenCta @NumCuotapartista=44,@FechaDesde='2008-09-01 00:00:00',@FechaHasta='2015-09-01 00:00:00',@CodCAFCI=460

function consultasCuentas($nombres_parametros, $valores_parametros)
{
    /* Conectamos a la BD */
    $dsn = "sqlsrv:Server=" . DB_SERVER . ";Database=" . DB_NAME .";";

    try {
        $dbh = new PDO($dsn, DB_USERNAME, DB_PASSWORD);
    } catch (PDOException $e) {
        return 'Connection failed: ' . $e->getMessage();
    }

    /* Preparo la llamada al SP segun la cantidad de parametros */
    $call_params = "";

    for($param = 1; $param <= count($nombres_parametros); $param++)
    {
        // Si es el ultimo => no le pongo la ,
        if ($param == count($nombres_parametros)) {
            $call_params .= "?";
        } else {
            $call_params .= "?,";
        }
    }

    // Preparo la llamada al stored procedure
    $stmt = $dbh->prepare("CALL " . DB_SP_NAME . " (" . $call_params . ")");

    // Relaciono los ? con valores y parametros recibidos desde el cliente
    for($param = 0; $param < count($nombres_parametros); $param++) {
        $stmt->bindParam($valores_parametros[$param], $nombres_parametros[$param]);
    }


    // Ejecuto el stored procedure
    $stmt->execute();

    // Veo los valores
    $resultados = $stmt->fetch();

    /*return join(",", array(
        "The WordPress Anthology: CALL login(" . $call_params .")"));*/

}

$server = new soap_server();
//$server->configureWSDL('ws');

$server->register("consultasCuentas");

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';

$server->service($HTTP_RAW_POST_DATA);