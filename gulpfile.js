var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss')
        .styles(
            [
                'blueimp-upload/jquery.fileupload.css',
                'blueimp-upload/jquery.fileupload-ui.css'
            ],
            'public/css/fileupload.css'
        )

        /*** NIVELES ***/
        .scripts(
            [
                'niveles/create.js'
            ],
            'public/js/niveles.js'
        )

        /*** PERSONAS ***/
        .scripts(
            [
                'personas/create.js',
                'nuevo_adjunto.js',
                'eliminar_adjunto.js'

            ],
            'public/js/personas.js'
        )

        /*** CUENTAS ***/
        .scripts(
            [
                'nuevo_adjunto.js',
                'eliminar_adjunto.js'
            ],
            'public/js/cuentas.js'
        )

        /*** MAILING ***/
        .scripts(
            [
                'mailing.js'
            ],
            'public/js/mailing.js'
        )

        /*** FILEUPLOAD ***/
        .scripts(
            [
                'blueimp-upload/jquery.ui.widget.js',
                'blueimp-upload/jquery.iframe-transport.js',
                'blueimp-upload/jquery.fileupload.js',
                'blueimp-upload/jquery.fileupload-validate.js',
                'blueimp-upload/jquery.fileupload-jquery-ui.js'
            ],
            'public/js/fileupload.js'
        );
    /*mix.sass('app.scss')
        .scripts(
            [
                'niveles/create.js'
            ],
            'public/js/niveles.js'
        )
        .scripts(
            [
                'personas/create.js',
                'personas/nuevo_adjunto.js'
            ],
            'public/js/personas.js'
        ).
        scriptsIn();*/
});
