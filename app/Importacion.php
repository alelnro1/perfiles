<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Importacion extends Model
{
    public $table = "importaciones";

    public $fillable = ['link', 'sistema', 'user_id'];

    public function Alertas() {
        return $this->hasMany(Alerta::class);
    }

    public function Cuentas() {
        return $this->hasMany(Cuenta::class);
    }

    public function User() {
        return $this->belongsTo(User::class);
    }
}
