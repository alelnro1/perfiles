<?php

namespace App\Classes\Configuracion;

use Illuminate\Database\Eloquent\Model;

class CategoriaMonotributo extends Model
{
    protected $table = "categorias_monotributo";

    protected $fillable = [
        'nombre', 'ingresos'
    ];

    public function setNombreAttribute($value) {
        $this->attributes['nombre'] = ucwords($value);
    }
}