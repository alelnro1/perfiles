<?php

namespace App\Classes\Configuracion;

use Illuminate\Database\Eloquent\Model;

class AhorroEdad extends Model
{
    protected $table = "configuracion_ahorro_edad";

    protected $fillable = [
        'edad_desde', 'edad_hasta', 'porcentaje', 'anios'
    ];
}