<?php

namespace App\Classes\Configuracion;

use Illuminate\Database\Eloquent\Model;

class ConfiguracionPonderacion extends Model
{
    protected $table = "configuracion_ponderacion";

    protected $fillable = [
        'tipo', 'peso_desde', 'peso_hasta', 'ponderacion'
    ];
}