<?php

namespace App\Classes\Ponderaciones;

use App\Classes\Configuracion\ConfiguracionPonderacion;

class PonderacionPersonaJuridica implements EstrategiaPonderacion
{
    public function __construct($total_pesos)
    {
        $this->total_pesos = $total_pesos;
    }

    public function calcularPonderacion()
    {
        $ponderaciones = ConfiguracionPonderacion::where('tipo', 'fisica')->get();


        foreach ($ponderaciones as $pond) {
            if ($this->total_pesos > $pond->peso_desde &&
                $this->total_pesos <= $pond->peso_hasta) {
                $ponderacion = $pond->ponderacion;
                break;
            }
        }
        /*if( $this->total_pesos < 30 )       $ponderacion = 80;

        elseif ( $this->total_pesos >= 50 ) $ponderacion = 30;

        else                         $ponderacion = 60;*/

        return $ponderacion;
    }
}