<?php

namespace App\Classes\Ponderaciones;

class Ponderacion
{
    private $estrategia;

    public function setEstrategia(EstrategiaPonderacion $estrategia)
    {
        $this->estrategia = $estrategia;
    }

    public function calcularPonderacion()
    {
        return $this->estrategia->calcularPonderacion();
    }
}