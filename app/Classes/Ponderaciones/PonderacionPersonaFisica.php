<?php

namespace App\Classes\Ponderaciones;

use App\Classes\Configuracion\ConfiguracionPonderacion;

class PonderacionPersonaFisica implements EstrategiaPonderacion
{
    public function __construct($total_pesos)
    {
        $this->total_pesos = $total_pesos;
    }

    public function calcularPonderacion()
    {
        $ponderaciones = ConfiguracionPonderacion::where('tipo', 'fisica')->get();

        foreach ($ponderaciones as $pond) {
            if ($this->total_pesos > $pond->peso_desde &&
                $this->total_pesos <= $pond->peso_hasta) {
                $ponderacion = $pond->ponderacion;
                break;
            }

        }
        /*if( $this->total_pesos < 35 )       $ponderacion = 100;

        elseif ( $this->total_pesos >= 50 ) $ponderacion = 50;

        else                         $ponderacion = 70;*/

        return $ponderacion;
    }
}