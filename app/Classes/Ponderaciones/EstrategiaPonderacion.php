<?php

namespace App\Classes\Ponderaciones;

interface EstrategiaPonderacion
{
    public function calcularPonderacion();
}