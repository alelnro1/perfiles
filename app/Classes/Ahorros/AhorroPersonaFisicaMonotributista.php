<?php

namespace App\Classes\Ahorros;

use App\Classes\Configuracion\CategoriaMonotributo;

class AhorroPersonaFisicaMonotributista extends Ahorro implements EstrategiaAhorro
{
    public function calcularAhorro()
    {
        $porcentajes = $this->getPorcentajeYAniosAhorroSegunEdad();
        $porcentaje_ahorro = $porcentajes['porcentaje'];
        $anios_ahorro = $porcentajes['anios'];
        $facturacion_anual = $this->getIngresosAnualesSegunCategoria();

        $ahorro_anual = ($facturacion_anual * $porcentaje_ahorro) / 100;

        return $ahorro_anual * $anios_ahorro;
    }

    private function getIngresosAnualesSegunCategoria()
    {
        $categoria = $this->getCategoria();

        $categorias = CategoriaMonotributo::all();

        foreach ($categorias as $cat) {
            if (ucwords($categoria) == ucwords($cat->nombre)) {
                $ingresos = $cat->ingresos;
                break;
            }
        }

        return $ingresos;

        /*switch ($categoria)
        {
            case "D": $ingresos = 96000;
                break;
            case "E": $ingresos = 144000;
                break;
            case "F": $ingresos = 192000;
                break;
            case "G": $ingresos = 240000;
                break;
            case "H": $ingresos = 288000;
                break;
            case "I": $ingresos = 400000;
                break;
            case "J": $ingresos = 470000;
                break;
            case "K": $ingresos = 540000;
                break;
            case "L": $ingresos = 600000;
                break;

            // Categorias B y C
            default:
                $ingresos = 80000;
        }*/

        //return $ingresos;
    }
}