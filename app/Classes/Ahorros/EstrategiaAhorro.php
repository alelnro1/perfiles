<?php

namespace App\Classes\Ahorros;

interface EstrategiaAhorro
{
    public function calcularAhorro();
}