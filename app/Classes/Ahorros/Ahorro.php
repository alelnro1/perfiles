<?php

namespace App\Classes\Ahorros;

use App\Classes\Configuracion\AhorroEdad;

class Ahorro
{
    private $estrategia;
    public $persona;

    public function __construct($persona)
    {
        $this->persona = $persona;
    }

    public function setEstrategia(EstrategiaAhorro $estrategia)
    {
        $this->estrategia = $estrategia;
    }

    public function calcularAhorro()
    {
        return $this->estrategia->calcularAhorro();
    }

    public function getPorcentajeYAniosAhorroSegunEdad()
    {
        //Solucion rapida
        $ahorros['porcentaje'] = "";
        $ahorros['anios'] = "";

        $ahorros_edades = AhorroEdad::all();

        foreach ($ahorros_edades as $ahorro_edad) {
            if ($this->getEdad() > $ahorro_edad->edad_desde &&
                $this->getEdad() <= $ahorro_edad->edad_hasta) {
                // Está en el rango
                $ahorros['porcentaje'] = $ahorro_edad->porcentaje;
                $ahorros['anios'] = $ahorro_edad->anios;
            }
        }

        /*//foreach ($ahorros_edades)

        dump($ahorros);
        die();

        if( $edad > 20 && $edad <= 35 )
        {
            $ahorros['porcentaje'] = 30;
            $ahorros['anios']      = 2;
        }
        elseif( $edad > 35 && $edad <= 50 )
        {
            $ahorros['porcentaje'] = 25;
            $ahorros['anios']      = 3;
        }
        elseif( $edad > 50 )
        {
            $ahorros['porcentaje'] = 25;
            $ahorros['anios']      = 3;
        }*/

        return $ahorros;
    }

    protected function getEdad()
    {
        return $this->persona->getEdad();
    }

    protected function getSueldo()
    {
        return $this->persona->getSueldo();
    }

    protected function getCategoria()
    {
        return $this->persona->getCategoria();
    }

    /**
     * Obtiene el ahorro de una persona fisica con ganancias (sueldo bruto) o con recibo de sueldo (sueldo)
     * @return float
     */
    protected function getAhorroPersonaConSueldo()
    {
        // Si es recibo de sueldo => sueldo mensual
        // Si es ganancias => sueldo anual
        $sueldo = $this->getSueldoPorTipo();

        $porcentajes = $this->getPorcentajeYAniosAhorroSegunEdad();
        $porcentaje_ahorro = $porcentajes['porcentaje'];
        $anios_ahorro = $porcentajes['anios'];

        $ahorro_anual = ($sueldo * $porcentaje_ahorro) / 100;

        return $ahorro_anual * $anios_ahorro;
    }
}