<?php

namespace App\Classes\Ahorros;

class AhorroPersonaFisicaGanancias extends Ahorro implements EstrategiaAhorro
{
    public function getSueldoPorTipo()
    {
        return $this->getSueldo();
    }

    public function calcularAhorro()
    {
        return parent::getAhorroPersonaConSueldo();
    }
}