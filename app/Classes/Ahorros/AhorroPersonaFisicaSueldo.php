<?php

namespace App\Classes\Ahorros;

class AhorroPersonaFisicaSueldo extends Ahorro implements EstrategiaAhorro
{
    public function getSueldoPorTipo()
    {
        return $this->getSueldo() * 13;
    }

    public function calcularAhorro()
    {
        return parent::getAhorroPersonaConSueldo();
    }
}
