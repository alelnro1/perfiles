<?php

namespace App\Classes\Traits;

use App\Persona;
use App\Matriz;
use App\Parametro;

trait Matrices
{
    /**
     * Muestra la matriz de una persona con los niveles seleccionados previamente
     * @param $tipo_matriz
     * @param $persona_id
     * @return mixed
     */
    public function mostrarMatrizPersona($tipo_matriz, $persona_id, $niveles_persona)
    {
        // Obtengo los datos de la matriz segun el id de persona
        if( empty($niveles_persona) ){
            //$niveles_persona = Persona::find($persona_id)->niveles;
        }

        // Muestro la matriz
        return $this->armarMatriz($tipo_matriz, false, $niveles_persona);
    }

    /* returns
     * Array (
        [0] => Array (
            [descripcion] => Actividad principal del cliente
            [niveles] => Array (
                [0] => Array (
                    [nivel] => No riesgosa
                    [peso] => 0
                )
                [1] => Array (
                    [nivel] => Riesgosa (hoteleria, casino, loteria)
                    [peso] => 20
                )
            )
        )
     )
     */
    /**
     * Crea el array matriz de una persona, tal cual lo entregó el cliente.
     * @param $matriz_id
     * @param bool $nueva_persona
     * @param array $niveles_persona si esta vacio es porque es una nueva matriz
     * @return mixed
     */
    public function armarMatriz($matriz_id, $nueva_persona = true, $niveles_persona = []){
        $cant_params = $cant_niveles = 1;

        $parametros = Parametro::with('niveles')->where('matriz_id', $matriz_id)->get();

        $matriz = [];

        // Para cada parametro armo un array de la siguiente forma ['parametro', [niveles] ]
        foreach($parametros as $parametro)
        {
            // Asigno los datos del parametro a la matriz
            $matriz[$cant_params]['id']          = $parametro->id;
            $matriz[$cant_params]['descripcion'] = $parametro->descripcion;
            $matriz[$cant_params]['peso']        = $parametro->peso;

            foreach($parametro->niveles as $nivel)
            {
                $matriz[$cant_params]['niveles'][$cant_niveles]['nivel'] = $cant_niveles;
                $matriz[$cant_params]['niveles'][$cant_niveles]['id'] = $nivel->id;
                $matriz[$cant_params]['niveles'][$cant_niveles]['descripcion'] = $nivel->descripcion;
                $matriz[$cant_params]['niveles'][$cant_niveles]['peso']  = $nivel->peso;

                if (!$nueva_persona && count($niveles_persona) > 0) {
                    if ($this->nivelEstaSeleccionado($niveles_persona, $nivel->id) ) {
                        $matriz[$cant_params]['niveles'][$cant_niveles]['checked']  = true;
                    }
                }

                $cant_niveles++;
            }

            $cant_params++;
            $cant_niveles = 1; // Reinicio para mostrar nivel 1,2,3 en la vista (en cada nivel)
        }

        return $matriz;
    }

    /**
     * Cicla por los niveles de una matriz, y verifica los niveles que tiene seleccionados una persona
     * @param $niveles_persona
     * @param $nivel_a_revisar
     * @return bool
     */
    private function nivelEstaSeleccionado($niveles_persona = [], $nivel_a_revisar)
    {
        foreach($niveles_persona as $nivel)
        {
            if( $nivel->id == $nivel_a_revisar )
            {
                return true;
            }
        }

        return false;
    }
}