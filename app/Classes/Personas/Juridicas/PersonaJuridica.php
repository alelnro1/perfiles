<?php

namespace App\Classes\Personas\Juridicas;

use App\Classes\Ponderaciones\EstrategiaPonderacion;
use App\Classes\Ponderaciones\Ponderacion;
use App\Classes\Ponderaciones\PonderacionPersonaJuridica;
use App\Persona;

class PersonaJuridica extends Persona implements EstrategiaPonderacion
{

    public function calcularPonderacion()
    {
        $ponderacion = new Ponderacion();
        $ponderacion->setEstrategia( new PonderacionPersonaJuridica( $this->total_pesos ) );

        return $ponderacion->calcularPonderacion();
    }

    private function getPatrimonioNeto()
    {
        return $this->datos->patrimonio_neto;
    }

    public function calcularPerfil()
    {
        $ponderacion = $this->calcularPonderacion();
        $patrimonio  = $this->getPatrimonioNeto();

        $perfil = ($patrimonio * $ponderacion) / 100;

        return $perfil;
    }
}