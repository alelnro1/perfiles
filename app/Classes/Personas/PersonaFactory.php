<?php

namespace App\Classes\Personas;

use App\Classes\Personas;

class PersonaFactory
{
    /**
     * Crea el objeto persona con su respectivo tipo si la clase existe
     * @param $descripcion_matriz (Ej: PPFF - Rec. Sueldo)
     * @return object
     */
    public static function crearPersona($descripcion_matriz)
    {
        $tipo_persona = PersonaFactory::getTipoPorUltimaPalabra($descripcion_matriz);

        // Defino los tipos de personas posibles
        $persona_fisica = "\App\Classes\Personas\Fisicas\PersonaFisica" . $tipo_persona;
        $persona_juridica = "\App\Classes\Personas\Juridicas\Persona" . $tipo_persona;

        if (class_exists($persona_fisica))

            return new $persona_fisica();

        elseif (class_exists($persona_juridica))

            return new $persona_juridica();

        else
            echo("El tipo de persona " . $tipo_persona . " no existe en PersonaFactory");
    }

    /**
     * Devuelve la ultima palabra de un string
     * @param $descripcion
     * @return Ultima palabra
     */
    public static function getTipoPorUltimaPalabra($descripcion)
    {
        $palabras = explode(' ', $descripcion);
        $ultima_palabra = array_pop($palabras);

        return $ultima_palabra;
    }
}