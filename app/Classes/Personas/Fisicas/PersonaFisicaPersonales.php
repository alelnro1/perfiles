<?php

namespace App\Classes\Personas\Fisicas;

use App\InterfacePersona;

class PersonaFisicaPersonales extends PersonaFisica implements InterfacePersona
{
    protected $table = 'personas';

    public function calcularPerfil()
    {
        $total_bienes_exentos = $this->getTotalBienesExentos();
        $dinero_efectivo      = $this->getDineroEfectivo();

        $ponderacion          = $this->calcularPonderacion();

        $perfil = ($total_bienes_exentos + $dinero_efectivo) * $ponderacion / 100;

        if( $perfil < 60000 )
        {
            $perfil = 60000;
        }

        return $perfil;
    }
}