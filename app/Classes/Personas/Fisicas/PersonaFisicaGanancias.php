<?php

namespace App\Classes\Personas\Fisicas;

use App\InterfacePersona;

class PersonaFisicaGanancias extends PersonaFisicaConAhorro implements InterfacePersona
{
    protected $table = 'personas';

    public function calcularPerfil()
    {
        return parent::calcularPerfilPersonaConAhorro();
    }
}