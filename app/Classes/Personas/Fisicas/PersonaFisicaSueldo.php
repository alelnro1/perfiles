<?php

namespace App\Classes\Personas\Fisicas;

use App\InterfacePersona;

class PersonaFisicaSueldo extends PersonaFisicaConAhorro implements InterfacePersona
{
    public $total_pesos;
    public $persona; // Contiene todos los datos de la persona, extraidos de la base de datos
    protected $table = 'personas';

    public function calcularPerfil()
    {
        return parent::calcularPerfilPersonaConAhorro();
    }
}