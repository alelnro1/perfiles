<?php

namespace App\Classes\Personas\Fisicas;

use App\Classes\Ponderaciones\EstrategiaPonderacion;
use App\Classes\Ponderaciones\Ponderacion;
use App\Classes\Ponderaciones\PonderacionPersonaFisica;
use App\Persona;

abstract class PersonaFisica extends Persona implements EstrategiaPonderacion
{
    public function calcularPonderacion()
    {
        $ponderacion = new Ponderacion();
        $ponderacion->setEstrategia( new PonderacionPersonaFisica( $this->total_pesos ) );

        return $ponderacion->calcularPonderacion();
    }

    public function getEdad()
    {
        return date("Y") - $this->datos->anio_nacimiento;
    }

    public function getSueldo()
    {
        return $this->datos->sueldo;
    }

    public function getCategoria()
    {
        return $this->datos->categoria_monotributo;
    }

    public function getMontoPercibido()
    {
        return $this->datos->monto_percibido;
    }

    public function getTotalBienesExentos()
    {
        return $this->datos->total_bienes_exentos;
    }

    public function getDineroEfectivo()
    {
        return $this->datos->dinero_efectivo;
    }
}