<?php

namespace App\Classes\Personas\Fisicas;

use App\InterfacePersona;

class PersonaFisicaOtros extends PersonaFisica implements InterfacePersona
{
    protected $table = 'personas';

    public function calcularPerfil()
    {
        $ponderacion     = $this->calcularPonderacion();
        $monto_percibido = $this->getMontoPercibido();

        $perfil = ($ponderacion * $monto_percibido) / 100;

        /* Si el perfil es menor al minimo, devolver el minimo, sino el calculado */
        if( $perfil < 60000 )
        {
            $perfil = 60000;
        }

        return $perfil;
    }
}