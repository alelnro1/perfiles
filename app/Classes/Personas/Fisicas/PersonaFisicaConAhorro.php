<?php

namespace App\Classes\Personas\Fisicas;

use App\Classes\Ahorros\Ahorro;
use App\Classes\Ahorros;

class PersonaFisicaConAhorro extends PersonaFisica
{
    public function calcularAhorro($clase_con_namespace)
    {
        $ahorro = new Ahorro($this);
        $clase = $this->obtenerNombreDeClaseSinNamespace($clase_con_namespace);
        $estrategia = "\App\Classes\Ahorros\Ahorro" . $clase;

        $ahorro->setEstrategia( new $estrategia($this) );

        return $ahorro->calcularAhorro();
    }

    private function obtenerNombreDeClaseSinNamespace($clase)
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * Obtiene el perfil de una persona fisica con recibo de sueldo/ganancias/monotributo
     */
    protected function calcularPerfilPersonaConAhorro()
    {
        $ponderacion = $this->calcularPonderacion();
        $ahorro      = $this->calcularAhorro(self::class);

        // El minimo del perfil es $60.000 segun las matrices
        $perfil = ($ahorro * $ponderacion) / 100;

        /* Si el perfil es menor al minimo, devolver el minimo, sino el calculado */
        if( $perfil < 60000 )
        {
            $perfil = 60000;
        }

        return $perfil;
    }
}
