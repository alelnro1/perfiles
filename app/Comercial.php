<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Cuenta;

class Comercial extends Model
{
    use SoftDeletes;

    public $table = "comercial";

    protected $fillable = ['codigo', 'nombre', 'email', 'telefono'];

    public function Cuentas() {
        return $this->hasMany(Cuenta::class);
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
