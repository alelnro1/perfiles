<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adjunto extends Model
{
    protected $fillable = ['adjunto', 'descripcion', 'cuenta_id', 'persona_id', 'alerta_id'];

    public function Personas()
    {
        return $this->belongsTo(Persona::class);
    }

    public function Cuentas()
    {
        return $this->belongsTo(Cuenta::class);
    }

    public function Alertas()
    {
        return $this->belongsTo(Alerta::class);
    }
}
