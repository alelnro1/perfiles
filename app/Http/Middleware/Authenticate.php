<?php

namespace App\Http\Middleware;

use App\Rol;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest(Config::get('ACTUAL_SYSTEM') . '/login');
            }
        }

        // Seteo el rol actual del usuario logueado
        $rol = Rol::find(Auth::user()->rol_id);
        Config::set('ACTUAL_ROL', $rol->nombre);

        return $next($request);
    }
}
