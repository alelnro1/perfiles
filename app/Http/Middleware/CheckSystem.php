<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class CheckSystem
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $path = explode("/", $request->getPathInfo());

        $system = $path[1];

        if ($system == "cibsa" || $system == "gainvest"){
            Config::set('ACTUAL_SYSTEM', $system);
            Session::put('ACTUAL_SYSTEM', $system); // Creo sesiones para cuando se envia por AJAX

            return $next($request);
        }

        return response('Pagina no encontrada', 404);
    }
}
