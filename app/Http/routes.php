<?php
//Route::get('/poblar-historial-persona-con-datos-actuales-persona/{id}/{anio}', 'PersonasController@completarMatrizHistorica');
//Route::get('/poblar-historial-persona-unica-vez/', 'PersonasController@completarMatrizHistorica_unica_vez');
Route::get('/poblar-historial-sin-acualizaciones/{anio}', 'PersonasController@completarMatrizHistorica_unica_vez_al_anio')/*->name('metodo-anual-actualizacion')*/;

$super_admin_routes = function() {
    Route::resource('matrices',      'MatricesController');
    Route::resource('parametros',    'ParametrosController');
    Route::resource('niveles',       'NivelesController');
    Route::resource('campos',        'CamposController');
    Route::resource('comercial',     'ComercialController');
    Route::resource('usuarios',      'UsersController', ['except' => 'store']);

    // Todo lo que venga de /api/ viene por JS
    Route::group(['prefix' => 'api'], function(){
        Route::get('/matriz/{id}/obtener_parametros',               'MatricesController@obtenerParametros');
        //Route::get('/matriz/{id}/armar_nueva_persona_matriz/{persona_id}',   'MatricesController@matrizNuevaPersona');
    });
};

Route::group(['middleware' => ['web']], function () use ($super_admin_routes) {
    /******************** Rutas que no necesitan verificar de que sistema se viene **************************/
    Route::post('/gainvest/login', 'Auth\AuthController@login');
    Route::post('/cibsa/login', 'Auth\AuthController@login');

    // Todo lo que venga de /api/ viene por JS1
    Route::group(['prefix' => 'api'], function(){
        Route::get('/matriz/{id}/obtener_parametros',               'MatricesController@obtenerParametros');
        Route::get('/matriz/{id}/armar_nueva_persona_matriz/{persona_id}',   'MatricesController@matrizNuevaPersona');
    });

    Route::group(['middleware' => ['auth', 'roles:super admin']], function() use ($super_admin_routes) { // Debe estar logueado y ser super admin
        $super_admin_routes();
    });

    /************** Rutas que necesitan verificar del sistema que viene *****************/
    Route::group(['middleware' => ['system'], 'prefix' => '{system?}'],  function() use ($super_admin_routes) {
        Route::auth();

        Route::get('/', 'HomeController@index');
        Route::get('/home', 'HomeController@index');

        // Roles
        Route::group(['prefix' => 'roles'], function(){
            Route::get('create',       'RolesController@nuevo');
            Route::post('create', 		'RolesController@create');
        });

        // Se necesita ser SUPER ADMIN
        Route::group(['middleware' => ['auth', 'roles:super admin']], function() use ($super_admin_routes) { // Debe estar logueado y ser super admin
            $super_admin_routes();
        });

        // Se necesita ser ADMIN
        Route::group(['middleware' => ['auth', 'roles:admin']], function(){ // Debe estar logueado y ser super admin
            // GESTIONAR USUARIOS
            Route::get('usuarios', 'UsersController@index');

            Route::resource('usuarios', 'UsersController', ['except' => 'store']);

            Route::post('usuarios/store', 'UsersController@store');

            Route::get('usuarios/{id}/delete', 'UsersController@destroy');
        });

        // Se necesita ser OPERADOR
        Route::group(['middleware' => ['auth', 'roles:operador']], function(){ // Debe estar logueado y ser super admin
            // Relacionar
            Route::get('alertas/importar_relaciones', 'AlertasController@importarRelaciones');
            Route::post('alertas/importar_relaciones', 'AlertasController@procesarImportacionRelaciones');

            // Mailing
            Route::get('mailing/alertas/{estado}/{nro_cuenta}/{nombre}/{tipo_busca_contador}/{contador}/{fecha_desde}/{fecha_hasta}/{comercial}', 'AlertasController@mailingForm');
            Route::post('mailing/alertas', 'MailingController@mailingAlertas');

            // Exportaciones
            Route::get('alertas/exportar/{estado}/{nro_cuenta}/{nombre}/{tipo_busca_contador}/{contador}/{fecha_desde}/{fecha_hasta}/{comercial}', 'ExportacionesController@exportarAlertas');
            Route::get('personas/exportar/{dni}/{cuit}/{cuil}/{nombre}/{nro_cuenta}', 'ExportacionesController@exportarPersonas');

            Route::get('cuentas/importar_relaciones', 'CuentasController@importarRelaciones');
            Route::post('cuentas/importar_relaciones', 'CuentasController@procesarImportacionRelaciones');

            // Ver Importaciones
            Route::get('alertas/importaciones', 'ImportacionesController@listarAlertas');
            Route::get('alertas/importaciones/{id}', 'ImportacionesController@verAlertasDeImportacion');
            Route::get('cuentas/importaciones', 'ImportacionesController@listarCuentas');
            Route::get('cuentas/importaciones/{id}', 'ImportacionesController@verCuentasDeImportacion');

            // Asignar matriz a persona si no tiene ninguna asignada
            Route::post('personas/{id}/asignarMatriz', 'PersonasController@asignarMatriz');

            // Buscador de personas por periodo
            Route::get('personas/por-periodo', 'PersonasController@buscarPersonasPorPeriodoForm');
            Route::get('personas/por-periodo-results', 'BuscadorPersonasController@buscarPersonasPorPeriodo');

            /* Adjuntos */
            // Alertas
            Route::post('alertas/subir_adjunto' ,    'AdjuntosController@subirAdjunto');
            Route::post('alertas/nuevo_adjunto',     'AdjuntosController@nuevoAdjunto');
            Route::post('alertas/eliminar_adjunto',  'AdjuntosController@eliminarAdjunto');
            // Personas
            Route::post('personas/subir_adjunto' ,    'AdjuntosController@subirAdjunto');
            Route::post('personas/nuevo_adjunto',     'AdjuntosController@nuevoAdjunto');
            Route::post('personas/eliminar_adjunto',  'AdjuntosController@eliminarAdjunto');

            // Cuentas
            Route::post('cuentas/subir_adjunto' , 'AdjuntosController@subirAdjunto');
            Route::post('cuentas/nuevo_adjunto',  'AdjuntosController@nuevoAdjunto');
            Route::post('cuentas/eliminar_adjunto',  'AdjuntosController@eliminarAdjunto');
            $only = ['create', 'store', 'edit', 'update', 'destroy']; // Defino las unicas acciones que quiero que se permitan siendo operador, no todo el resource
            Route::resource('personas',      'PersonasController',
                ['only' => $only]);
            Route::resource('alertas',       'AlertasController',
                ['only' => $only]);
            Route::resource('comercial',     'ComercialController',
                ['only' => $only]);
            Route::resource('cuentas',       'CuentasController',
                ['only' => $only]);

            Route::group(['prefix' => 'configuracion'], function () {
                Route::group(['prefix' => 'ahorro-edad'], function () {
                    Route::get('/', 'Configuracion\AhorroEdadController@listar');
                    Route::get('create', 'Configuracion\AhorroEdadController@create');
                    Route::get('{id}/edit', 'Configuracion\AhorroEdadController@edit');
                    Route::post('store', 'Configuracion\AhorroEdadController@store');
                    Route::patch('{id}', 'Configuracion\AhorroEdadController@update');
                    Route::delete('{id}', 'Configuracion\AhorroEdadController@delete');
                });

                Route::group(['prefix' => 'categorias-monotributo'], function() {
                    Route::get('/', 'Configuracion\CategoriasMonotributoController@listar');
                    Route::get('create', 'Configuracion\CategoriasMonotributoController@create');
                    Route::get('{id}/edit', 'Configuracion\CategoriasMonotributoController@edit');
                    Route::post('store', 'Configuracion\CategoriasMonotributoController@store');
                    Route::patch('{id}', 'Configuracion\CategoriasMonotributoController@update');
                    Route::delete('{id}', 'Configuracion\CategoriasMonotributoController@delete');
                });

                Route::group(['prefix' => 'ponderaciones'], function() {
                    Route::get('/', 'Configuracion\PonderacionesController@listar');
                    Route::get('create', 'Configuracion\PonderacionesController@create');
                    Route::get('{id}/edit', 'Configuracion\PonderacionesController@edit');
                    Route::post('store', 'Configuracion\PonderacionesController@store');
                    Route::patch('{id}', 'Configuracion\PonderacionesController@update');
                    Route::delete('{id}', 'Configuracion\PonderacionesController@delete');
                });
            });
        });

        // Se necesita ser CONSULTA
        Route::group(['middleware' => ['auth', 'roles:operador,consulta']], function(){ // Debe estar logueado y ser super admin

            // Personas
            Route::get('personas', 'PersonasController@index');
            Route::get('personas/buscar', 'PersonasController@buscar');
            Route::get('personas/{id}', 'PersonasController@show');

            // Alertas
            Route::get('alertas/buscar',  'AlertasController@buscar');
            Route::get('alertas',  'AlertasController@index');
            Route::get('alertas/{id}', 'AlertasController@show');

            // Cuentas
            Route::get('cuentas/buscar',  'CuentasController@buscar');
            Route::get('cuentas',  'CuentasController@index');
            Route::get('cuentas/{id}', 'CuentasController@show');

            Route::get('matrices/matriz-historica/{id}', 'MatricesController@matrizHistorica');
            Route::get('matrices/matriz-historica/{id}/{periodo}', 'MatricesController@matrizHistorica');
        });
    });
});













