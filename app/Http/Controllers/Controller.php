<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\View;
use PHPExcel_Style_Fill;
use Illuminate\Support\Facades\Config;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $coleccion_resultados; // Resultado de la busqueda de obtenerColeccionDeAPartes()

    public function __construct()
    {
        $this->system = Config::get('ACTUAL_SYSTEM');

        $this->coleccion_resultados = [];

        View::share('system', $this->system);
    }

    public function esGainvest()
    {
        return $this->system == "gainvest";
    }

    public function esCibsa()
    {
        return $this->system == "cibsa";
    }

    /**
     * Dice si el sistema al que intenta acceder es el correcto
     * @param $sistema_al_que_intenta_acceder
     * @return bool
     */
    public function sistemaCorrecto($sistema_al_que_intenta_acceder)
    {

        // A veces recibo numeros de sistemas => traducirlos
        if ($sistema_al_que_intenta_acceder == 1) $sistema_al_que_intenta_acceder = "gainvest";
        if ($sistema_al_que_intenta_acceder == 2) $sistema_al_que_intenta_acceder = "cibsa";

        if ($sistema_al_que_intenta_acceder == $this->system) {
            return true;
        }

        return false;
    }

    /**
     * Filtra una coleccion ya filtrada por otros parametros.
     * Ej (en alertas): filtra primero por oficial_cuenta y da un resultado; a ese resultado lo filtra por el siguiente parametro, y da otro array, etc
     */
    public function filtrarColeccionYaFiltrada($nombres_filtros = [], $valores_filtros = [], $coleccion)
    {
        foreach ($valores_filtros as $key => $valor_filtro) {
            if ($valor_filtro != '-') {
                $nombre_filtro = $nombres_filtros[$key];

                $coleccion = $coleccion->filter(function ($item) use ($nombre_filtro, $valor_filtro) {
                    if (strpos(strtolower(trim($item->$nombre_filtro)), strtolower(trim($valor_filtro))) !== false) return $item;
                });
            }
        }

        return $coleccion;
    }

    /**
     * El limite de SQL Server son 2100, asi que selecciono de a $cantidad, y los meto en un array
     * @param $coleccion
     * @param $cantidad
     */
    public function obtenerColeccionDeAPartes($coleccion, $cantidad = 2000)
    {

        $coleccion->chunk(100, function ($personas) {
            foreach ($personas as $persona) {
                array_push($this->coleccion_resultados, $persona);
            }
        });

        return $this->coleccion_resultados;
    }

    /**
     * Escribe en una celda del xls el resultado de la importacion de las relaciones de las personas con las cuentas
     * @param $reader
     * @param $num_col_actual el numero de columna a formatear
     * @param $num_row_actual el numero de row a formatear
     * @param $msg el mensaje a escribir en la celda
     * @param $color el color de fondo de la celda
     */
    public function escribirResultadoCeldaConEstilo($reader, $ult_col, $num_row_actual, $msg, $background_color, $font_color)
    {
        $sheet = $reader->getExcel()->setActiveSheetIndex(0);
        $actual_row = $ult_col . $num_row_actual;
        $sheet->setCellValue($actual_row, $msg)
            ->getStyle($actual_row)
            ->applyFromArray(array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => $background_color)
                ),
                "font" => array(
                    "bold" => true,
                    "color" => array("rgb" => $font_color)
                ),
            ));
    }

    /**
     * Obtiene el numero del sistema (gainvest o cibsa, 1 o 2)
     * @param $archivo el archivo de importacion
     * @return int el numero de sistema
     */
    public function obtenerNumeroDeSistema($archivo = null)
    {
        // Si se recibio un archivo, obtengo el sistema desde ese nombre, sino desde la variable de entorno
        if ($archivo)
            $nombre_original = strtolower($archivo->getClientOriginalName());
        else
            $nombre_original = $this->system;

        $primeros_tres_chars = substr($nombre_original, 0, 3);

        // Detecto el sistema
        if ($primeros_tres_chars == "gai") { // Hablamos de GAINVEST
            $num_sistema = "1";
        } else if ($primeros_tres_chars == "cib") { // Hablamos de CIBSA
            $num_sistema = "2";
        } else { // Error
            $num_sistema = false;
        }

        return $num_sistema;
    }

    public function agregarMascaraACampos($persona)
    {
        if (isset($persona->sueldo))
            $persona->sueldo = '$' . number_format((double)$persona->sueldo, 2, ',', '.');
        if (isset($persona->monto_percibido))
            $persona->monto_percibido = '$' . number_format((double)$persona->monto_percibido, 2, ',', '.');
        if (isset($persona->total_bienes_exentos))
            $persona->total_bienes_exentos = '$' . number_format((double)$persona->total_bienes_exentos, 2, ',', '.');
        if (isset($persona->dinero_efectivo))
            $persona->dinero_efectivo = '$' . number_format((double)$persona->dinero_efectivo, 2, ',', '.');
        if (isset($persona->patrimonio_caja_y_bancos))
            $persona->patrimonio_caja_y_bancos = '$' . number_format((double)$persona->patrimonio_caja_y_bancos, 2, ',', '.');
        if (isset($persona->patrimonio_inversiones))
            $persona->patrimonio_inversiones = '$' . number_format((double)$persona->patrimonio_inversiones, 2, ',', '.');
        if (isset($persona->patrimonio_activo_corriente))
            $persona->patrimonio_activo_corriente = '$' . number_format((double)$persona->patrimonio_activo_corriente, 2, ',', '.');
        if (isset($persona->patrimonio_pasivo_corriente))
            $persona->patrimonio_pasivo_corriente = '$' . number_format((double)$persona->patrimonio_pasivo_corriente, 2, ',', '.');
        if (isset($persona->patrimonio_neto))
            $persona->patrimonio_neto = '$' . number_format((double)$persona->patrimonio_neto, 2, ',', '.');
        if (isset($persona->ventas))
            $persona->ventas = '$' . number_format((double)$persona->ventas, 2, ',', '.');
        if (isset($persona->resultado_bruto))
            $persona->resultado_bruto = '$' . number_format((double)$persona->resultado_bruto, 2, ',', '.');
        if (isset($persona->dni)) {

            if (isset($persona->dni) && !empty($persona->dni))
                if (count(str_split($persona->dni)) == 8) {
                    $persona->dni = vsprintf('%s%s.%s%s%s.%s%s%s', str_split($persona->dni));
                }
            if (count(str_split($persona->dni)) == 7) {
                $persona->dni = vsprintf('%s.%s%s%s.%s%s%s', str_split($persona->dni));
            }


        }
        if (isset($persona->cuit) && !empty($persona->cuit)) {


            if (count(str_split($persona->cuit))== 11) {
                $persona->cuit = vsprintf('%s%s-%s%s%s%s%s%s%s%s-%s', str_split($persona->cuit));
            }
            if (count(str_split($persona->cuit))== 10) {
                $persona->cuit = vsprintf('%s%s-%s%s%s%s%s%s%s-%s', str_split($persona->cuit));
            }
        }
        if (isset($persona->cuit) && !empty($persona->cuil)) {
        if (count(str_split($persona->cuil)) == 11) {
            $persona->cuil = vsprintf('%s%s-%s%s%s%s%s%s%s%s-%s', str_split($persona->cuil));
        }

        if (count(str_split($persona->cuil)) == 10) {
            $persona->cuil = vsprintf('%s%s-%s%s%s%s%s%s%s-%s', str_split($persona->cuil));
        }}
        return $persona;
    }
}
