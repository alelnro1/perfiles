<?php

namespace App\Http\Controllers;

use App\Classes\Traits\Matrices;
use App\HistorialPersona;
use App\Nivel;
use App\Parametro;
use App\Riesgo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Matriz;
use App\Persona;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Validator;
use App\Http\Controllers\Controller;

class MatricesController extends Controller
{
    use Matrices;

    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matrices = Matriz::all();

        return view('matrices.listar', compact('matrices', $matrices) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('matrices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|max:100|unique:matrices'
        ]);

        Matriz::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($matriz_id)
    {
        $matriz = $this->armarMatriz($matriz_id);

        $nombre_matriz = Matriz::find($matriz_id);

        return view('matrices/ver', array('matriz_parametros' => $matriz, 'nombre_matriz' => $nombre_matriz->descripcion) );
    }

    /**
     * Arma la matriz con los camposcorrespondientes para crear una nueva persona
     * @param $matriz_id
     * @return View
     */
    public function matrizNuevaPersona($matriz_id, $persona_id){
        if ($persona_id != "undefined") { // Quiere editar
            return $this->matrizEditarPersona($matriz_id, $persona_id);
        } else { // Nueva persona

            $matriz = $this->armarMatriz($matriz_id);
            $campos = Matriz::find($matriz_id)->campos;
            $riesgos = Riesgo::where('sistema', '=', Session::get('ACTUAL_SYSTEM'))->get();

            return view('matrices.nueva_persona_matriz', [
                'matriz_parametros' => $matriz,
                'campos' => $campos,
                'persona' => null,
                'riesgos' => $riesgos
            ]);
        }
    }

    public function matrizEditarPersona($matriz_id, $persona_id)
    {
        $persona = Persona::find($persona_id);
        $matriz  = Matriz::find($matriz_id);
        $riesgos = Riesgo::where('sistema', '=', Session::get('ACTUAL_SYSTEM'))->get();
        $campos  = $matriz->campos;

        $matriz_editar = $this->mostrarMatrizPersona($matriz_id, true, null);

        return view('matrices.nueva_persona_matriz', [
            'matriz_parametros' => $matriz_editar,
            'campos' => $campos,
            'persona' => $persona,
            'riesgos' => $riesgos
        ]);
    }

    public function matrizHistorica($key, $matriz_historica_id, $periodo=null)
    {
        $datos_matriz_historica  = HistorialPersona::find($matriz_historica_id);

        if (!$datos_matriz_historica) abort(404);

        $persona = Persona::find($datos_matriz_historica->persona_id);
        $campos  = Matriz::find($persona->matriz_id)->campos;

        $historial_anterior = HistorialPersona::where('persona_id',$persona->id)->where('id','<',$matriz_historica_id)->orderby('id','DESC')->first();



        $descripcion_matriz_historica = Matriz::find($datos_matriz_historica->matriz_id);

        $riesgo_historico = $this->getRiesgoHistorico($datos_matriz_historica);

        $matriz_historica = $this->mostrarMatrizPersona($datos_matriz_historica->matriz_id, $datos_matriz_historica->persona_id, json_decode($datos_matriz_historica->niveles));
        $fecha_actualizacion = $datos_matriz_historica->updated_at;
        $persona->perfil = $datos_matriz_historica->perfil;

        $persona = $this->agregarMascaraACampos($persona);

        $perfil_calculado = $datos_matriz_historica->perfil_calculado;
        $perfil_vigente = $datos_matriz_historica->perfil_vigente;

        return view('personas.ver_matriz_historica', [
            'historial_anterior' => $historial_anterior,
            'datos_matriz_historica' => $datos_matriz_historica,
            'perfil_calculado' => $perfil_calculado,
            'perfil_vigente' => $perfil_vigente,
            'matriz_parametros' => $matriz_historica,
            'campos' => $campos,
            'persona' => $persona,
            'riesgo_historico' => $riesgo_historico,
            'fecha_actualizacion' => $fecha_actualizacion,
            'descripcion_matriz' => $descripcion_matriz_historica->descripcion,
            'periodo' => $periodo
        ]);
    }


    /**
     * Devuelve los parametros de una matriz.
     *
     * @param  int  $id
     * @return Array
     */
    public function obtenerParametros($matriz_id)
    {
        $parametros = Matriz::find($matriz_id)->parametros()->where('matriz_id', $matriz_id)->get();

        return response()
                    ->json([
                        'array_parametros' => $parametros
                    ]);
    }

    /**
     * Devuelve el objeto Riesgo de la matriz historica
     * @param $datos_matriz_historica
     */
    private function getRiesgoHistorico($datos_matriz_historica)
    {
        $riesgo = null;

        // Tiene riesgo historico
        if ($datos_matriz_historica->Riesgo) {
            $riesgo = $datos_matriz_historica->Riesgo;
        } else {
            // No tiene riesgo historico => devuelve el default del sistema
            $riesgo = Riesgo::dameRiesgoDefault(Session::get('ACTUAL_SYSTEM'));
        }

        return $riesgo;
    }
}
