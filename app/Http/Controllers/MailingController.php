<?php

namespace App\Http\Controllers;

use App\Comercial;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;


class MailingController extends ExportacionesController
{
    private $buscador_alertas;
    private $buscador_personas;

    public function __construct()
    {
        $this->buscador_alertas  = new BuscadorAlertasController();
        $this->buscador_personas = new BuscadorPersonasController();

        parent::__construct();
    }

    public function mailingAlertas($key, Request $request)
    {
        $this->validate($request, [
            'comerciales' => 'required',
            'asunto' => 'required'
        ]);

        $filtros        = $request->filtros;
        $asunto         = $request->asunto;
        $cuerpo         = $request->cuerpo;
        $comerciales_id = $request->comerciales;

        // Busco los comerciales

        $comerciales = Comercial::findMany($comerciales_id);

        // Si quiere adjuntar => creo el excel y devuelvo la url del adjunto, sino solo false
        if (isset($request->adjuntar) && $request->adjuntar == "on") {
            $alertas = $this->buscarAlertasParaExportar($key, $filtros['estado'], $filtros['nro_cuenta'], $filtros['nombre'], $filtros['tipo_busca_contador'], $filtros['contador'], $filtros['fecha_desde'], $filtros['fecha_hasta'], $filtros['comercial']);

            $excel_adjunto = $this->crearExcelAlertas($alertas, false);
        } else {
            $excel_adjunto = false;
        }

        Mail::raw($cuerpo, function ($message) use ($asunto, $excel_adjunto, $comerciales){
            $message->subject($asunto);
            $message->from("no-reply@" . $this->system . ".com", $this->system);

            foreach ($comerciales as $comercial)
            {
                $message->bcc($comercial->email);
            }

            if ($excel_adjunto)
                $message->attach($excel_adjunto['full']);
        });

        return Redirect::back()->with('mails_enviados', 'El mail ha sido enviado a los comerciales seleccionados');
    }
}

