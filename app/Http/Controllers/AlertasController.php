<?php

namespace App\Http\Controllers;

use App\Alerta;
use App\Comercial;
use App\Importacion;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Estado;
use App\Cuenta;
use Exception;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Writers\CellWriter;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;
use PHPExcel_Style_Fill;
use PHPExcel_Style_Font;
use PHPExcel_Cell;
use Illuminate\Support\Facades\Config;
use Illuminate\Pagination\Paginator;

class AlertasController extends BuscadorAlertasController
{
    public function __construct()
    {
        parent::__construct();

        $this->soloGainvest();
    }

    public function soloGainvest() {
        if ($this->system != "gainvest") {
            abort(404);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estados = Estado::all();
        return view('alertas.index', array('estados' => $estados));
    }

    public function show($key, $alerta_id)
    {
        $alerta = Alerta::find($alerta_id);

        $alerta->load(['adjuntos' => function($query) {
            $query->orderBy('created_at', 'DESC');
        }, 'estado']);

        $estados = Estado::all();

        $observacion_historico = $alerta->observacion;

        $alerta_cuenta   = $alerta->cuenta;
        $alerta_adjuntos = $alerta->adjuntos;

        return view('alertas.ver', array('alerta' => $alerta, 'cuenta' => $alerta_cuenta, 'adjuntos' => $alerta_adjuntos, 'estados' => $estados, 'observacion_historico' => $observacion_historico));
    }

    public function buscar($key, Request $request)
    {
        // Motivos de busqueda
        $estado_id           = $request->estado;
        $nro_cuenta          = $request->nro_cuenta;
        $nombre              = $request->nombre;
        $tipo_busca_contador = $request->tipo_busca_contador;
        $contador            = $request->contador;
        $fecha_desde         = $request->fecha_desde;
        $fecha_hasta         = $request->fecha_hasta;
        $comercial_cuenta    = $request->comercial_cuenta;

        if ($estado_id == "0")          $estado_id      = "-";
        if ($nro_cuenta == "")          $nro_cuenta     = "-";
        if ($nombre == "")              $nombre         = "-";
        if ($contador == "")            $contador       = "-";
        if ($fecha_desde == "")         $fecha_desde    = "-";
        if ($fecha_hasta == "")         $fecha_hasta    = "-";
        if ($comercial_cuenta == "")      $comercial_cuenta = "-";

        $pagination = "";

        $alertas = $this->buscarAlertas($key, $estado_id, $nro_cuenta, $nombre, $tipo_busca_contador, $contador, $fecha_desde, $fecha_hasta, $comercial_cuenta);

        $filtros = ['estado' => $estado_id, 'nro_cuenta' => $nro_cuenta, 'nombre' => $nombre, 'tipo_busca_contador' => $tipo_busca_contador, 'contador' => $contador, 'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta, 'comercial' => $comercial_cuenta];

        $alertas = $alertas->get();

        if ($alertas->count() <= 0) {
            $alertas = [];
        }

        return view('alertas.listar', array('alertas' => $alertas, 'pagination' => $pagination, 'filtros' => $filtros));
    }

    public function mailingForm($key, $estado_id, $nro_cuenta, $nombre, $tipo_busca_contador, $contador, $fecha_desde, $fecha_hasta, $comercial_cuenta)
    {
        $comerciales = Comercial::all();

        $filtros = ['estado' => $estado_id, 'nro_cuenta' => $nro_cuenta, 'nombre' => $nombre, 'tipo_busca_contador' => $tipo_busca_contador, 'contador' => $contador, 'fecha_desde' => $fecha_desde, 'fecha_hasta' => $fecha_hasta, 'comercial' => $comercial_cuenta];

        $comerciales_en_alertas = $this->buscarMailsDeComercialesConFiltros($key, $filtros);

        return view('alertas.enviar_mails', array('comerciales' => $comerciales, 'filtros' => $filtros, 'comerciales_en_alertas' => $comerciales_en_alertas));
    }

    private function buscarMailsDeComercialesConFiltros($key, $filtros)
    {
        $alertas = $this->buscarAlertas($key, $filtros['estado'], $filtros['nro_cuenta'], $filtros['nombre'], $filtros['tipo_busca_contador'], $filtros['contador'], $filtros['fecha_desde'], $filtros['fecha_hasta'], $filtros['comercial']);
        $alertas = $alertas->get()->load('cuenta.comercial');
        $comerciales_en_alertas = array();

        foreach ($alertas as $alerta)
        {
            if (isset($alerta->cuenta->comercial)) {
                array_push($comerciales_en_alertas, $alerta->cuenta->comercial->id);
            }
        }

        return $comerciales_en_alertas;
    }

    public function update($key, Request $request, $alerta_id)
    {
        $this->validate($request, [
            'estado_id'      => 'required|exists:estados,id',
            'observacion'    => 'required'
        ]);

        $now = date("d/m/y H:i:s", time());
        $alerta = Alerta::find($alerta_id);
        $estado = Estado::find($request->estado_id);
        $observaciones_viejas = $alerta->observacion;

        /* Auditoria usuario */
        $user = Auth::user();
        $email = $user->email;

        $observacion_concatenada =
            $now . " (" . $email .")-> " . $estado->descripcion . "\n" . $request->observacion . "\n\n" . $observaciones_viejas;

        $alerta->observacion = $observacion_concatenada;
        $alerta->estado_id   = $request->estado_id;

        $alerta->save();

        // Si la alerta se resolvio, actualizar la fecha de resolucion
        if ($alerta->tieneEstadoResuelto($alerta)) {
            $alerta->actualizarLaFechaDeResolucionDeLaAlertaResuelto();
        } else {
            $alerta->actualizarLaFechaDeResolucionDeLaAlertaNoResuelto();
        }

        // Si la alerta pasa a no estar resuelta, tiene que actualizar el contador
        $alerta->actualizarElContadorDeLaAlerta($alerta);

        return redirect($this->system . '/alertas/' . $alerta->id);
    }

    /**
     * Filtra las alertas de un estado por otros parametros.
     * Ej: filtra primero por oficial_cuenta y da un resultado; a ese resultado lo filtra por el siguiente parametro, y da otro array, etc
     */
    private function filtrarAlertasYaFiltradasPorEstado($nombres_filtros = [], $valores_filtros = [], $alertas)
    {
        foreach($valores_filtros as $key => $valor_filtro)
        {
            if($valor_filtro != '-') {
                $nombre_filtro = $nombres_filtros[$key];

                $alertas = $alertas->filter(function($alerta) use ($nombre_filtro, $valor_filtro) {
                    if( $alerta->$nombre_filtro == $valor_filtro ) return $alerta;
                });
            }
        }

        return $alertas;
    }

    public function importarRelaciones()
    {
        return view('alertas.importar_relaciones');
    }

    /**
     * Importacion de relaciones entre alertas y cuentas
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function procesarImportacionRelaciones(Request $request)
    {
        $this->validate($request, [
            'archivo' => 'required'
        ]);

        $directorio_destino = 'uploads/';
        $extension          = $request->archivo->getClientOriginalExtension();
        $nombre_archivo     = rand(11111,99999) .'.'. $extension;

        $numero_sistema = $this->obtenerNumeroDeSistema($request->archivo);

        if(!$numero_sistema || $numero_sistema != 1) {
            return redirect($this->system . 'alertas/importar_relaciones')->with('error_sistema_invalido', 'Nombre incorrecto. Debe comenzar con <b><i>GAINVEST</i></b>');
        }

        $request->archivo->move($directorio_destino, $nombre_archivo);

        // Obtengo todos los estados para poder buscar
        $array_estados = Estado::all()->toArray();

        // Leemos el archivo recien subido y lo exportamos para que el cliente vea el resultado
        Excel::selectSheetsByIndex(0)->load($directorio_destino . $nombre_archivo, function($reader) use ($directorio_destino, $nombre_archivo, $extension, $numero_sistema, $array_estados) {
            // Obtengo la ultima columna escrita para escribir el resultado
            $sheet = $reader->getExcel()->setActiveSheetIndex(0);

            // Devuelve ultima columna escrita y le sumo 1 para poder escribir en la siguiente
            $num_ult_col = PHPExcel_Cell::columnIndexFromString($sheet->getHighestDataColumn());
            $ult_col =  PHPExcel_Cell::stringFromColumnIndex($num_ult_col);

            // Escribo el encabezado del resultado en el xls
            $this->escribirResultadoCeldaConEstilo($reader, $ult_col, 1, "Resultado importacion",'FFCC11', '000000');

            $sheet->getColumnDimension($ult_col)->setAutoSize(true);

            $rows = $reader->all();

            $num_row_actual = 2;

            $columnas_deseadas = ['monto_estimado', 'desvio', 'estado', 'nombre', 'numero', 'importe', 'oficial_de_cuenta', 'fecha_de_inicio'];

            if (!$this->validarColumnas($rows, $columnas_deseadas))
            {
                return redirect($this->system . 'alertas/importar_relaciones')->with('error_sistema_invalido', 'Formato de columnas incorrecto. Deben ser: <i>monto estimado</i>, <i>desvio</i>, <i>estado</i>, <i>nombre</i>, <i>numero</i>, <i>importe</i> y <i>fecha de inicio</i>');
            }

            /* Auditoria Usuarios */
            $user = Auth::user();
            $user_id = $user->id;

            // Creacion de la importacion
            $importacion = Importacion::create(['link' => $directorio_destino . $nombre_archivo, 'sistema' => $numero_sistema, 'user_id' => $user_id]);

            foreach($rows as $row)
            {
                if($this->isRowEmpty($row)){
                    continue;
                }

                $monto_estimado     = $row->monto_estimado;
                $desvio             = $row->desvio;
                $estado_descripcion = $row->estado;
                $nombre             = $row->nombre;
                $nro_cuenta         = $row->numero;
                $importe            = $row->importe;
                $oficial_cuenta     = $row->oficial_de_cuenta;
                $fecha_excel        = str_replace("/", ".", $row->fecha_de_inicio);

                if (!$nro_cuenta)
                {
                    $this->escribirResultadoCeldaConEstilo($reader, $ult_col, $num_row_actual, 'Numero de cuenta incorrecto', 'FF0000', 'FFFFFF');
                    continue;
                }

                // Busca el id estado en el array de arrays de estados [ [estado1], [estado2], ... ]
                $estado_id = $this->buscarIdEstadoPorDescripcion($array_estados, $estado_descripcion);

                // Verifico que el estado exista en la tabla de estados
                if ($estado_id == false)
                {
                    $this->escribirResultadoCeldaConEstilo($reader, $ult_col, $num_row_actual, 'Estado incorrecto', 'FF0000', 'FFFFFF');
                }
                else if (!$fecha_excel)
                {
                    $this->escribirResultadoCeldaConEstilo($reader, $ult_col, $num_row_actual, 'Fecha incorrecta', 'FF0000', 'FFFFFF');
                }
                else
                {
                    // Busco si la cuenta existe SOLO en GAINVEST (id_sistema1)
                    $cuenta  = Cuenta::where('id_sistema1', $nro_cuenta)->first();

                    if( count($cuenta) > 0 )
                    {
                        if ($monto_estimado != $cuenta->perfil) {
                            $this->escribirResultadoCeldaConEstilo($reader, $ult_col, $num_row_actual, 'El perfil importado difiere del perfil de la cuenta', 'FF0000', 'FFFFFF');
                        } else {

                            // Obtengo el numero de cuenta del sistema 1 (GAINVEST)
                            $nro_cuenta = $cuenta->id_sistema1;

                            // Busco alertas para ver si ya hay alguna existente igual a la que quiero crear
                            $alerta = Alerta::where('cuenta_id', $cuenta->id)
                                ->where('desvio', $desvio)
                                ->where('monto_estimado', $monto_estimado)
                                ->where('importe', $importe)
                                ->where('nombre', $nombre)
                                ->first();

                            if (!$alerta) // No existe ninguna alerta igual => creo la alerta
                            {
                                $array_campos = [
                                    'cuenta_id'         => $cuenta->id,
                                    'estado_id'         => $estado_id,
                                    'desvio'            => $desvio,
                                    'monto_estimado'    => $monto_estimado,
                                    'oficial_cuenta'    => $oficial_cuenta,
                                    'importe'           => $importe,
                                    'nombre'            => $nombre,
                                    'importacion_id'    => $importacion->id,
                                    'id_sistema1'       => $nro_cuenta,
                                    'fecha_alta'        => $fecha_excel
                                ];

                                // Creo la alerta
                                $alerta = Alerta::create($array_campos);
                            }
                            else // Actualizo oficial de cuenta
                            {
                                $alerta->oficial_cuenta = $oficial_cuenta;

                                $alerta->save();
                            }

                            $alerta->actualizarElContadorDeLaAlerta($alerta);

                            // Relacionar alerta con su importacion correspondiente
                            $importacion->alertas()->save($alerta);

                            // Escribo OK en el xls aunque la relacion ya exista
                            $this->escribirResultadoCeldaConEstilo($reader, $ult_col, $num_row_actual, 'OK', '1F9B23', 'FFFFFF');
                        }
                    }
                    else
                    {
                        $this->escribirResultadoCeldaConEstilo($reader, $ult_col, $num_row_actual, 'Error: La cuenta no existe', 'FF0000', 'FFFFFF');
                    }
                }
                $num_row_actual++;
            }
        })->store($extension, 'uploads');

        return redirect($this->system . '/alertas/importar_relaciones')->with('link_archivo', $directorio_destino . $nombre_archivo);
    }

    /*
     * Verifico que las columnas del excel recibido sean las que yo necesito para procesarlo
     */
    private function validarColumnas($rows, $columnas_deseadas)
    {
        $columnas_recibidas = [];

        foreach($rows[0] as $key => $titulo){
            array_push($columnas_recibidas, $key);
        }

        /*echo "COLUMNAS DESEADAS: <br>";
        print_r($columnas_deseadas);
        echo "<br><br>COLUMNAS RECIBIDAS:<br>";
        print_r($columnas_recibidas);*/

        foreach ($columnas_deseadas as $columna_deseada) {
            if (!in_array($columna_deseada, $columnas_recibidas)) return false;
        }

        return true;
    }

    private function isRowEmpty($row){
        $empty = true;

        foreach($row as $key => $value){
            if( $value )
            {
                $empty = false;
            }
        }

        return $empty;
    }

    private function buscarIdEstadoPorDescripcion($array_estados, $descripcion)
    {
        foreach ($array_estados as $key => $estado) {
            if (strtolower(trim($estado['descripcion'])) == strtolower(trim($descripcion))) {
                return $estado['id'];
            }
        }

        return false;
    }
}
