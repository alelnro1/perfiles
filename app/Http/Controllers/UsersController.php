<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\User;
use App\Rol;

class UsersController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Si es el super admin => cargo todos los usuarios
        if (Auth::user()->hasRole('super admin')) {
            $usuarios = User::all();
        } else {
            $usuarios = User::where('rol_id', '<>', '1')->get();
        }

        $usuarios->load('rol');

        return view('usuarios.listar', array('usuarios' => $usuarios));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Si es el super admin => le doy la opcion de crear super admins
        if (Auth::user()->hasRole('super admin')) {
            $roles = Rol::all();
        } else {
            $roles = Rol::where('id', '<>', '1')->get();
        }

        return view('auth.register', array('roles' => $roles) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|confirmed|min:6',
            'rol_id' => 'required|not_in:0'
        ]);

        $user = User::create([
                    'nombre' => $request->nombre,
                    'email' => $request->email,
                    'password' => bcrypt($request->password),
                    'rol_id' => $request->rol_id
                ]);

        return redirect('/' . $this->system . '/usuarios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($key, $id)
    {
        $usuario = User::findOrFail($id);
        $usuario->load('rol');

        return view('usuarios.ver', array('usuario' => $usuario));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($key, $id)
    {
        $usuario = User::findOrFail($id);

        // Si es el super admin => le doy la opcion de editar super admins
        if (Auth::user()->hasRole('super admin')) {
            $roles = Rol::all();
        } else {
            $roles = Rol::where('id', '<>', '1')->get();
        }

        return view('usuarios.edit', array('usuario' => $usuario, 'roles' => $roles));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($key, Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'confirmed|min:6',
            'rol_id' => 'required|not_in:0'
        ]);

        $usuario = User::findOrFail($id);

        $usuario->nombre = $request->nombre;
        $usuario->email  = $request->email;
        $usuario->rol_id = $request->rol_id;


        if ($request->password) {
            $usuario->password = bcrypt($request->password);
        }

        $usuario->save();

        return redirect('/' . $this->system . '/usuarios/' . $usuario->id)->with('usuario_actualizado', 'El usuario ha sido actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($key, $id)
    {
        $usuario = User::findOrFail($id);

        $nombre = $usuario->nombre;

        $usuario->delete();

        return redirect('/' . $this->system . '/usuarios/')->with('usuario_eliminado', 'El usuario ' . $nombre . ' ha sido eliminado');
    }
}
