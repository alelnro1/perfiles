<?php

namespace App\Http\Controllers;

use App\Importacion;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Alerta;
use App\Cuenta;

class ImportacionesController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    private $tipo_importaciones_a_listar;

    public function listarAlertas()
    {
        $this->tipo_importaciones_a_listar = "alertas";

        return $this->listar();
    }

    public function listarCuentas()
    {
        $this->tipo_importaciones_a_listar = "cuentas";

        return $this->listar();
    }

    public function verAlertasDeImportacion($key, $id)
    {
        if ($this->esCibsa()) abort(404);

        $importacion = Importacion::find($id);

        if ($importacion && $this->sistemaCorrecto($importacion->sistema))
        {
            $alertas_importadas = Alerta::where('importacion_id', $id)->get();
            $alertas_importadas->load('cuenta');
            $fecha_importacion = $importacion->created_at;
        }
        else {
            abort(404);
        }

        return view('importaciones.alertas.ver', array('alertas' => $alertas_importadas, 'fecha_importacion' => $fecha_importacion));
    }

    public function verCuentasDeImportacion($key, $id)
    {
        $importacion = Importacion::findOrFail($id);


        if ($importacion && $this->sistemaCorrecto($importacion->sistema))
        {
            $cuentas_importadas = Cuenta::where('importacion_id', $id)->get();
            $cuentas_importadas->load('personas');
            $fecha_importacion = $importacion->created_at;
        }
        else {
            abort(404);
        }

        return view('importaciones.cuentas.ver', array('cuentas' => $cuentas_importadas, 'fecha_importacion' => $fecha_importacion));
    }

    private function listar()
    {
        /* Defino sistema actual */
        $numero_sistema = $this->numeroSistema();

        $importaciones = Importacion::where('sistema', $numero_sistema)
        ->with([
            $this->tipo_importaciones_a_listar,
            'user' => function($query) {
                $query->withTrashed();
            }
        ])->has($this->tipo_importaciones_a_listar)->orderBy('created_at', 'desc')->paginate(10);

        $importaciones->filter(function($item) {
            if(count($item->tipo_importaciones_a_listar) > 0) return $item;
        });


        return view('importaciones.listar', array('importaciones' => $importaciones, 'tipo_importacion' => $this->tipo_importaciones_a_listar));
    }

    private function numeroSistema()
    {
        if ($this->esGainvest()) {
            $nro_sistema = "1";
        } else if ($this->esCibsa()) {
            $nro_sistema = "2";
        }

        return $nro_sistema;
    }
}