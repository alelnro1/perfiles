<?php

namespace App\Http\Controllers\Configuracion;

use App\Classes\Configuracion\CategoriaMonotributo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;

class CategoriasMonotributoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listar()
    {
        $categorias_monotributo = CategoriaMonotributo::all();

        return view('configuracion.categorias-monotributo.listar', [
            'categorias_monotributo' => $categorias_monotributo
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('configuracion.categorias-monotributo.create');
    }

    public function store(Request $request)
    {
        CategoriaMonotributo::create($request->all());

        return redirect($this->system . '/configuracion/categorias-monotributo')->with('categoria_monotributo_creada', true);
    }

    public function edit($system, $id)
    {
        $categoria_monotributo = CategoriaMonotributo::findOrFail($id);

        return view('configuracion.categorias-monotributo.edit', [
            'categoria_monotributo' => $categoria_monotributo
        ]);
    }

    public function update(Request $request, $system, $id)
    {
        $categoria_monotributo = CategoriaMonotributo::findOrFail($id);
        $categoria_monotributo->update($request->all());

        return redirect($this->system . '/configuracion/categorias-monotributo')->with('categoria_monotributo_actualizada', true);
    }

    public function delete($key, $id)
    {
        $categoria_monotributo = CategoriaMonotributo::findOrFail($id);
        $categoria_monotributo->delete();

        return redirect($this->system . '/configuracion/categorias-monotributo')->with('categoria_monotributo_eliminada', true);
    }
}