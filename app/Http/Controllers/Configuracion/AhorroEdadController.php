<?php

namespace App\Http\Controllers\Configuracion;

use App\Classes\Configuracion\AhorroEdad;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;

class AhorroEdadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listar()
    {
        $ahorros_edad = AhorroEdad::all();

        return view('configuracion.ahorroedad.listar', [
            'ahorros_edad' => $ahorros_edad
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('configuracion.ahorroedad.create');
    }

    public function store(Request $request)
    {
        AhorroEdad::create($request->all());

        return redirect($this->system . '/configuracion/ahorro-edad')->with('ahorro_edad_creado', true);
    }

    public function edit($system, $id)
    {
        $ahorro_edad = AhorroEdad::findOrFail($id);

        return view('configuracion.ahorroedad.edit', [
            'ahorro_edad' => $ahorro_edad
        ]);
    }

    public function update(Request $request, $system, $id)
    {
        $ahorro_edad = AhorroEdad::findOrFail($id);
        $ahorro_edad->update($request->all());

        return redirect($this->system . '/configuracion/ahorro-edad')->with('ahorro_edad_actualizado', true);
    }

    public function delete($key, $id)
    {
        $ahorro_edad = AhorroEdad::findOrFail($id);
        $ahorro_edad->delete();

        return redirect($this->system . '/configuracion/ahorro-edad')->with('ahorro_edad_eliminado', true);
    }
}