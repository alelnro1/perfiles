<?php

namespace App\Http\Controllers\Configuracion;

use App\Classes\Configuracion\CategoriaMonotributo;
use App\Classes\Configuracion\ConfiguracionPonderacion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;

class PonderacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listar()
    {
        $ponderaciones_fisicas = ConfiguracionPonderacion::where('tipo', 'fisica')->get();
        $ponderaciones_juridicas = ConfiguracionPonderacion::where('tipo', 'juridica')->get();

        return view('configuracion.ponderaciones.listar', [
            'ponderaciones_fisicas' => $ponderaciones_fisicas,
            'ponderaciones_juridicas' => $ponderaciones_juridicas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('configuracion.ponderaciones.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'tipo'      => 'required'
        ]);

        ConfiguracionPonderacion::create($request->all());

        return redirect($this->system . '/configuracion/ponderaciones')->with('ponderacion_creada', true);
    }

    public function edit($system, $id)
    {
        $ponderacion = ConfiguracionPonderacion::findOrFail($id);

        return view('configuracion.ponderaciones.edit', [
            'ponderacion' => $ponderacion
        ]);
    }

    public function update(Request $request, $system, $id)
    {
        $ponderacion = ConfiguracionPonderacion::findOrFail($id);
        $ponderacion->update($request->all());

        return redirect($this->system . '/configuracion/ponderaciones')->with('ponderacion_actualizada', true);
    }

    public function delete($key, $id)
    {
        $ponderacion = ConfiguracionPonderacion::findOrFail($id);
        $ponderacion->delete();

        return redirect($this->system . '/configuracion/ponderaciones')->with('ponderacion_eliminada', true);
    }
}