<?php

namespace App\Http\Controllers;

use App\Comercial;
use Illuminate\Http\Request;

use App\Http\Requests;

class ComercialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comerciales = Comercial::all();

        return view('comercial.listar', array('comerciales' => $comerciales));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('comercial.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'codigo'   => 'required|unique:comercial',
            'nombre'   => 'required|max:100',
            'email'    => 'required|email',
            'telefono' => 'required',
        ]);

        $comercial = Comercial::create($request->all());

        return redirect('/' . $this->system . '/comercial/')->with('comercial_creado', 'Comercial con código ' . $comercial->codigo . ' creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($key, $id)
    {
        $comercial = Comercial::findOrFail($id);

        return view('comercial.ver', array('comercial' => $comercial));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($key, $id)
    {
        $comercial = Comercial::findOrFail($id);

        return view('comercial.edit', array('comercial' => $comercial));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($key, Request $request, $id)
    {
        $this->validate($request, [
            'codigo'   => 'required|max:100',
            'nombre'   => 'required|max:100',
            'email'    => 'required|email',
            'telefono' => 'required',
        ]);

        $comercial = Comercial::findOrFail($id);

        $comercial->codigo   = $request->codigo;
        $comercial->nombre   = $request->nombre;
        $comercial->email    = $request->email;
        $comercial->telefono = $request->telefono;


        $comercial->save();

        return redirect('/' . $this->system . '/comercial/' . $comercial->id)->with('actualizado', 'Comercial actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($key, $id)
    {
        $comercial = Comercial::findOrFail($id);

        $comercial->delete();

        return redirect('/' . $this->system . '/comercial/')->with('comercial_eliminado', 'Comercial ' . $comercial->nombre . ' eliminado');
    }
}
