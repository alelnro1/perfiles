<?php

namespace App\Http\Controllers;

use App\Comercial;
use App\Cuenta;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Persona;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Writers\CellWriter;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;
use PHPExcel_Style_Fill;
use PHPExcel_Style_Font;
use PHPExcel_Cell;
use App\Importacion;

class CuentasController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $personas = Persona::all();

        return view('cuentas.create', compact('personas', $personas) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id_sistema1' => 'max:100|required_without_all:id_sistema2|unique:cuentas',
            'id_sistema2' => 'max:100|required_without_all:id_sistema1|unique:cuentas'
        ]);

        Cuenta::create($request->all());
    }

    public function crearRelacion(Request $request)
    {
        $cuenta = Cuenta::find($request->cuenta_id);

        // Guardo la relacion en la base de datos
        $cuenta->Personas()->attach($request->persona_id);

        return redirect('personas/' . $request->persona_id);
    }

    public function importarRelaciones()
    {
        return view('cuentas.importar_relaciones');
    }

    private function buscarCuentaDeSistema($nro_cuenta) {
        if ($this->esGainvest()) {
            $cuenta = Cuenta::where('id_sistema1', $nro_cuenta)->first();
        } else if ($this->esCibsa()) {
            $cuenta = Cuenta::where('id_sistema2', $nro_cuenta)->first();
        }

        return $cuenta;
    }

    public function procesarImportacionRelaciones(Request $request)
    {
        $this->validate($request, [
            'archivo' => 'required'
        ]);

        $directorio_destino = 'uploads/';
        $extension          = $request->archivo->getClientOriginalExtension();
        $nombre_archivo     = rand(11111,99999) .'.'. $extension;

        $numero_sistema = $this->obtenerNumeroDeSistema($request->archivo);

        if(!$this->sistemaCorrecto($numero_sistema)) {
            return redirect($this->system . '/cuentas/importar_relaciones')->with('error_sistema_invalido', 'Nombre incorrecto. Debe comenzar con <b><i>' . $this->system . '</i></b>');
        }

        $request->archivo->move($directorio_destino, $nombre_archivo);

        // Leemos el archivo recien subido y lo exportamos para que el cliente vea el resultado
        Excel::selectSheetsByIndex(0)->load($directorio_destino . $nombre_archivo, function($reader) use ($directorio_destino, $nombre_archivo, $extension, $numero_sistema) {
            /* Obtengo la ultima columna escrita para escribir el resultado */
            $sheet = $reader->getExcel()->setActiveSheetIndex(0);

            // Devuelve ultima columna escrita y le sumo 1 para poder escribir en la siguiente
            $num_ult_col = PHPExcel_Cell::columnIndexFromString($sheet->getHighestDataColumn());
            $ult_col =  PHPExcel_Cell::stringFromColumnIndex($num_ult_col);

            // Escribo el encabezado del resultado en el xls
            $this->escribirResultadoCeldaConEstilo($reader, $ult_col, 1, "Resultado importacion",'FFCC11', '000000');

            $sheet->getColumnDimension($ult_col)->setAutoSize(true);

            /* Auditoria Usuarios */
            $user = Auth::user();
            $user_id = $user->id;

            // Creacion de la importacion
            $importacion = Importacion::create(['link' => $directorio_destino . $nombre_archivo, 'sistema' => $numero_sistema, 'user_id' => $user_id]);

            $rows = $reader->all();

            $num_row_actual = 2;

            foreach($rows as $row)
            {
                $cuit           = $row->cuit;
                $cuil           = $row->cuil;
                $dni            = $row->documento;
                $nro_cuenta     = $row->numero_de_cuenta;
                $oficial_cuenta = $row->oficial;

                // Verifico que haya dni, cuit o cuil para buscar
                if( trim($dni) == "" && trim($cuit) == "" && trim($cuil) == "")
                {
                    $this->escribirResultadoCeldaConEstilo($reader, $ult_col, $num_row_actual, 'Persona sin identificador', 'FF0000', 'FFFFFF');
                } else if( trim($nro_cuenta) == "" || !is_numeric($nro_cuenta) )
                {
                    $this->escribirResultadoCeldaConEstilo($reader, $ult_col, $num_row_actual, 'Numero de Cuenta invalido', 'FF0000', 'FFFFFF');
                }
                else
                {
                    $persona = Persona::where('cuit', $cuit)
                        ->orWhere('cuil', $cuil)
                        ->orWhere('dni', $dni)
                        ->where('sistema', $numero_sistema)
                        ->first();

                    if( count($persona) > 0 )
                    {
                        $cuenta = $this->buscarCuentaDeSistema($nro_cuenta); // Estoy en el sistema correcto y hay una cuenta

                        $comercial_id = $this->buscarComercialId($oficial_cuenta);

                        if( count($cuenta) > 0 )
                        {
                            if( !$cuenta->personas->contains($persona->id) ) // Si la relacion no esta creada
                            {
                                $cuenta->Personas()->attach($persona->id);

                                // Sumo al perfil de la cuenta, el perfil de la nueva persona vinculada
                                $cuenta->actualizaTuPerfil(0, $persona->perfil_vigente);
                            }

                            $this->actualizaComercial($cuenta, $comercial_id);

                            // Relacionar cuenta con su importacion correspondiente
                            $importacion->cuentas()->save($cuenta);

                            // Escribo OK en el xls aunque la relacion ya exista
                            $this->escribirResultadoCeldaConEstilo($reader, $ult_col, $num_row_actual, 'OK', '1F9B23', 'FFFFFF');
                        }
                        else
                        {
                            // Si la cuenta no existe, entonces tampoco existe la relacion y no hay que verificar si ésta existe
                            $cuenta = $this->crearCuentaYRelacionarConPersona($numero_sistema, $persona, $nro_cuenta, $importacion, $comercial_id);

                            // Sumo al perfil de la cuenta, el perfil de la nueva persona vinculada
                            $cuenta->actualizaTuPerfil(0, $persona->perfil_vigente);

                            $this->actualizaComercial($cuenta, $comercial_id);

                            // Relacion creada => escribo OK
                            $this->escribirResultadoCeldaConEstilo($reader, $ult_col, $num_row_actual, 'OK', '1F9B23', 'FFFFFF');
                        }
                    }
                    else
                    {
                        $this->escribirResultadoCeldaConEstilo($reader, $ult_col, $num_row_actual, 'Error: La persona no existe', 'FF0000', 'FFFFFF');
                    }
                }
                $num_row_actual++;
            }

        })->store($extension, 'uploads');

        return redirect($this->system . '/cuentas/importar_relaciones')->with('link_archivo', $directorio_destino . $nombre_archivo);
    }

    private function actualizaComercial($cuenta, $comercial_id)
    {
        if ($comercial_id) {
            $cuenta->comercial_id = $comercial_id;

            $cuenta->save();
        }
    }

    private function buscarComercialId($oficial_cuenta)
    {
        $comercial = Comercial::where('codigo', $oficial_cuenta)->first();

        if ($comercial)
            return $comercial->id;
        else
            return false;
    }

    private function crearCuentaYRelacionarConPersona($numero_sistema, $persona, $nro_cuenta, $importacion, $comercial_id)
    {
        // Creo la cuenta
        if($numero_sistema == 1) {
            if ($comercial_id) {
                $campos_a_llenar = ['id_sistema1' => $nro_cuenta, 'importacion_id' => $importacion->id, 'comercial_id' => $comercial_id];
            } else {
                $campos_a_llenar = ['id_sistema1' => $nro_cuenta, 'importacion_id' => $importacion->id];
            }
        } else {
            if ($comercial_id) {
                $campos_a_llenar = ['id_sistema2' => $nro_cuenta, 'importacion_id' => $importacion->id, 'comercial_id' => $comercial_id];
            } else {
                $campos_a_llenar = ['id_sistema2' => $nro_cuenta, 'importacion_id' => $importacion->id];
            }
        }

        $cuenta = Cuenta::create($campos_a_llenar);

        // Relacionar cuenta con su importacion correspondiente
        $importacion->cuentas()->save($cuenta);

        // Relaciono la cuenta con la persona
        $cuenta->Personas()->attach($persona->id);

        return $cuenta;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($key, $cuenta_id)
    {
        $cuenta = Cuenta::with([
            'personas',
            'adjuntos' => function($query) {
                $query->orderBy('created_at', 'DESC');
            },
            'alertas' => function($query) {
                $query->with('estado');
            },
            'comercial'
        ])->where('id', $cuenta_id)->get();

        if( count($cuenta) == 0 ) {
            abort(404);
        } else {
            $cuenta = $cuenta[0];

            if( $cuenta->id_sistema1 != 0 ){ // La cuenta es de gainvest
                $nro_sistema_cuenta = 1;
            } else { // La cuenta es de cibsa
                $nro_sistema_cuenta = 2;
            }

            // La cuenta corresponde al sistema por el cual se esta ingresando
            if ($this->sistemaCorrecto($nro_sistema_cuenta)) {
                $personas = $cuenta->personas;
                $adjuntos = $cuenta->adjuntos;
                $alertas = $cuenta->alertas;
            } else {
                abort(404);
            }
        }

        return view('cuentas.ver', array('cuenta' => $cuenta, 'personas' => $personas, 'adjuntos' => $adjuntos, 'alertas' => $alertas));
    }

    public function edit($key, $cuenta_id)
    {
        $cuenta = Cuenta::findOrFail($cuenta_id);
        $comerciales = Comercial::all();

        return view('cuentas.edit', array('cuenta' => $cuenta, 'comerciales' => $comerciales));
    }

    public function update($key, Request $request, $id)
    {
        $this->validate($request, [
            'comercial_id' => 'required|not_in:0'
        ]);

        $cuenta = Cuenta::findOrFail($id);

        $cuenta->comercial_id  = $request->comercial_id;

        $cuenta->save();

        return redirect('/' . $this->system . '/cuentas/' . $cuenta->id)->with('actualizado', 'Cuenta actualizada');

    }
}
