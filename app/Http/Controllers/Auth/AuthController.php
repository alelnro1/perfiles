<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Rol;
use Illuminate\Support\Facades\Config;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('guest',['except' => 'logout']);

        parent::__construct();
    }

    /**
     * Override del metodo que está en Illuminate\Routing\Router::auth() para poder pasarle variables al form del registro
     *
     * @return void
     */
    public function showRegistrationForm()
    {
        $roles = Rol::all();

        if (property_exists($this, 'registerView')) {
            return view($this->registerView);
        }

        return view('auth.register', compact('roles', $roles) );
    }

    public function logout()
    {
        Auth::logout();

        return redirect(Config::get('ACTUAL_SYSTEM'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'nombre' => $data['nombre'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'rol_id' => $data['rol_id']
        ]);
    }
}
