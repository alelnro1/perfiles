<?php

namespace App\Http\Controllers;

use App\Alerta;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Adjunto;
use App\Persona;
use App\Cuenta;

class AdjuntosController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Metodo utilizado unicamente para subir adjuntos
     * @param Request $request
     */
    public function subirAdjunto(Request $request)
    {
        $directorio_destino = 'uploads/adjuntos/';
        $nombre_original    = $request->adjunto->getClientOriginalName();
        $extension          = $request->adjunto->getClientOriginalExtension();
        $nombre_adjunto     = rand(11111,99999) .'.'. $extension;

        if ($request->adjunto->getMaxFilesize() > 2000000) {
            if ($request->adjunto->isValid()) {
                if ($request->adjunto->move($directorio_destino, $nombre_adjunto)) {
                    $valid = true;
                    $url = $directorio_destino . $nombre_adjunto;
                    $error = false;
                } else {
                    $valid = false;
                    $url = false;
                    $error = "No se pudo mover el archivo";
                }
            } else {
                $valid = false;
                $url = false;
                $error = $request->adjunto->getErrorMessage();
            }
        } else {
            $valid = false;
            $url = false;
            $error = "El tamaño maximo debe ser de 2MB";
        }

        return json_encode(['valid' => $valid, 'url_adjunto_subido' => $url, 'nombre_original' => $nombre_original, 'error' => $error]);
    }

    /**
     * Crea el modelo adjunto y lo mete en la base de datos, sea de persona, alerta o cuenta (lo recibe por parametro)
     * @param Request $request
     */
    public function nuevoAdjunto(Request $request)
    {
        if($request['persona_id'] == 0) $request['persona_id'] = NULL;
        if($request['cuenta_id'] == 0)  $request['cuenta_id'] = NULL;
        if($request['alerta_id'] == 0)  $request['alerta_id'] = NULL;
        $adjunto = Adjunto::create($request->all());

        return json_encode(['descripcion' => $adjunto->descripcion, 'url' => $adjunto->adjunto, 'created_at' => $adjunto->created_at]);
    }

    public function eliminarAdjunto(Request $request)
    {
        $adjunto_id = $request->adjunto_id;
        $persona_id = $request->persona_id;
        $cuenta_id  = $request->cuenta_id;
        $alerta_id  = $request->alerta_id;

        if ($persona_id != 0) {
            $elimino_adjunto = $this->eliminarAdjuntoDeObjeto($adjunto_id, 'persona_id', $persona_id);
        } else if ($cuenta_id != 0) {
            $elimino_adjunto = $this->eliminarAdjuntoDeObjeto($adjunto_id, 'cuenta_id', $cuenta_id);
        } else if ($alerta_id != 0) {
            $elimino_adjunto = $this->eliminarAdjuntoDeObjeto($adjunto_id, 'alerta_id', $alerta_id);
        }

        return json_encode(['success' => $elimino_adjunto, 'adjunto_id' => $adjunto_id]);
    }

    private function eliminarAdjuntoDeObjeto($adjunto_id, $campo_id, $id)
    {
        // Verifico que el adjunto exista y le pertenezca a la persona => obtengo el objeto
        $adjunto = Adjunto::where($campo_id, $id)->where('id', $adjunto_id)->first();

        // Elimino el archivo adjunto del servidor
        if ($adjunto) {
            unlink($adjunto->adjunto);
            $adjunto->delete();
            return true;
        } else {
            return false;
        }



    }
}
