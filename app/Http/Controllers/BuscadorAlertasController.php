<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Alerta;
use Illuminate\Support\Facades\Config;

class BuscadorAlertasController extends Controller
{
    public $alerta;

    public function __construct(){
        $this->alerta = new Alerta();

        parent::__construct();
    }

    public function buscarAlertas($key, $estado_id, $nro_cuenta, $nombre, $tipo_busca_contador, $contador, $fecha_desde, $fecha_hasta, $comercial_cuenta)
    {
        if ($comercial_cuenta != "-")
        {
            $alertas = $this->alerta->dameAlertasDeCuentasFiltradasPorComercialDeCuenta($comercial_cuenta);
        }
        else if ($estado_id != "-" && $nro_cuenta != "-") // Busco por ESTADO y NRO_CUENTA
        {
            $alertas = $this->alerta->dameAlertasFiltradasPorEstadoYNroCuenta($estado_id, $nro_cuenta);
        }
        else if ($estado_id != "-" && $nro_cuenta == "-") // Busco por ESTADO
        {
            $alertas = $this->alerta->dameAlertasFiltradasPorEstado($estado_id);
        }
        else if ($estado_id == "-" && $nro_cuenta != "-") // Busco por NRO_CUENTA
        {
            $alertas = $this->alerta->dameAlertasFiltradasPorNroCuenta($nro_cuenta);
        }
        else // No busco por nada
        {
            $alertas = $this->alerta->dameAlertasSinFiltrar();
        }

        if ($nombre != "-")
            $alertas = $this->alerta->dameAlertasFiltradasPorNombre($alertas, $nombre);

        if ($contador != "-")
            $alertas = $this->alerta->dameAlertasFiltradasPorContador($alertas, $tipo_busca_contador, $contador);

        if ($fecha_desde != "-" || $fecha_hasta != "-") {
            if ($fecha_desde == "-") $fecha_desde = "0000-00-00"; // Fecha minima
            if ($fecha_hasta == "-") $fecha_hasta = "3050-12-31"; // Fecha maxima => Dudo que vivamos hasta el año 3050..

            $alertas = $this->alerta->dameAlertasFiltradasPorFecha($alertas, $fecha_desde, $fecha_hasta);
        }

        return $alertas;
    }
}
