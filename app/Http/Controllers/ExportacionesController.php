<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Maatwebsite\Excel\Facades\Excel;

class ExportacionesController extends Controller
{

    private $buscador_alertas;
    private $buscador_personas;

    public $personas_busqueda;
    public $alertas_busqueda;

    public function __construct()
    {
        $this->buscador_alertas  = new BuscadorAlertasController();
        $this->buscador_personas = new BuscadorPersonasController();

        $this->personas_busqueda = [];
        $this->alertas_busqueda  = [];

        parent::__construct();
    }

    public function exportarAlertas($key, $estado, $nro_cuenta, $nombre, $tipo_busca_contador, $contador, $fecha_desde, $fecha_hasta, $comercial_cuenta) {

        $alertas = $this->buscarAlertasParaExportar($key, $estado, $nro_cuenta, $nombre, $tipo_busca_contador, $contador, $fecha_desde, $fecha_hasta, $comercial_cuenta);

        return $this->crearExcelAlertas($alertas, true);
    }

    public function exportarPersonas($key, $dni, $cuit, $cuil, $nombre, $nro_cuenta)
    {
        if( trim($dni) == "" )        $dni = "-";
        if( trim($cuit) == "" )       $cuit = "-";
        if( trim($cuil) == "" )       $cuil = "-";
        if( trim($nombre) == "" )     $nombre = "-";
        if( trim($nro_cuenta) == "" ) $nro_cuenta = "-";

        $personas = $this->buscador_personas->buscarPersonas($key, $dni, $cuit, $cuil, $nombre, $nro_cuenta);

        $this->personas_busqueda = $this->obtenerColeccionDeAPartes($personas);

        $personas = $this->personas_busqueda; // Lo pongo asi para no tener que cambiar todo el codigo del Excel

        Excel::create('personas', function($excel) use ($personas) {

            $excel->sheet('sheet1', function($sheet) use ($personas) {

                /* Escribo los encabezados */
                $encabezados = [
                    'Tipo Persona',
                    'Nombre/Razon Social',
                    'Perfil Calculado',
                    'Perfil Vigente',
                    'Año Nacimiento',
                    'Sueldo',
                    'Categoria Monotributo',
                    'Monto Percibido',
                    'Total Bienes Exentos',
                    'Dinero Efectivo',
                    'Patrimonio Caja y Bancos',
                    'Patrimonio Inversiones',
                    'Patrimonio Activo Corriente',
                    'Patrimonio Pasivo Corriente',
                    'Patrimonio Neto',
                    'Ventas',
                    'Resultado Bruto',
                    'DNI',
                    'CUIT',
                    'CUIL',
                    'Fecha Documentacion',
                    'Cierre Balance',
                    'Cuenta',
                    'Perfil Cuenta'
                ];
                // Ultima columna escrita
                $num_ult_col = count($encabezados);

                $sheet->appendRow(1, $encabezados);

                for ($col = 1; $col <= $num_ult_col; $col++) {
                    $sheet->row(1, function ($row) {
                        $row->setFontWeight('bold');
                    });
                }

                /* Escribo las alertas */
                $row_activo = 2;

                foreach ($personas as $persona) {
                    // Escribo los datos de la persona que no tiene cuenta
                    if (count($persona->cuentas) <= 0) {
                        $persona_xls = $this->armarDatosPersona($persona);

                        $sheet->appendRow($row_activo, $persona_xls);

                        $row_activo++;
                    }

                    foreach ($persona->cuentas as $cuenta) {
                        $persona_xls = $this->armarDatosPersona($persona);

                        $persona_xls['cuenta'] = $cuenta->id_sistema1;
                        $persona_xls['perfil_cuenta'] = $cuenta->perfil;

                        $sheet->appendRow($row_activo, $persona_xls);

                        $row_activo++;
                    }
                }
            });

        })->download('xls');
    }

    protected function buscarAlertasParaExportar($key, $estado, $nro_cuenta, $nombre, $tipo_busca_contador, $contador, $fecha_desde, $fecha_hasta, $comercial_cuenta)
    {
        if ($estado == "0")             $estado  = "-";
        if ($nro_cuenta == "")          $nro_cuenta = "-";
        if ($nombre == "")              $nombre     = "-";
        if ($contador == "")            $contador   = "-";
        if ($fecha_desde == "")         $fecha_desde    = "-";
        if ($fecha_hasta == "")         $fecha_hasta    = "-";
        if ($comercial_cuenta == "")      $comercial_cuenta = "-";

        $alertas = $this->buscador_alertas->buscarAlertas($key, $estado, $nro_cuenta, $nombre, $tipo_busca_contador, $contador, $fecha_desde, $fecha_hasta, $comercial_cuenta);

        $this->alertas_busqueda = $this->obtenerColeccionDeAPartes($alertas);

        return $this->alertas_busqueda;
    }

    protected function crearExcelAlertas($alertas, $download)
    {
        $excel = Excel::create(rand(0, 9999) . "-" . time() . '_alertas', function($excel) use ($alertas) {

                $excel->sheet('sheet1', function($sheet) use ($alertas) {

                    /* Escribo los encabezados */
                    $encabezados = ['Numero de Cuenta', 'Estado', 'Oficial de Cuenta', 'Nombre', 'Contador', 'Fecha Alta', 'Fecha Resolucion'];
                    // Ultima columna escrita
                    $num_ult_col = count($encabezados);

                    $sheet->appendRow(1, $encabezados);

                    for ($col = 1; $col <= $num_ult_col; $col++) {
                        $sheet->row(1, function ($row) {
                            $row->setFontWeight('bold');
                        });
                    }

                    /* Escribo las alertas */
                    $row_activo = 2;

                    foreach ($alertas as $alerta) {
                        $alerta_xls = $this->armarDatosAlerta($alerta);

                        $sheet->appendRow($row_activo, $alerta_xls);

                        $row_activo++;
                    }
                });
            });

        if ($download)
            return $excel->download('xls');
        else {
            return $excel->store('xls', 'uploads/mailing', true);
        }
    }

    private function armarDatosPersona($persona)
    {
        if (isset($persona->matriz->descripcion))
            $persona_xls['tipo_persona'] = $persona->matriz->descripcion;
        else
            $persona_xls['tipo_persona'] = "No asignada";

        $persona_xls['nombre'] = $persona->nombre;
        $persona_xls['perfil_calculado'] = $persona->perfil_calculado;
        $persona_xls['perfil_vigente'] = $persona->perfil_vigente;
        $persona_xls['anio_nacimiento'] = $persona->anio_nacimiento;
        $persona_xls['sueldo'] = $persona->sueldo;
        $persona_xls['categoria_monotributo'] = $persona->categoria_monotributo;
        $persona_xls['monto_percibido'] = $persona->monto_percibido;
        $persona_xls['total_bienes_exentos'] = $persona->total_bienes_exentos;
        $persona_xls['dinero_efectivo'] = $persona->dinero_efectivo;
        $persona_xls['patrimonio_caja_bancos'] = $persona->patrimonio_caja_y_bancos;
        $persona_xls['patrimonio_inversiones'] = $persona->patrimonio_inversiones;
        $persona_xls['patrimonio_activo_corriente'] = $persona->patrimonio_activo_corriente;
        $persona_xls['patrimonio_pasivo_corriente'] = $persona->patrimonio_pasivo_corriente;
        $persona_xls['patrimonio_neto'] = $persona->patrimonio_neto;
        $persona_xls['ventas'] = $persona->ventas;
        $persona_xls['resultado_bruto'] = $persona->resultado_bruto;
        $persona_xls['dni'] = $persona->dni;
        $persona_xls['cuit'] = $persona->cuit;
        $persona_xls['cuil'] = $persona->cuil;
        $persona_xls['fecha_documentacion'] = substr($persona->fecha_documentacion, 0, 10);
        $persona_xls['cierre_balance'] = substr($persona->cierre_balance, 0, 10);

        if (strtotime($persona->fecha_documentacion) === false || strtotime($persona->fecha_documentacion) < 0) {
            $persona_xls['fecha_documentacion'] = "-";
        }

        if (strtotime($persona->cierre_balance) === false || strtotime($persona->cierre_balance) < 0) {
            $persona_xls['cierre_balance'] = "-";
        }

        return $persona_xls;
    }

    private function armarDatosAlerta($alerta)
    {
        $alerta_xls['nro_cuenta'] = $alerta->cuenta->id_sistema1;
        $alerta_xls['estado'] = preg_replace('/^\s+|\n|\r|\s+$/m', '', $alerta->estado->descripcion); // Elimino posibles saltos de linea
        $alerta_xls['oficial_cuenta'] = $alerta->oficial_cuenta;
        $alerta_xls['nombre'] = $alerta->nombre;
        $alerta_xls['contador'] = $alerta->contador;
        $alerta_xls['fecha_alta'] = substr($alerta->fecha_alta, 0, 10);
        $alerta_xls['fecha_resolucion'] = substr($alerta->fecha_resolucion, 0, 10);

        if (strtotime($alerta->fecha_resolucion) === false || strtotime($alerta->fecha_resolucion) < 0) {
            $alerta_xls['fecha_resolucion'] = "No resuelto";
        }

        return $alerta_xls;
    }
}
