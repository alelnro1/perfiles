<?php

namespace App\Http\Controllers;

use App\HistorialPersona;
use App\Persona;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use DateTime;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class BuscadorPersonasController extends Controller
{
    public $persona;

    public function __construct()
    {
        $this->persona = new Persona();

        parent::__construct();
    }

    public function buscarPersonas($key, $dni, $cuit, $cuil, $nombre, $nro_cuenta) {

        if ($nro_cuenta != "-") // Busca por numero de cuenta
        {
            $personas = $this->buscarPersonasPorCuenta($nro_cuenta);

        }

        if (isset($personas)) { // Filtro las personas de esa cuenta que acabo de encontrar
            $personas = $this->buscarPersonasPorCampos($dni, $cuit, $cuil, $nombre, $personas);
        } else {
            // Si no puso un campo, poner el wildcard asi encuentra a todas las cuentas de las personas recien filtradas
            if ($nro_cuenta == "-")   $nro_cuenta = "%";
            if ( $dni == "-" )        $dni = "%";
            if ( $cuit == "-" )       $cuit = "%";
            if ( $cuil == "-" )       $cuil = "%";
            if ( $nombre == "-" )     $nombre = "%";

            $personas = $this->buscarPersonasConCuentas($dni, $cuit, $cuil, $nombre, $nro_cuenta);


            if( count($personas) <= 0 )
            {
                $personas = false;
            }
        }

        return $personas;
    }

    protected function buscarCuentaDeSistema($query, $nro_cuenta) {
        if ($this->esGainvest()) {
            $query->where('id_sistema1', 'LIKE', '%' . $nro_cuenta . '%')
                ->where('id_sistema2', '=', '');
        } else if ($this->esCibsa()) {
            $query->where('id_sistema2', 'LIKE', '%' . $nro_cuenta . '%')
                ->where('id_sistema1', '=', '');
        }
    }

    private function buscarPersonasPorCampos($dni, $cuit, $cuil, $nombre, $personas)
    {
        if ($dni != "-")
            $personas->where('dni', 'LIKE', '%' . $dni . '%');

        if ($cuit != "-")
            $personas->where('cuit', 'LIKE', '%' . $cuit . '%');

        if ($cuil != "-")
            $personas->where('cuil', 'LIKE', '%' . $cuil . '%');

        if ($nombre != "-")
            $personas->where('nombre', 'LIKE', '%' . $nombre . '%');


        return $personas;
    }

    private function buscarPersonasConCuentas($dni, $cuit, $cuil, $nombre, $nro_cuenta)
    {
        $personas =  Persona::where('dni', 'LIKE', '%' . $dni . '%')
                        ->where('cuit', 'LIKE', '%' . $cuit . '%')
                        ->where('cuil', 'LIKE', '%' . $cuil . '%')
                        ->where('nombre', 'LIKE', '%' . $nombre . '%')
                        ->with(['cuentas' => function($query) use ($nro_cuenta) {
                            $this->buscarCuentaDeSistema($query, $nro_cuenta);
                        }, 'matriz'])
                        ->where('sistema', 'LIKE', $this->obtenerNumeroDeSistema());

        return $personas;
    }

    /* Obtengo todas las personas que tengan cuenta con id_sistema1/id_sistema2 = nro_cuenta (en cuenta_persona)
           // Y despues obtengo las relaciones con las cuentas con id_sistema1/id_sistema2 = nro_cuenta */
    private function buscarPersonasPorCuenta($nro_cuenta) {
        $personas = Persona::withAndWhereHas('cuentas', function($query) use ($nro_cuenta) {
                        $query->where(function ($query) use ($nro_cuenta) {
                            $this->buscarCuentaDeSistema($query, $nro_cuenta);
                        });
                    })
                    ->with('matriz')
                    ->where('sistema', $this->obtenerNumeroDeSistema());

        return $personas;
    }

    public function buscarPersonasPorPeriodo(Request $request)
    {
        $key=0;
        $periodo = $request->periodo;
        $fecha_actual = new DateTime();
        $ano_actual = $fecha_actual->format('Y');
        $id_personas_filtradas = [];
        if($periodo == $ano_actual){
            $persona_controller = new PersonasController;
            return redirect($this->system .'/personas/buscar');
        }

       /*$personas_filtradas = $this->buscarPersonas($key,$request->dni, $request->cuit, $request->cuil, $request->nombre, $request->nro_cuenta)->get();
        foreach ($personas_filtradas as $persona_filtrada){
            array_push($id_personas_filtradas,$persona_filtrada->id);
        }*/
        // Busco matrices históricas del período seleccionado
        $historiales =
            HistorialPersona::/*whereIn('persona_id', $id_personas_filtradas)*/whereYear('created_at', '=', $periodo)
                ->select(['id', 'persona_id', 'perfil_vigente', 'created_at','matriz_id'])
                ->with([
                    'Persona' => function ($query) {
                        $query->select('id', 'nombre', 'dni', 'cuit', 'cuil')
                            ->with('Cuentas');
                    }
                ])
                ->whereHas('Persona', function ($query) {
                    $query->where('sistema', $this->obtenerNumeroDeSistema());
                })->paginate(1500);
                

        //$historiales = $this->agregarUltimoPeriodoDePeriodoAnterior($periodo, $historiales);
		
	/*$historiales = DB::select("select *,hp1.id historial_persona_id,hp1.created_at created_at_historial_persona from historial_persona hp1
            inner join  
                (select  hp.persona_id, max(hp.id) as id from historial_persona hp, personas p 
                where hp.persona_id=p.id
                and hp.created_at >= '2016-01-01'
                and hp.created_at <= '2016-12-31 23:59:59'
                and p.sistema = ".$this->obtenerNumeroDeSistema() . "
                group by hp.persona_id) p2    on 
            hp1.id = p2.id inner join personas p3 on p3.id = hp1.persona_id")->paginate(20);
		*/
		//$persona = new Persona();
		
        return view('personas.listar-por-periodo', [
            'historiales' => $historiales,
            'periodo' => $periodo
			//'persona' => $persona
        ]);
    }

    /**
     * Funcion para obtener la ultima matriz vigente del periodo anterior.
     * Ejemplo: Se busca el periodo 2017, y no tiene matrices para ese periodo.
     *          Se accede a esta funcion y se encuentran 3 matrices historiacs.
     *          De esas 3 matrices se obtiene la que tiene fecha más grande (la última).
     * @param $periodo
     * @param $historiales
     * @return mixed
     */
    private function agregarUltimoPeriodoDePeriodoAnterior($periodo, $historiales)
    {
        $personas_con_historico_en_periodo_seleccionado = [];

        foreach ($historiales as $historial) {
            $personas_con_historico_en_periodo_seleccionado[] = $historial->persona_id;
        }

        // Busco la ultima matriz historica de las personas que no tienen matriz para el período seleccionado
        $historiales_anteriores_al_periodo =
            HistorialPersona::whereYear('created_at', '<', $periodo)
                ->select(['id', 'persona_id', 'perfil_vigente', 'created_at'])
                ->with([
                    'Persona' => function ($query) {
                        $query->select('id', 'nombre', 'dni', 'cuit', 'cuil')
                            ->with('Cuentas');
                    }
                ])
                ->whereHas('Persona', function ($query) {
                    $query->where('sistema', $this->obtenerNumeroDeSistema())
                        ->whereNotNull('matriz_id');
                })
                ->whereNotIn('persona_id', $personas_con_historico_en_periodo_seleccionado)
                ->orderBy('created_at', 'DESC')
                ->get();

        $historiales_anteriores = [];

        /*
         *  Hago una especie de burbujeo recorriendo por cada persona, sus historiales, para obtener el último
         */
        foreach ($historiales_anteriores_al_periodo as $key => $historial_anterior) {
            if (!isset($historiales_anteriores[$historial_anterior->persona_id])) {
                $historiales_anteriores[$historial_anterior->persona_id] = $historial_anterior;
            } else {
                $historial_anterior_guardado = $historiales_anteriores[$historial_anterior->persona_id];

                if (strtotime($historial_anterior->created_at) > strtotime($historial_anterior_guardado->created_at)) {
                    $historiales_anteriores[$historial_anterior->persona_id] = $historial_anterior;
                }
            }
        }

        // Agrego a la colección de historiales
        foreach ($historiales_anteriores as $historial_anterior) {
            $historiales->push($historial_anterior);
        }

        return $historiales;
    }
}
