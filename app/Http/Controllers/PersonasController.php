<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;

use App\Classes\Personas\PersonaFactory;
use App\Classes\Traits\Matrices;
use App\HistorialPersona;
use App\Persona;
use App\Matriz;
use App\Riesgo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Log;


class PersonasController extends BuscadorPersonasController
{
    use Matrices;

    public $personas_busqueda;

    public function __construct()
    {
        $this->personas_busqueda = []; // Son las personas de una busqueda

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('personas.index');
    }

    public function buscar($key, Request $request)
    {
        // Motivos de busqueda
        $dni = $request->dni;
        $cuit = $request->cuit;
        $cuil = $request->cuil;
        $nombre = $request->nombre;
        $nro_cuenta = $request->nro_cuenta;

        if (trim($dni) == "") $dni = "-";
        if (trim($cuit) == "") $cuit = "-";
        if (trim($cuil) == "") $cuil = "-";
        if (trim($nombre) == "") $nombre = "-";
        if (trim($nro_cuenta) == "") $nro_cuenta = "-";

        $personas = $this->buscarPersonas($key, $dni, $cuit, $cuil, $nombre, $nro_cuenta);


        $filtros = ['dni' => $dni, 'cuit' => $cuit, 'cuil' => $cuil, 'nombre' => $nombre, 'nro_cuenta' => $nro_cuenta];


        $this->personas_busqueda = $this->obtenerColeccionDeAPartes($personas);

        /* Si se filtro por algo (dni, cuit, cuil, nombre, nro_cuenta) => si ya se hizo el get */
        /*if ($this->personas_busqueda->count() <= 0) {
            $this->personas_busqueda = [];
        }*/
        foreach ($this->personas_busqueda as $persona) {
            $persona = $this->agregarMascaraACampos($persona);
        }


        return view('personas.listar', array('personas' => $this->personas_busqueda, 'filtros' => $filtros));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Le paso todas las matrices a la vista
        $matrices = Matriz::all();


        return view('personas.create', [
            'matrices' => $matrices
        ]);

    }

    private function validarPersona($request)
    {
        // Las categorias siempre se manejan con mayusculas, asi que me aseguro de eso
        if (isset($request['categoria_monotributo'])) $request['categoria_monotributo'] = strtoupper($request['categoria_monotributo']);

        // Se verifica que el campo descripcion sea unico en la tabla campos para la matriz_id
        $this->validate($request, [
            'matriz_id' => 'required',
            'nombre' => 'required|alpha_spaces',

            // Comun a todas las matrices
            'anio_nacimiento' => 'digits:4',
            'sueldo' => 'digits_between:3,15',
            'dni' => 'digits_between:7,8',
            'cuit' => 'digits_between:10,11',
            'cuil' => 'digits_between:10,11',
            'perfil_vigente' => 'digits_between:1,15',

            // Monotributista
            'categoria_monotributo' => 'in:A,B,C,D,E,F,G,H,I,J,K,L',

            // Persona Juridica
            'patrimonio_caja_y_bancos' => 'digits_between:1,15',
            'patrimonio_inversiones' => 'digits_between:1,15',
            'patrimonio_activo_corriente' => 'digits_between:1,15',
            'patrimonio_pasivo_corriente' => 'digits_between:1,15',
            'patrimonio_neto' => 'digits_between:1,15',
            'ventas' => 'digits_between:1,15',
            'resultado_bruto' => 'digits_between:1,15',

            // Bienes Personales
            'total_bienes_exentos' => 'digits_between:1,15',
            'dinero_efectivo' => 'digits_between:1,15',

            // Otros
            'monto_percibido' => 'digits_between:1,15',

            'email' => 'email'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validarPersona($request);

        $obj_persona = $this->crearTipoDePersonaSegunMatriz($request->matriz_id);

        // Creo la nueva persona y la guardo en una variable
        $persona = $obj_persona::create($request->all());

        // Agregar las relaciones many-to-many de nivel_persona
        if ($this->crearRelacionesNivelesConPersona($persona, $request->input('niveles'))) {
            // Obtengo el total de los pesos de niveles asignados a la persona
            $obj_persona->total_pesos = $persona->totalPeso();
            $obj_persona->datos = $persona;

            // Actualizo el perfil de la persona recien creada
            $persona->perfil_calculado = $obj_persona->calcularPerfil();
            $persona->perfil_vigente = $persona->perfil_calculado;

            // Guardo a que sistema pertenece la persona
            $persona->sistema = $this->obtenerNumeroDeSistema();
            $persona->periodo = date("Y", time());
            $persona->riesgo_id = $request->riesgo_id;

            $persona->save();

            return response()->json(['redirectTo' => '/' . $this->system . '/personas/' . $persona->id]);
        }
    }

    /**
     * Guarda en la tabla nivel_persona las relaciones correspondientes
     * @param \Illuminate\Http\Request $request
     * @return none
     *
     */
    private function crearRelacionesNivelesConPersona($persona, $niveles)
    {
        foreach ($niveles as $nivel_id => $valor) {
            $persona->Niveles()->attach($valor);
            $persona->touch();
        }

        return true;
    }

    /**
     * Crea un objeto segun el tipo de matriz
     * @param $matriz_id
     * @return object
     *
     */
    private function crearTipoDePersonaSegunMatriz($matriz_id)
    {
        $matriz = Matriz::find($matriz_id);

        return PersonaFactory::crearPersona($matriz->descripcion);
    }

    /**
     * Muestra la matriz, campos, adjuntos y datos de una persona
     * @param $persona_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($key, $persona_id)
    {
        $persona = Persona::with([
            'cuentas' => function ($query) {
                $this->buscarCuentaDeSistema($query, '%');
            },
            'niveles',
            'adjuntos' => function ($query) {
                $query->orderBy('created_at', 'DESC');
            },
            'historial' => function ($query) {
                $query->orderBy('created_at', 'DESC')
                    ->with(['user' => function ($query) {
                        $query->withTrashed();
                    }]);
            },
            'Riesgo'
        ])->where('id', $persona_id)->get()[0]; // El 0 va para sacarle los [] de [...]

        $cuentas = $persona->cuentas;

        // Obtengo el tipo de persona (matriz_id)
        $tipo_persona = $persona->matriz_id;

        // Si la persona tiene asignada una matriz
        if ($tipo_persona) {
            $matriz = $this->mostrarMatrizPersona($tipo_persona, $persona->id, $persona->niveles);

            $obj_matriz_persona = Matriz::find($tipo_persona);

            $campos = $obj_matriz_persona->campos;
            $descripcion_matriz = $obj_matriz_persona->descripcion;


        } else {
            $matriz = [];
            $campos = [];
            $descripcion_matriz = "";
        }
        $historial = $persona->historial;
        $adjuntos = $persona->adjuntos;
        $matrices = Matriz::all();

        $perfil_calculado = $persona->perfil_calculado;
        $perfil_vigente = $persona->perfil_vigente;
        $persona = $this->agregarMascaraACampos($persona);

        return view('personas.ver',
            [
                'matrices' => $matrices,
                'matriz_parametros' => $matriz,
                'campos' => $campos,
                'persona' => $persona,
                'perfil_calculado' => $perfil_calculado,
                'perfil_vigente' => $perfil_vigente,
                'cuentas' => $cuentas,
                'historial' => $historial,
                'adjuntos' => $adjuntos,
                'descripcion_matriz' => $descripcion_matriz
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($key, $persona_id)
    {
        $persona = Persona::find($persona_id);
        $cuentas = $persona->cuentas;

        $tipo_persona = $persona->matriz_id;

        $matriz = $this->mostrarMatrizPersona($tipo_persona, $persona->id, $persona->niveles);

        $matrices = Matriz::all();

        $obj_matriz_persona = $matrices->filter(function ($matriz) use ($tipo_persona) {
            if ($matriz->id == $tipo_persona) return $matriz;
        })->first();

        $campos = $obj_matriz_persona->campos;

        $descripcion_matriz = $obj_matriz_persona->descripcion;

        $riesgos = Riesgo::where('sistema', '=', Session::get('ACTUAL_SYSTEM'))->get();

        return view('personas.edit', array('matriz_parametros' => $matriz, 'matrices' => $matrices, 'campos' => $campos, 'persona' => $persona, 'cuentas' => $cuentas, 'descripcion_matriz' => $descripcion_matriz, 'riesgos' => $riesgos));
    }

    public function asignarMatriz(Request $request, $key, $persona_id)
    {
        $persona = Persona::findOrFail($persona_id);

        $persona->matriz_id = $request->matriz_id;

        $persona->save();

        return redirect($this->system . "/personas/" . $persona_id);
    }


    public function completarMatrizHistorica($persona_id, $anio)
    {
        $persona = Persona::find($persona_id);
        // Cuido los datos viejos
        $perfil_calculado_viejo = $persona->perfil_calculado;
        $perfil_vigente_viejo = $persona->perfil_vigente;
        if ($persona->niveles == "") {
            $niveles_viejos = "[]";
        } else {
            $niveles_viejos = $persona->niveles;
        }

        $matriz_id_vieja = $persona->matriz_id;
        $riesgo_id_viejo = (int)$persona->riesgo_id;

        /* Auditoria Usuario */

        $user_id = 3;

        // Guardo en el historial los datos viejos
        $historial_persona = HistorialPersona::create([
            'niveles' => $niveles_viejos,
            'perfil_calculado' => $perfil_calculado_viejo,
            'perfil_vigente' => $perfil_vigente_viejo,
            'riesgo_id' => $riesgo_id_viejo,
            'persona_id' => $persona->id,
            'matriz_id' => $matriz_id_vieja,
            'user_id' => $user_id,
        ]);

        DB::table('historial_persona')
            ->where('id', $historial_persona->id)
            ->update(['created_at' => $anio . '-12-31 23:59:59', 'updated_at' => $anio . '-12-31 23:59:59']);

        return $historial_persona;
    }

   /* public function completarMatrizHistorica_unica_vez()
        //esto fue para arreglar la migracion inicial
    {
        set_time_limit(300);

        $personas = DB::select("select * from personas where id not in (select persona_id from historial_persona where created_at < '2017-01-01') 
        and (created_at < '2017-01-01' or created_at is null)");
        foreach ($personas as $persona) {
            $this->completarMatrizHistorica($persona->id, '2016');
        }
    }*/

    public function completarMatrizHistorica_unica_vez_al_anio($anio)
        //esto se va a usar el primero de enero todos los años, el anio que recibe es el año nuevo.
    {
        set_time_limit(300);
        $anio_anterior = $anio - 1;
        $personas = DB::select("select * from personas where id not in (select persona_id from historial_persona where created_at < '" . $anio . "-01-01 00:00:00' and created_at > '" . $anio_anterior . "-01-01 00:00:00')");
        foreach ($personas as $persona) {
            $this->completarMatrizHistorica($persona->id, $anio_anterior);
        }
        $personas = DB::select("select * from personas where id not in (select persona_id from historial_persona where created_at < '" . $anio . "-01-01 00:00:00' and created_at > '" . $anio_anterior . "-01-01 00:00:00')");

        dd(count($persona));
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($key, Request $request, $persona_id)
    {

        $this->validarPersona($request);

        // Obtengo la persona vieja, para poder extraer los datos y guardar en el historial
        $persona = Persona::find($persona_id);

        // Realizar validaciones
        if (isset($request['categoria_monotributo'])) $request['categoria_monotributo'] = strtoupper($request['categoria_monotributo']);

        // Se verifica que el campo descripcion sea unico en la tabla campos para la matriz_id
        $this->validate($request, [
            'matriz_id' => 'required',
            'nombre' => 'required|alpha_spaces',

            // Comun a todas las matrices
            'anio_nacimiento' => 'digits:4',
            'sueldo' => 'digits_between:3,15',
            'dni' => 'digits_between:5,15',
            'cuit' => 'digits_between:5,15',
            'cuil' => 'digits_between:5,15',

            // Monotributista
            'categoria_monotributo' => 'in:A,B,C,D,E,F,G,H,I,J,K,L',

            // Persona Juridica
            'patrimonio_caja_y_bancos' => 'digits_between:1,15',
            'patrimonio_inversiones' => 'digits_between:1,15',
            'patrimonio_activo_corriente' => 'digits_between:1,15',
            'patrimonio_pasivo_corriente' => 'digits_between:1,15',
            'patrimonio_neto' => 'digits_between:1,15',
            'ventas' => 'digits_between:1,15',
            'resultado_bruto' => 'digits_between:1,15',

            // Bienes Personales
            'total_bienes_exentos' => 'digits_between:1,15',
            'dinero_efectivo' => 'digits_between:1,15',

            // Otros
            'monto_percibido' => 'digits_between:1,15',

            // Ajuste
            'ajuste' => 'numeric'
        ]);

        // Cuido los datos viejos
        $perfil_calculado_viejo = $persona->perfil_calculado;
        $perfil_vigente_viejo = $persona->perfil_vigente;
        $niveles_viejos = $persona->niveles;
        $matriz_id_vieja = $persona->matriz_id;
        $riesgo_id_viejo = (int)$persona->riesgo_id;

        /* Auditoria Usuario */
        $user = Auth::user();
        $user_id = $user->id;

        // Guardo en el historial los datos viejos
        HistorialPersona::create([
            'niveles' => $niveles_viejos,
            'perfil_calculado' => $perfil_calculado_viejo,
            'perfil_vigente' => $perfil_vigente_viejo,
            'riesgo_id' => $riesgo_id_viejo,
            'persona_id' => $persona->id,
            'matriz_id' => $matriz_id_vieja,
            'user_id' => $user_id
        ]);

        // Actualizo a la persona
        $obj_persona = $this->crearTipoDePersonaSegunMatriz($request->matriz_id);

        // Actualizo los campos de la persona
        $obj_persona::where('id', $persona->id)->update($request->except(['_token', 'niveles', '_method', 'persona_id', 'ajuste']));

        // Elimino las relaciones de niveles con la persona
        $persona->niveles()->detach();

        // Agregar las relaciones many-to-many de nivel_persona
        if ($this->crearRelacionesNivelesConPersona($persona, $request->input('niveles'))) {
            // Actualizo los niveles del modelo buscando la persona recien actualizada
            $persona = Persona::find($persona->id);

            // Obtengo el total de los pesos de niveles asignados a la persona
            $obj_persona->total_pesos = $persona->totalPeso();
            $obj_persona->datos = $persona;

            // Actualizo el perfil de la persona recien creada
            $persona->perfil_calculado = $obj_persona->calcularPerfil();

            Log::info('MODIFICO PERFIL');
            Log::info((double)$persona->perfil_calculado);
            Log::info((double)$perfil_calculado_viejo);
            Log::info(trim((double)$persona->perfil_calculado) != trim((double)$perfil_calculado_viejo));

            // Si el perfil calculado es distinto al que ya estaba calculado => vigente = calculado (nuevo)
            if (trim((double)$persona->perfil_calculado) !== trim((double)$perfil_calculado_viejo)) {
                $persona->perfil_vigente = $persona->perfil_calculado;
            } else // Ajusto el perfil vigente
            {
                $persona->perfil_vigente = $persona->perfil_vigente + $request->ajuste;
            }

            $persona->periodo = date("Y", time());
            $persona->riesgo_id = $request->riesgo_id;
            $persona->save();


            // Actualizo los perfiles de las cuentas SOLO si cambio el perfil vigente
            if ($persona->perfil_vigente != $perfil_vigente_viejo) {
                $this->actualizarPerfilDeCuentasRelacionadasConLaPersona($persona->cuentas, $perfil_vigente_viejo, $persona->perfil_vigente);
            }

            $request->session()->flash('actualizado', 'Persona actualizada');
            return json_encode(['success' => true, 'redirectTo' => '/' . $this->system . '/personas/' . $persona->id]);
        }
    }

    private function actualizarPerfilDeCuentasRelacionadasConLaPersona($cuentas, $perfil_vigente_viejo, $perfil_vigente_nuevo)
    {
        foreach ($cuentas as $cuenta) {
            $cuenta->actualizaTuPerfil($perfil_vigente_viejo, $perfil_vigente_nuevo);
        }
    }

    public function buscarPersonasPorPeriodoForm()
    {
        return view('personas.buscar-por-periodo');
    }
}
