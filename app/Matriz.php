<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matriz extends Model
{
    protected $table = "matrices";

    protected $fillable = [
        'descripcion'
    ];

    /*
     * Defino las relaciones del modelo Matriz
     */

    public function Parametros(){
        return $this->hasMany(Parametro::class);
    }

    public function Campos(){
        return $this->hasMany(Campo::class);
    }

    public function Personas(){
        return $this->hasMany(Persona::class);
    }
}
