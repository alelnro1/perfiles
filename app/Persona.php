<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = 'personas';

    protected $fillable = [
        'nombre', 'dni', 'cuit', 'cuil', 'matriz_id', 'razon_social', 'edad', 'sueldo', 'categoria_monotributo', 'monto_percibido',
        'fecha_documentacion', 'cierre_balance', 'total_bienes_exentos', 'dinero_efectivo', 'patrimonio_caja_y_bancos',
        'patrimonio_inversiones', 'patrimonio_activo_corriente', 'patrimonio_pasivo_corriente', 'patrimonio_neto', 'ventas',
        'resultado_bruto', 'perfil_vigente', 'anio_nacimiento', 'perfil_vigente', 'email', 'periodo', 'riesgo_id'
    ];

    public function exists($id)
    {
        return $this->find($id);
    }

    public function scopeWithAndWhereHas($query, $relation, $constraint){
        return $query->whereHas($relation, $constraint)
            ->with([$relation => $constraint]);
    }

    /**
     * Calcula el peso de todas los niveles asignados a esa persona
     * @return int
     */
    public function totalPeso()
    {
        $niveles = $this->niveles;
        $total_peso = 0;

        foreach($niveles as $nivel)
        {
            $total_peso += $nivel->peso;
        }

        return $total_peso;
    }


    public function Cuentas(){
        return $this->belongsToMany(Cuenta::class, 'cuenta_persona', 'persona_id', 'cuenta_id');
    }

    public function Niveles(){
        return $this->belongsToMany(Nivel::class, 'nivel_persona', 'persona_id', 'nivel_id')->withTimestamps();
    }

    public function Historial(){
        return $this->hasMany(HistorialPersona::class);
    }

    public function Adjuntos(){
        return $this->hasMany(Adjunto::class);
    }

    public function Matriz(){
        return $this->belongsTo(Matriz::class);
    }

    public function Riesgo() {
        return $this->belongsTo(Riesgo::class);
    }
}
