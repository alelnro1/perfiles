<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Riesgo extends Model
{
    protected $table = "riesgos";

    /**
     * Devuelve el riesgo por default de cada sistema, ordenado por id.
     * O sea que siempre va a agarrar por default al primero.
     * Ej: riesgo(id:1, conservador), riesgo(id:2, moderado), riesgo(id:3, agresivo)
     * Va a devolver el riesgo con id menor => conservador
     * @param $sistema
     * @return mixed
     */
    public static function dameRiesgoDefault($sistema)
    {
        $riesgo = Riesgo::where('sistema', '=', Session::get('ACTUAL_SYSTEM'))->first();

        return $riesgo;
    }

    public function Personas() {
        return $this->hasMany(Persona::class);
    }

    public function Historiales() {
        return $this->hasMany(HistorialPersona::class);
    }
}
