<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuenta extends Model
{
    protected $fillable = [
        'id_sistema1', 'id_sistema2', 'perfil', 'importacion_id', 'comercial_id'
    ];

    public function actualizaTuPerfil($perfil_persona_vigente_viejo, $perfil_persona_vigente_actualizado) {
        $this->perfil = $this->perfil - $perfil_persona_vigente_viejo + $perfil_persona_vigente_actualizado;

        $this->save();
    }

    public function Personas(){
        return $this->belongsToMany(Persona::class, 'cuenta_persona', 'cuenta_id', 'persona_id');
    }

    public function Adjuntos(){
        return $this->hasMany(Adjunto::class);
    }

    public function Alertas(){
        return $this->hasMany(Alerta::class);
    }

    public function Importacion() {
        return $this->belongsTo(Importacion::class);
    }

    public function Comercial() {
        return $this->belongsTo(Comercial::class);
    }
}
