<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    public function Alertas(){
        return $this->hasMany(Alerta::class);
    }

    public function Cuentas(){
        return $this->hasManyThrough(Cuenta::class, Alerta::class, 'estado_id', 'alerta_id');
    }
}
