<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class Alerta extends Model
{
    protected $fillable = [
        'cuenta_id', 'estado_id', 'id_sistema1', 'id_sistema2', 'oficial_cuenta', 'excedente', 'observacion', 'limite_perfil',
        'nombre', 'fecha_resolucion', 'monto_estimado', 'desvio', 'importe', 'importacion_id', 'fecha_alta'
    ];

    protected function actualizaContadores() {
        $alertas = $this->all();

        $alertas->each(function($alerta) {
            $this->actualizarElContadorDeLaAlerta($alerta);
        });
    }

    private function contarDiasEntreFechas($fecha_inicio) {
        $hoy = time();
        $fecha = strtotime($fecha_inicio);
        $diferencia_dias = $hoy - $fecha;
        $contador = 150 - floor($diferencia_dias/(60*60*24));

        return $contador;
    }

    public function dameAlertasSinFiltrar(){
        $this->actualizaContadores();

        $alertas = $this->with(['cuenta', 'estado']);

        return $alertas;
    }

    public function dameAlertasFiltradasPorEstadoYNroCuenta($estado_id, $nro_cuenta){
        $this->actualizaContadores();

        $alertas = $this->where('estado_id', $estado_id)
                    ->with([
                        'estado',
                        'cuenta' => function($query) use ($nro_cuenta) {
                            $query->where(function ($query) use ($nro_cuenta) {
                                $query->where('id_sistema1', 'LIKE', '%' . $nro_cuenta . '%');
                            });
                        }
                    ])
                    ->whereHas('cuenta', function($query) use ($nro_cuenta) {
                        $query->where(function ($query) use ($nro_cuenta) {
                            $query->where('id_sistema1', 'LIKE', '%' . $nro_cuenta . '%');
                        });
                    });

        return $alertas;
    }

    public function dameAlertasFiltradasPorNroCuenta($nro_cuenta) {
        $this->actualizaContadores();

        $alertas = $this->with([
                    'cuenta' => function ($query) use ($nro_cuenta) {
                        $query->where(function ($query) use ($nro_cuenta) {
                            $query->where('id_sistema1', 'LIKE', '%' . $nro_cuenta . '%');
                        });
                    }])
                    ->whereHas('cuenta', function($query) use ($nro_cuenta) {
                        $query->where(function ($query) use ($nro_cuenta) {
                            $query->where('id_sistema1', 'LIKE', '%' . $nro_cuenta . '%');
                        });
                    });

        return $alertas;
    }

    public function dameAlertasFiltradasPorEstado($estado_id) {
        $this->actualizaContadores();

        $alertas = $this->where('estado_id', $estado_id)
                    ->with(['estado', 'cuenta']);

        return $alertas;
    }

    public function dameAlertasFiltradasPorNombre($alertas, $nombre) {
        $this->actualizaContadores();

        $alertas = $this->where('nombre', 'LIKE', '%' . $nombre . '%');

        return $alertas;
    }

    public function dameAlertasFiltradasPorContador($alertas, $tipo_busca_contador, $contador) {
        $this->actualizaContadores();

        $alertas = $this->where('contador', $tipo_busca_contador, $contador);

        return $alertas;
    }

    public function dameAlertasFiltradasPorFecha($alertas, $fecha_desde, $fecha_hasta) {
        $this->actualizaContadores();

        $alertas = $this->where('fecha_alta', ">=", $fecha_desde)
                    ->where('fecha_alta', "<=", $fecha_hasta);

        return $alertas;
    }

    public function dameAlertasDeCuentasFiltradasPorComercialDeCuenta($comercial_cuenta) {
        $this->actualizaContadores();

        $alertas = $this
                    ->whereHas('cuenta.comercial', function($query) use ($comercial_cuenta) {
                        $query->where('comercial.nombre', 'LIKE', '%' . $comercial_cuenta . '%');
                    });

        return $alertas;
    }

    public function actualizarElContadorDeLaAlerta($alerta)
    {
        $contador = $this->contarDiasEntreFechas($alerta->fecha_alta);

        if (!$this->tieneEstadoResuelto($alerta)) {
            $alerta->contador = $contador;

            $alerta->save();
        }
    }

    public function actualizarLaFechaDeResolucionDeLaAlertaResuelto() {
        $this->fecha_resolucion = date('Y-m-d H:i:s');
        $this->save();
    }

    public function actualizarLaFechaDeResolucionDeLaAlertaNoResuelto() {
        $this->fecha_resolucion = "";
        $this->save();
    }

    public function tieneEstadoResuelto($alerta)
    {
        $alerta->load('estado');
        $estado = $alerta->estado->descripcion;
        $estados_resueltos = ["Resuelto desfavorable", "Resuelta Favorable"];

        if (in_array($estado, $estados_resueltos)) {
            return true;
        } else {
            return false;
        }
    }

    /**************** RELACIONES ****************************/
    public function Estado() {
        return $this->belongsTo(Estado::class);
    }

    public function Cuenta() {
        return $this->belongsTo(Cuenta::class);
    }

    public function Adjuntos() {
        return $this->hasMany(Adjunto::class);
    }

    public function Importaciones() {
        return $this->belongsTo(Importacion::class);
    }
}
