<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nivel extends Model
{
    protected $table = "niveles";

    protected $fillable = [
        'descripcion', 'parametro_id', 'peso'
    ];

    public function Parametros(){
        return $this->belongsTo(Parametro::class, 'parametro_id');
    }
}
