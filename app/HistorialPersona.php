<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistorialPersona extends Model
{
    protected $table = 'historial_persona';

    protected $fillable = ['niveles', 'perfil_calculado', 'perfil_vigente', 'persona_id', 'matriz_id', 'user_id', 'riesgo_id'];

    /*******************
     * Está mal definida pero no la quito porque no se si se usa en otro lugar
     */
    public function Personas(){
        return $this->belongsTo(Persona::class);
    }

    public function User() {
        return $this->belongsTo(User::class);
    }

    public function Persona() {
        return $this->belongsTo(Persona::class, 'persona_id');
    }

    public function Riesgo() {
        return $this->belongsTo(Riesgo::class, 'riesgo_id');
    }
}
