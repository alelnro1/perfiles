<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parametro extends Model
{
    protected $table = "parametros";

    protected $fillable = [
        'descripcion', 'matriz_id', 'peso'
    ];

    public function Matrices()
    {
        return $this->belongsTo(Matriz::class, 'matriz_id');
    }

    public function Niveles()
    {
        return $this->hasMany(Nivel::class);
    }
}
