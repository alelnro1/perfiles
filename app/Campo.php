<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campo extends Model
{
    protected $fillable = [
        'nombre', 'descripcion', 'matriz_id'
    ];

    public function Matriz(){
        return $this->belongsTo(Matriz::class);
    }

    public function Persona(){
        return $this->belongsToMany(Persona::class);
    }
}
