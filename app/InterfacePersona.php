<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 25/02/2016
 * Time: 10:38 AM
 */

namespace App;


interface InterfacePersona
{
    public function calcularPerfil();
}