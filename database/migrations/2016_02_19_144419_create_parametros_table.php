<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParametrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parametros', function(Blueprint $table){
            $table->increments('id');
            $table->string('descripcion');
            $table->integer('peso');
            $table->integer('matriz_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('parametros', function($table) {
            $table->foreign('matriz_id')->references('id')->on('matrices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('parametros');
    }
}
