<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNivelesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('niveles', function(Blueprint $table){
            $table->increments('id');
            $table->string('descripcion');
            $table->float('peso')->unsigned();
            $table->integer('parametro_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('niveles', function($table) {
            $table->foreign('parametro_id')->references('id')->on('parametros');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('niveles');
    }
}
