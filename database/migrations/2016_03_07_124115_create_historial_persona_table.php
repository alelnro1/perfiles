<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorialPersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_persona', function(Blueprint $table){
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->integer('matriz_id')->unsigned();
            $table->text('niveles');
            $table->double('perfil_calculado');
            $table->double('perfil_vigente');
            $table->integer('user_id')->unsigned()->comment = "El id del usuario que hizo las modificaciones";
            $table->timestamps();
        });

        Schema::table('historial_persona', function($table) {
            $table->foreign('matriz_id')->references('id')->on('matrices');
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('historial_persona');
    }
}
