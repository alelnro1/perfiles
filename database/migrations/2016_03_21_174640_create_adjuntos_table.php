<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdjuntosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adjuntos', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('cuenta_id')->unsigned()->nullable();
            $table->integer('persona_id')->unsigned()->nullable();
            $table->integer('alerta_id')->unsigned()->nullable();
            $table->string('adjunto');
            $table->text('descripcion');
            $table->timestamps();
        });

        Schema::table('adjuntos', function($table) {
            $table->foreign('cuenta_id')->references('id')->on('cuentas');
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->foreign('alerta_id')->references('id')->on('alertas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adjuntos');
    }
}
