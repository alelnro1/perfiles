<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('importaciones', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('link');
            $table->integer('sistema')->unsigned();
            $table->integer('user_id')->unsigned()->comment = "El id del usuario que hizo las modificaciones";
            $table->timestamps();
        });

        Schema::table('importaciones', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('importaciones');
    }
}
