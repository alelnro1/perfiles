<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCamposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campos', function(Blueprint $table){
            $table->increments('id');
            $table->string('descripcion');
            $table->string('nombre');
            $table->integer('matriz_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('campos', function($table) {
            $table->foreign('matriz_id')->references('id')->on('matrices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campos');
    }
}
