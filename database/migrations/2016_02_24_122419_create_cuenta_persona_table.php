<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentaPersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuenta_persona', function(Blueprint $table){
            $table->integer('cuenta_id')->unsigned();
            $table->integer('persona_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('cuenta_persona', function($table) {
            $table->foreign('cuenta_id')->references('id')->on('cuentas');
            $table->foreign('persona_id')->references('id')->on('personas');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cuenta_persona');
    }
}
