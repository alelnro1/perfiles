<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuentas', function(Blueprint $table){
            $table->increments('id');
            $table->integer('importacion_id')->unsigned()->nullable();
            $table->integer('id_sistema1')->unsigned();
            $table->integer('id_sistema2')->unsigned();
            $table->double('perfil')->unsigned();
            $table->integer('comercial_id')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::table('cuentas', function($table) {
            $table->foreign('importacion_id')->references('id')->on('importaciones');
            $table->foreign('comercial_id')->references('id')->on('comercial');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cuentas');
    }
}
