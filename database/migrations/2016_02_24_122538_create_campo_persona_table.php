<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampoPersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campo_persona', function(Blueprint $table){
            $table->integer('persona_id')->unsigned();
            $table->integer('campo_id')->unsigned();
            $table->string('valor');
            $table->timestamps();
        });

        Schema::table('campo_persona', function($table) {
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->foreign('campo_id')->references('id')->on('campos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campo_persona');
    }
}
