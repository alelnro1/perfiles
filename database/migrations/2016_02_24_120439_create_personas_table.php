<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function(Blueprint $table){
            $table->increments('id');
            $table->double('perfil_calculado')->unsigned();
            $table->double('perfil_vigente')->unsigned();
            $table->string('nombre');
            $table->string('email');
            $table->integer('anio_nacimiento')->unsigned();
            $table->double('sueldo')->unsigned();
            $table->string('categoria_monotributo', 1);
            $table->double('monto_percibido');
            $table->double('total_bienes_exentos');
            $table->double('dinero_efectivo');
            $table->double('patrimonio_caja_y_bancos');
            $table->double('patrimonio_inversiones');
            $table->double('patrimonio_activo_corriente');
            $table->double('patrimonio_pasivo_corriente');
            $table->double('patrimonio_neto');
            $table->double('ventas');
            $table->double('resultado_bruto');
            $table->string('dni');
            $table->string('cuit');
            $table->string('cuil');
            $table->dateTime('fecha_documentacion')->nullable();
            $table->dateTime('cierre_balance')->nullable();
            $table->integer('matriz_id')->unsigned()->nullable();
            $table->integer('sistema');
            $table->timestamps();
        });

        Schema::table('personas', function($table) {
            $table->foreign('matriz_id')->references('id')->on('matrices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personas');
    }
}
