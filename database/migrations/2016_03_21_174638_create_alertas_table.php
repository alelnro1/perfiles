<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alertas', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('cuenta_id')->unsigned();
            $table->integer('estado_id')->unsigned();
            $table->integer('importacion_id')->unsigned();
            $table->integer('id_sistema1')->unsigned();
            $table->integer('id_sistema2')->unsigned();
            $table->string('oficial_cuenta')->nullable();
            $table->double('excedente')->nullable();
            $table->text('observacion');
            $table->double('limite_perfil');
            $table->string('nombre');
            $table->double('monto_estimado');
            $table->double('desvio')->nullable();
            $table->double('importe');
            $table->integer('contador')->unsigned();
            $table->dateTime('fecha_alta');
            $table->dateTime('fecha_resolucion');
            $table->timestamps();
        });

        Schema::table('alertas', function($table) {
            $table->foreign('cuenta_id')->references('id')->on('cuentas');
            $table->foreign('estado_id')->references('id')->on('estados');
            $table->foreign('importacion_id')->references('id')->on('importaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alertas');
    }
}
