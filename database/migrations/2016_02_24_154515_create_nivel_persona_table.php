<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNivelPersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nivel_persona', function(Blueprint $table){
            $table->integer('nivel_id')->unsigned();
            $table->integer('persona_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('nivel_persona', function($table) {
            $table->foreign('nivel_id')->references('id')->on('niveles');
            $table->foreign('persona_id')->references('id')->on('personas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nivel_persona');
    }
}
