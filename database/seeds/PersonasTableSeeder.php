<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PersonasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        // Creo 100 DNI
        for($dni = 1; $dni <= 100; $dni++) {
            $matriz_id = $faker->numberBetween(1,7);

            while($matriz_id == 3){ // Salteo la matriz con id 3 porque no existe en la tabla
                $matriz_id = $faker->numberBetween(1,7);
            }

            // Selecciono un tipo de monotributo
            $tipos_monotributo      = ['B', 'C', 'D', 'E', 'F', 'J', 'H', 'I', 'J', 'K', 'L'];
            $monotributo_random_key = array_rand($tipos_monotributo, 1);
            $monotributo = $tipos_monotributo[$monotributo_random_key];

            // Guardo la persona con DNI
            \App\Persona::create([
                'nombre' => $faker->name,
                'anio_nacimiento' => $faker->numberBetween(1950, 2000),
                'sueldo' => $faker->randomNumber(5),
                'dni' => $dni,
                'cuit' => $dni + 100,
                'cuil' => $dni + 200,
                'categoria_monotributo' => $monotributo,
                'total_bienes_exentos' => $faker->randomNumber(6),
                'dinero_efectivo' => $faker->randomNumber(5),
                'monto_percibido' => $faker->randomNumber(6),
                'patrimonio_caja_y_bancos' => $faker->randomNumber(6),
                'patrimonio_inversiones' => $faker->randomNumber(8),
                'patrimonio_activo_corriente' => $faker->randomNumber(8),
                'patrimonio_pasivo_corriente' => $faker->randomNumber(8),
                'patrimonio_neto' => $faker->randomNumber(8),
                'ventas' => $faker->randomNumber(8),
                'resultado_bruto' => $faker->randomNumber(7),
                'matriz_id' => $matriz_id
            ]);
        }
    }
}
